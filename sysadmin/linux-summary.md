# Display A Summary Of System Information On Linux

The [neofetch](https://github.com/dylanaraps/neofetch) tool provides a visually pleasing summary of system information.

This appears to be a standard package installable through both `apt` and `yum`.
