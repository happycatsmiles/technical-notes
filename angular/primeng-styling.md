# PrimeNG Styling

## Including Styles

The creator of PrimeNG puts the includes in the root `styles.scss`.
See sample below.

## Styling the Main Page

This does not touch the root `<body>`.
This must be styled manually.
See sample below.

## Sample `styles.scss`

```scss
@import url("../node_modules/primeng/resources/primeng.min.css");
@import url("../node_modules/primeflex/primeflex.css");
@import url("../node_modules/primeicons/primeicons.css");
@import url("../node_modules/primeng/resources/themes/bootstrap4-dark-purple/theme.css");

body {
  background-color: var(--surface-a);
  font-family: var(--font-family);
  color: var(--text-color);
}
```
