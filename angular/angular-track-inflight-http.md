# Track In-Flight HTTP Calls

## Overview

Web apps commonly need to track in-flight HTTP calls so a busy indicator can inform the user to wait.
Often logic is repeated across components or API calls.

This attempts to abstract this into an interceptor and a service.
One may use the service as the foundation for a busy indicator.

## Service

The service acts as a simple container for tracking the number of in-flight requests.

A busy indicator can hang off of the `count$` observable and watch for values greater than zero.

```ts
export interface IInFlightService {
  /** Track the number of in-flight requests. */
  count$: Observable<number>

  /** The number of HTTP requests currently in-flight. */
  readonly count: number

  /** Increments the number of in-flight requests. */
  inFlight(): void

  /** Decrements the number of in-flight requests. */
  completed(): void
}
```

## Interceptor

Interceptors return an observable of `HttpEvent` objects.
Each `HttpEvent` signals a state in the HTTP call.
It appears each HTTP call starts with a `HttpEventType.Sent` event and ends with a `HttpEventType.Response` event,
or RxJS error.

```ts
intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
return next
    .handle(req)
    .pipe(
        tap(x => {
            switch (x.type) {
            case HttpEventType.Sent:
                this.inFlightService.inFlight()
                break
            case HttpEventType.Response:
                this.inFlightService.completed()
                break
            }
        }),

        // Pass errors through.
        catchError(x => {
            this.inFlightService.completed()
            return throwError(x)
        }),
    )
}
```

## Sample Code

### `in-flight.service.ts`

```ts
import { Injectable } from '@angular/core'

import { BehaviorSubject } from 'rxjs'

@Injectable({providedIn: 'root'})
export class InFlightService {

  private _count$ = new BehaviorSubject(0)

  /** Track the number of in-flight requests. */
  public count$ = this._count$.asObservable()

  /** The number of HTTP requests currently in-flight. */
  public get count(): number {
    return this._count$.value
  }

  /** Increments the number of in-flight requests. */
  public inFlight(): void {
    this._count$.next(this.count + 1)
  }

  /** Decrements the number of in-flight requests. */
  public completed(): void {
    const nextValue = Math.max(this.count - 1, 0)
    this._count$.next(nextValue)
  }
}
```

### `in-flight.interceptor.ts`

```ts
import {
  HttpEvent,
  HttpEventType,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http'
import { Injectable } from '@angular/core'

import { catchError, tap } from 'rxjs/operators'
import { InFlightService } from '../services/in-flight.service'
import { Observable, throwError } from 'rxjs'

@Injectable()
export class InFlightInterceptor implements HttpInterceptor {

  constructor(
    private inFlightService: InFlightService,
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next
      .handle(req)
      .pipe(
        // This observable consists of HTTP events.
        // It appears that for any given successful HTTP call,
        // there are a minimum of two events that are sent:
        //
        // 1. HttpEventType.Sent
        // 2. HttpEventType.Response
        //
        // This interceptor is not interested in the other events.
        tap(x => {
          switch (x.type) {
            case HttpEventType.Sent:
              this.inFlightService.inFlight()
              break
            case HttpEventType.Response:
              this.inFlightService.completed()
              break
          }
        }),

        // Pass errors through.
        catchError(x => {
          this.inFlightService.completed()
          return throwError(x)
        }),
      )
  }
}
```
