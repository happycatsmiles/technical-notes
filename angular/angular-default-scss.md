# Default Styling to Sass in Angular

As of Angular 9,
the following will change the defaults in an existing project:

```sh
ng config schematics.@schematics/angular:component.style scss
```

[StackOverflow](https://stackoverflow.com/questions/36220256/angular-cli-sass-options)
