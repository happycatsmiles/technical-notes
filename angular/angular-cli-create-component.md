# Angular CLI Create Component

Dec 3, 2019

```
ng g component {path/component-name} [-m module]
```

If `ng` cannot find the correct module to add the component to, then the `-m` parameter will let it know. 

## Example

```
ng g component cats/cat-item
```

will create a new component thus: 

```
src/app/cats/cat-item/cat-item.component.css
src/app/cats/cat-item/cat-item.component.html
src/app/cats/cat-item/cat-item.component.spec.ts
src/app/cats/cat-item/cat-item.component.ts
```

And add it to `cats.module.ts`. 

If needed, the `--prefix=`*something* parameter can set the component's prefix. It doesn't change  the filenames. 

## More Notes

The app code is under `<PROJECT_ROOT>/src/app` with module directories underneath.

The CLI command

```
ng g c directory/component-name
```

will (g)enerate a (c)component at `<PROJECT_ROOT>/src/app/directory/component-name`. 
