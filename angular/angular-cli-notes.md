# Angular CLI Notes

For most cases during development,

```
ng serve -aot
```

There are a few cases where it's useful to connect from another device. 
Normally `ng serve` binds to localhost,
preventing outside connections from being accepted.

**NOTE** With Angular version 9,
this isn't needed as it was in the past.

To allow other devices to connect to your `ng serve` instance,

```
ng serve -aot -H 0.0.0.0
```


Produce a production build

```
ng build --prod
```

[This stackoverflow](https://stackoverflow.com/questions/44517805/does-ng-build-use-aot-compilation-when-production-is-set-to-true-in-environmen#44518517) says that `--prod` triggers AOT compilation, so it's not needed.

More information on [ng build](https://github.com/angular/angular-cli/wiki/build). 
