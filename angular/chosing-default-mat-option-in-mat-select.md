# Choosing the default `<mat-option>` in a `<mat-select>` list

## TL;DR

The `<mat-select>` component has a `compareWith` property that accepts a function to compare two objects. 
This returns `true` or `false` depending on whether they represent the same object. 

```html
<mat-select [compareWith]="compareFunction" ...> 
  <mat-option ...>
```

```ts
function compareWith(a T: b: T):boolean { 
... 
```

Remember two objects may have the identical content but be, well, two completely different objects so the `===` comparison fails. 
The `compareWith` property allows Angular Material to correctly match up the value with the right option.

## Example

```html
<mat-form-field>
  <mat-select [compareWith]="isSameUser" name="users" [(ngModel)]="users" multiple>
    <mat-option *ngFor="let person of people" [value]="person">
       {{ person.username }}
    </mat-option>
  </mat-select>
</mat-form-field>
```

```ts
isSameUser(a: User, b: User): boolean {
  return a && b && ? a.id === b.id : a === b
}
```

## Details

If there is a list of options in a drop-down,
and we want to set one of the options as a default, 
old-school HTML would use something like:

```html
<select>
  <option selected>
```

With Angular Material, 
there is no `selected` property on `mat-option`. 
While there is 
[a request to support this](https://github.com/angular/material2/issues/7246), 
Andrey Kolybelnikov explains how this works:

>Depending on a use-case it is good to know that initializing some default options as selected might not work by simply binding to the ngModel, because default values are different object instances than those in the options array.
>Thanks to the support for compareWith it is possible to set them as selected like so:
>```
>[html]
><mat-select [compareWith]="compareFn" ......>
>
>[ts]
>compareFn(c1: Object, c2: Object): boolean {
>    return c1 && c2 ? c1.id === c2.id : c1 === c2;
>}
>```
>In my case none of the above suggested solutions worked, but once I compared the default selected objects, that come from the parent component, with the objects in the mat-options array, the default options got initialized as selected automatically.

and further,

>@Jonibigoud it's literally just that. Have a look at the offical Angular docs [here](https://angular.io/api/forms/SelectControlValueAccessor#caveat-option-selection).
>
>In Material2 demo-app they have an example of the function with two implementations. It's [here](https://github.com/angular/material2/blob/1616f2fe90949b9ad76ac3389519fd49f3a1853c/src/demo-app/select/select-demo.html#L124).
>
>In my component I have a collection of User objects [people] for the options of mat select. The component receives a collection of selected User objects [users] as Input from previous state. Fair enough, objects in [people] and objects in [users] have different identities and the subset in the multiple select does not initialize with selected checkboxes by default.
>
>So, the magical `compareWith` just literally compares objects by some given values and returns true or false, and the checkboxes on the subset of [people] get the status of selected. In my code I decided to go with `[(ngModel)]` binding:
>```html
>    <mat-form-field>
>        <mat-select [compareWith]="compareFn" name="users" [(ngModel)]="users" multiple>
>            <mat-option *ngFor="let person of people" [value]="person">
>               {{ person.username }}
>            </mat-option>
>       </mat-select>
>    </mat-form-field>
>```
>And in the .ts file I utilize the function from the Angular doc to return true if two User objects have the same id:
>```ts
>    compareFn(user1: User, user2: User) {
>        return user1 && user2 ? user1.id === user2.id : user1 === user2;
>    }
>```
>If you have a similar use-case, it might work out-of-the-box.
>
>On the what's-under-the-hood note `compareWith` got me going and after a little digging I found out that it is based on a function in Angular2 called `looseIdentical` (have a look [here](https://github.com/angular/angular/blob/a9efc48e711d77e6d49050a0ce43e847833f5575/modules/angular2/src/facade/lang.dart#L223) in your free time), which in turn derives from the `identical` in Dart.js library by Google. It can be found [here](https://api.dartlang.org/stable/1.24.3/dart-core/identical.html#source) .
