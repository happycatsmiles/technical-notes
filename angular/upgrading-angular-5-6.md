# Upgrading from Angular 5 to Angular 6

Some problems were encountered. 
This was originally an Angular 2 project that to Angular 4, then Angular 5. 

## `@angular/cdk` and `@angular/material` 

These had to be manually updated. 
Uninstall/reinstall worked fine. 

## `.angular.json` 

A configuration file needed to be migrated as described [here](https://stackoverflow.com/questions/49810580/error-local-workspace-file-angular-json-could-not-be-found). 

```bash
ng update @angular/cli --migrate-only --from=1.7.4
``` 

resolved this problem. 

## Old Stuff

There were a lot of warnings about dependencies with `codelyzer` and `tsickle`. 

```bash
npm uninstall -s codelyzer tsickle
``` 

Helped but didn't eliminate the warnings. 

```bash
npm uninstall @angular/compiler-cli
npm install @angular/compiler-cli
```  

eliminated the warnings. 

## `rxjs-compat` 

Nothing would compile cleanly still, with RxJS complaining about missing `rxjs-compat` files. 
Uninstalling and reinstalling `rxjs-compat` didn't help. 

Wiping out the entire `node_modules` directory and reinstalling fixed it. 
This is a brutal method but 
at lest we knew for certain its contents were current without any old bits hanging around. 

```bash
rm -rf node_modules/
npm i
```

## `tslint`

Oops.
Eliminating the dependency warnings with `codelyzer` above broke `tslint`. 
Re-installing it solved the problem. 

```bash
npm i -s codelyzer
```
