# Accessibility and Angular

Angular has an  [Accessibility Guide](https://angular.io/guide/accessibility).

The W3C has [ARIA Authoring Practices](https://www.w3.org/TR/wai-aria-practices-1.1/)
and
[Introduction to ARIA](https://developers.google.com/web/fundamentals/accessibility/semantics-aria).

The Angular CDK has an [a11y package](https://material.angular.io/cdk/a11y/overview).


Some ideas on [patterns](https://www.smashingmagazine.com/2021/03/good-better-best-untangling-complex-world-accessible-patterns/).
This includes a link to [hiding things](https://css-tricks.com/inclusively-hidden/).
It has some good information on mindset.
ARIA can make the UX worse for people who need it.

Thoughts on handling different types of common patterns in UX design are on
[Inclusive Components](https://inclusive-components.design/).

