# Angular Array of Elements

14 Jan 2021

From
[this](https://github.com/Reboog711/JeffryHouserBlogSamplesSource/tree/master/2020/01/FocusInputs/src/app)
example code:

```html
<div *ngFor="let x of [0, 1, 2, 3, 4]">
  <b>Input {{x}}</b>: <input #input> <button (click)="onClick(x)">Set Focus</button><br/>
</div>
```

```ts
import {Component, ElementRef, QueryList, ViewChildren} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChildren('input') inputs: QueryList<ElementRef>;

  onClick(index) {
    this.inputs.toArray()[index].nativeElement.focus();
  }

}
```

