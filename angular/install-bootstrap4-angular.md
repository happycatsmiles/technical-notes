# Install Bootstrap 4 On Angular

## Add Bootstrap

Adding Bootstrap consists of two steps,
installing the NPM packages and 
adding bootstrap to the Angular configuration.

```bash
npm i --save bootstrap font-awesome jquery popper.js
```

The `angular.json` file needs to reference Bootstrap
in the projects build node,
specifically add the following lines
in the `"styles"` and `"scripts"` nodes:

```json
           "assets": [
              "src/favicon.ico",
              "src/assets"
            ],
            "styles": [
              "node_modules/bootstrap/dist/css/bootstrap.min.css",
              "src/styles.css"
            ],
            "scripts": [
              "node_modules/jquery/dist/jquery.min.js",
              "node_modules/bootstrap/dist/js/bootstrap.min.js"
            ]
          },
```

## Access Bootstrap in Sass

Bootstrap Sass variables, etc.
may be accessed by importing Bootstrap thus:

```scss
@import "~bootstrap/scss/bootstrap.scss";
```

## Global Sass

The location of the global Sass file can be moved
if the `angular.json` file is updated.

For example,
if the global styles file is moved to
`src/css/styles.scss`,
the `"build"` and `"test"` sections
are adjusted thus:

```
            ],
            "styles": [
              "node_modules/bootstrap/dist/css/bootstrap.min.css",
              "src/css/styles.scss"
            ],
            "scripts": [
...
            ],
            "styles": [
              "src/css/styles.scss"
            ],
```
