# Drop-Down Lists in Angular + Material

```html
<mat-form-field>
  <mat-label>Widget</mat-label>
  <mat-select [(ngModel)]="widget"
              [compareWith]="isSameWidget"
              (selectionChange)="changeWidget($event)">
    <mat-option *ngFor="let i of widgets" 
                [value]="i">
      {{i.name}}
    </mat-option>
  </mat-select>
</mat-form-field>
```

## Notes and Observations

### `[(ngModel)]` 

It appears that bidirectional binding ("banana in a box") is required for changes to update correctly. 

### `[compareWith]` 

This is needed for the list to be able to tell if two objects are the same. 
If two objects can be identical but not be a single unique instance, 
this is probably needed. 

### `(selectionChange)` 

This event handler fires each time the user makes a change to the selection. 
It won't fire if the program changes the selection object (`ngModel`).

The event handler receives a `MatSelectChange` object as the incoming `$event` parameter: 

```typescript
  changeWidget = ($event: MatSelectChange): void => {
    // TODO: Do something with $event.value
  }
```

### `[value]` 

The `mat-option` attribute `[value]="i"` is the object that will be assigned to the `ngModel`;
this is the selection's object value. 
