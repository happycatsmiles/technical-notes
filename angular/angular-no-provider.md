# No provider for ...!

When creating a new module with components in it,
this error message at run-time 
may indicate 
the new module was not added to the 
`app.module` 
`imports: []` 
list.

Also ensure all components exist in the library.
For example,
there is no outer `<mat-dialog-container>`.

It may be helpful to 
comment out the components and HTML content en masse and 
re-introduce elements 
until the faulty component or directive turns up.

## Dialogs

In the `app.module.ts` file,
the dialog components need to be included in the 
`declarations` and `entryComponents` section.

Also,
ensure the 
`mat-dialog-title`,
`mat-dialog-content`,
and `mat-dialog-action`
attributes are used correctly with a `<div>`,
i.e. 

```html
<div mat-dialog-title>
  {{title}}
</div>
<div mat-dialog-content>
   ... content goes here ...
</div>
<div mat-dialog-actions>
    ... buttons go here ...
</div>
```

**It is incorrect to use these attributes as elements.**
In other words,
`<mat-dialog-title>Title</mat-dialog-title>`
will **not** work.
