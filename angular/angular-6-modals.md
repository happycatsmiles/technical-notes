# Angular 6 Modals

Nov 30, 2018

This article covers the basics well,
and explains the need for the displayed component to be in the app's 
`entryComponents` array so that it is available for injection at run-time.

[	
Display Component Dynamically As A Modal Popup Using Material Dialog In Angular 6](https://www.c-sharpcorner.com/article/display-component-dynamically-as-a-modal-popup-using-material-dialog-in-angular/)
