# Upgrading Angular

## Checklist

The checklist for upgrading Angular is
[here](https://update.angular.io/).

If this link becomes outdated,
look at the
[Angular README on GitHub](https://github.com/angular/angular).


## Global Angular CLI

```bash
npm install -g @angular/cli
```

