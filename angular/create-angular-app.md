# Creating an Angular App

These are notes on helpful options for`ng new`
based on personal patterns of behavior.

* `--commit false` will not auto-commit to git.
  This is useful if one wishes to check the app was initialized correctly first.
* `--prefix <prefix>` sets the app prefix which defaults to `app` if this is not specified.
* `--routing` if one anticipates using the router.
* `--strict` per the Angular team's recommendations.
* `--style scss` to create Sass `.scss` files instead of plain CSS `.css` files.
* `-v` enabled verbose output.
* `--dry-run` enables a dry run.

```bash
ng new fuzzy-tomatoes --commit false --prefix ft --routing --strict --style scss -v --dry-run
```

## Helpful Articles

* [With Best Practices from the Start](https://blog.angular.io/with-best-practices-from-the-start-d64881a16de8).
