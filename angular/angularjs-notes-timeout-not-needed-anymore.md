# AngularJS Notes: $timeout not needed anymore

According to 
[this article](http://www.syntaxsuccess.com/viewarticle/timeouts-and-timerwrapper-in-angular-2.0) 
there is no need for `$timeout` so the normal JavaScript `setTimeout` family of functions can be used. 
