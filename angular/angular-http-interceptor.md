# Angular HTTP Interceptors

## Overview

HTTP interceptors are injectable classes that modify behaviour of `HttpClient` calls.
Interceptors implement the `HttpInterceptor` interface.
As of Angular 11 an interceptor looks like:

```ts
@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    ...
  }
}
```

## Scope

IMHO interceptors should be simple, focused, and efficient.
It's better to chain multiple easy-to-debug interceptors together than
maintain a fragile, complicated behemoth that tries to do everything.

## Providing

Note that interceptors are not global services thus they only need:

```ts
@Injectable()
```

instead of:

```ts
@Injectable({providedIn: 'root'})
```

as is common with global services.

Interceptors are explicitly declared in an `@NgModule`'s
`providers` section.
 `AppModule` may be a good candidate.

For example,
providing three interceptors which add API information,
add authorization information, and
handle HTTP errors might look like:

```ts
@NgModule({
  bootstrap: [
    ...
  ],
  imports: [
    ...
  ],
  providers: [
    HttpClientModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
  ],
})
export class AppModule {
}
```

## General Interceptor Use

The the `HttpInterceptor` interface defines the `intercept` method:

```ts
intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
```

To alter the request,
one modifies the request through the `req.clone(parameters)` method,
and passes it to `next.handle()`.

As of Angular 11, the `req.clone()` method's parameters do not have a named
type but is defined thus:

```ts
    clone(update: {
        headers?: HttpHeaders;
        reportProgress?: boolean;
        params?: HttpParams;
        responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
        withCredentials?: boolean;
        body?: T | null;
        method?: string;
        url?: string;
        setHeaders?: {
            [name: string]: string | string[];
        };
        setParams?: {
            [param: string]: string;
        };
    }): HttpRequest<T>;
```

Note that none of the fields are required;
one only need provide the items to be altered.

Two common alterations utilize the `url` and `setHeaders` fields.

## Use Case: Altering an API Endpoint

One common operation might be where
the app makes calls to API endpoints without specifying the full URI.
For example, one might do API calls against an endpoint URL such as `/customer/123`.
This endpoint needs the rest of the URL prepended to it.

For example:

```ts
readCustomer$ = (id: string): Observable<ClientInformation> => {
  const endpoint = `/customers/${encodeURIComponent(id)}`
  return this.httpClient.get<ClientInformation>(endpoint)
}
```

A simple interceptor which prepends the endpoint URL with an API prefix might look like:

```ts
@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  private readonly apiUrl = environment.apiPrefix

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const newUrlParameters = {
      url: this.apiUrl + req.url
    }

    return next.handle(req.clone(newUrlParameters))
  }
}
```

## Use Case: Adding An `Authorization` Header

Another common operation might be where a service handles OAuth2 concerns,
and the HTTP `Authorization` header needs to include a bearer token.

A simple interceptor which relies on a service to handle the details might look like:

```ts
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.authService.isLoggedIn) {
      return next.handle(req)
    }

    const updateParameters = {
      setHeaders: {
        Authorization: `${this.authService.tokenType} ${this.authService.accessToken}`
      },
    }

    return next.handle(req.clone(updateParameters))
  }
}
```

## Use Case: Handling HTTP Errors

The `next.handle()` method returns an observable with the HTTP result.
A `pipe` may be used to examine the results:

```ts
    return next
      .handle(req)
      .pipe(
        catchError((err :any): Observable<any> => {

          if (err instanceof HttpErrorResponse) {
            if (err.status === 401 || err.status === 403) {
              // Authorization problem. Handle it here.
              this.authService.handleAuthError(req, err)
              return throwError(err)
            }
          }

          return throwError(err)
        }),
      )
```

### Mind The Observables

The interceptor tutorials on the internet commonly show handled errors as a simple `return`.

```ts
    return next
      .handle(req)
      .pipe(
        catchError((err :any): Observable<any> => {

          if (err instanceof HttpErrorResponse) {
            if (err.status === 401 || err.status === 403) {
              // Authorization problem. Handle it here.
              this.authService.handleAuthError(req, err)
              return // !!!!!!!!!!!!!!!!
            }
          }

          return throwError(err)
        }),
      )
```

The thinking seems to be, "The error has been handled, so one simply returns".

The context for the interceptor stereotypically be along the lines of:

```ts
this.httpClient
  .get('/customer/123')
  .subscribe(x => this.processCustomer(x))
```

What the error interceptor returns is consumed by the `.subscribe`.
If one simply uses `return`, the following error may be seen in the console:

```
Error: You provided 'undefined' where a stream was expected. You can provide an Observable, Promise, Array, or Iterable.
```

It is possible that the default error handlers silently discards this error,
but that leaves program behaviour to chance.
Future Angular behaviour could change.

It's cleaner to return an observable created by the RxJS function `throwError`,
or another appropriate observable.
Regardless of the type of observable,
the point remains that **one should should return an observable**.

This will allow the `HttpClient` call to terminate cleanly,
while the error is *also* trapped and handled properly by the interceptor.
