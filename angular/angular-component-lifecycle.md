# Angular Component Lifecycle

A nice summary for this is [here](https://medium.com/front-end-hacking/angular-2-component-lifecycle-hooks-fa5a84b4b64d).

Official docs are [here](https://angular.io/guide/lifecycle-hooks).

## Most Common Phases

The phases that are used most commonly (in sequential order) are:

### Constructor

This should be obvious. 
It's the class constructor. 
Inject dependencies here. 

### `ngOnChanges`

This is triggered whenever a bound property's value changes. 
This is triggered before ngOnInit

### `ngOnInit`

Triggered after the constructor and the first `ngOnChanges`. 
This is a typical place for initialization. 

### `ngOnDestroy`

Triggered just before the component instance is destroyed. 
Clean up background tasks, terminate hot observables, etc. 

## Other Useful Phases

### `ngAfterViewInit` 

Called when a component's view has been initialized. 
This is called for child components before parent components. 
