# PrimeNG Theme

The PrimeNG themes don't apply to the root `<body>`.

If one wishes to apply the theme to the `<body>`,
one must do it directly. For example in the project's
root `styles.scss`:

```scss
@import url("../node_modules/primeng/resources/primeng.min.css");
@import url("../node_modules/primeflex/primeflex.css");
@import url("../node_modules/primeicons/primeicons.css");
@import url("../node_modules/primeng/resources/themes/bootstrap4-dark-purple/theme.css");

body {
  background-color: var(--surface-a);
  font-family: var(--font-family);
  color: var(--text-color);
}
```
