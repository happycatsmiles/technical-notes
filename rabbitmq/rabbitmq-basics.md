# RabbitMQ Basics

## Terminology

* Produce / producer
  * To send a message to an exchange
  * Sends a message to an exchange
* Exchange
  * Producers send messages to exchanges
  * Exchanges determine what queue (if any) a message should go.
* Queue
  * A buffer that stores messages
  * Exchanges put messages into zero or more queues.
  * Consumers read from a queue
* Consume / Consumer
  * To receive a message from a queue
  * Receives messages from a queue
  * Connects to queues

## Setup

1. Create a connection
2. Create a channel
3. Assert something to talk to
   1. Assert an exchange (producers)
   2. Assert a queue (consumers)

## Exchanges

Exchanges can be one of four types:

1. `'direct'`
2. `'topic'`
3. `'headers'`
4. `'fanout'`

### The `'direct'` Exchange Type

> The routing algorithm behind a direct exchange is simple:
   a message goes to the queues whose `binding key` exactly matches the `routing key` of the message.

1. A direct exchange accepts a message with any `routing key`.
   1. It then looks for all queues bound to the exchange whose `binding key` matches the message's `routing key`,
     and adds the message to those queues.
   2. If there are no matches, the message is silently dropped.
2. One may bind temporary (`{exclusive :true}`) queues to these exchanges as usual.
3. One may bind multiple queues to a direct exchange.
4. One may bind multiple queues to a direct exchange with different `binding key`s.

### The `'topic'` Exchange Type

1. Routing keys are words separated by periods.
2. Up to 255 bytes in length.
3. Binding keys have the same format with optional wildcards:
   1. `'*'` will substitute for exactly one word.
   2. `'#'` will substitute for zero or more words.

Example:

![](python-five.png)

Routing Key | Q1 | Q2
:-- |:-- |:-- |
`quick.orange.rabbit` | Yes | Yes
`lazy.orange.elephant` | Yes | Yes
`quick.orange.fox` | Yes | No
`lazy.brown.fox` | No | Yes
`lazy.pink.rabbit` | No | Yes
`quick.orange.male.rabbit` | No | No | Four words in the routing key.
`lazy.orange.male.rabbit` | No | Yes

### The `'fanout'` Exchange Type

1. Broadcast to all connected queues.
2. Pub/sub
3. The routing key is ignored when publishing to the exchange.
   1. Leave it as an empty string.

```ts
channel.assertExchange(exchangeName, 'fanout', {durable: true})
channel.publish(exchangeName, '', data)
```

## Queues

### Binding

Queues need to be bound to an exchange in order to receive messages.

```ts
await channel.assertExchange(exchangeName, 'fanout', {durable: true})
const queue = await channel.assertQueue('', {exclusive: true})
await channel.bindQueue(queue.queue, exchangeName, '')
```

### Anonymous Temporary Queues

When a worker connects to a pub/sub style of exchange,
it needs a temporary queue dedicated to itself to receive messages;
it doesn't need a formal named queue.

If a queue is marked `{exclusive: true}`,
then it will be disposed of when the channel closes.

An empty string for a queue name will cause RabbitMQ to generate a random unique name.

```ts
await channel.assertExchange(exchangeName, 'fanout', {durable: true})
const queue = await channel.assertQueue('', {exclusive: true})
await channel.bindQueue(queue.queue, exchangeName, '')
await channel.consume(
  queue.queue,
  ...
```

## Durability

* A queue marked `{durable: true}` will survive server restarts.

## Immutability

1. "Asserting" a queue or exchange will only create it if it doesn't exist already.
2. RabbitMQ doesn't allow one to redefine an existing queue or exchange
   with different parameters.
   1. RabbitMQ will throw and exception.
3. To change queue or exchange parameters, one must delete the queue/exchange and re-create it.

For example, the `{durable: false}` cannot later be changed to `{durable: true}`.

```ts
channel.assertQueue(queue, {durable: false})
channel.assertExchange(exchangeName, exchangeType, {durable: true})
```

## Message Durability

When RabbitMQ quits or crashes it will forget the queues and messages unless one tells it not to.
Two things are required to make sure that messages aren't lost:

1. We need to mark the queue as durable.
2. We need to mark the messages as persistent.

To mark the queue as durable, use `{durable: true}` when creating the queue:

```ts
channel.assertQueue(queue, {durable: true})
```

This must be done on **both** the sender and receiver code.

To mark the message as persistent, use `{persistent: true}` when sending:

```ts
channel.sendToQueue(queue, Buffer.from(msg), {persistent: true})
```

**NOTE:** This lowers the chance of message loss,
but doesn't guarantee message delivery.
A stronger guarantee can be obtained through [publisher confirms](https://www.rabbitmq.com/confirms.html).

## Message Distribution

* Messages are doled out to workers in a round-robin fashion.
* For messages to receive messages only when they're
  ready, workers can tell RabbitMQ to queue up only one message at a time.
  This called "fair dispatch".

The following connects a worker to a durable queue,
and tells it to only give it one message at a time,
i.e. use fair dispatch:

```ts
channel.assertQueue(queue, {durable: true});
channel.prefetch(1)
```

## Message Acknowledgements

If one doesn't care whether the worker actually completes correctly,
one can use automatic acknowledgement:

```ts
channel.consume(queue, consumerFunction, {noAck: true})
```

If one wants to redeliver a message if it the worker dies,
one can use manual acknowledgements by setting `{noAck: false}`:

```ts
channel.consume(queue, consumerFunction, {noAck: false})
```

If one uses manual acknowledgements,
one **must** send an acknowledgement on the channel it was received on.
Otherwise, the message will time out and be re-queued for processing:

```ts
channel.ack(msg)
```

> **Forgotten acknowledgment**
> It's a common mistake to miss the ack. It's an easy error, but the consequences are serious. Messages will be redelivered when your client quits (which may look like random redelivery), but RabbitMQ will eat more and more memory as it won't be able to release any unacked messages.
>
> In order to debug this kind of mistake you can use rabbitmqctl to print the messages_unacknowledged field:
>
> `sudo rabbitmqctl list_queues name messages_ready messages_unacknowledged`
>
> Source: [RabbitMQ Docs](https://www.rabbitmq.com/tutorials/tutorial-two-javascript.html)
