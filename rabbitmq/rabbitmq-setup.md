# RabbitMQ Setup

## Staring Container for Testing

The following will run RabbitMQ with the manager available on port 15672.

```sh
docker run -d --hostname my-rabbit --name some-rabbit-manager -p 5672:5672 -p 15672:15672 -p 61613:61613 -p 61614:61614 -p 15692:15692 rabbitmq:3-management
```

## Configuration Files

The [docs](https://www.rabbitmq.com/configure.html#configuration-files) say startup will show which config file is being used. 

The startup says:

```txt
2022-09-21 01:38:17.970324+00:00 [info] <0.222.0>  config file(s) : /etc/rabbitmq/conf.d/10-defaults.conf
```

The [docs](https://www.rabbitmq.com/plugins.html) talks about enabling plugins.

### Docker Image

The [official docker image](https://registry.hub.docker.com/_/rabbitmq/) says:

> Please use a configuration file instead; visit [rabbitmq.com/configure](https://www.rabbitmq.com/configure.html) to learn more about the configuration file.

It also says:

> Enabling Plugins
>
> Creating a Dockerfile will have them enabled at runtime. To see the full list of plugins present on the image rabbitmq-plugins list
> 
> FROM rabbitmq:3.8-management
> RUN rabbitmq-plugins enable --offline rabbitmq_mqtt rabbitmq_federation_management rabbitmq_stomp
> 
> You can also mount a file at /etc/rabbitmq/enabled_plugins with contents as an erlang list of atoms ending with a period.
> 
> Example enabled_plugins
> 
> [rabbitmq_federation_management,rabbitmq_management,rabbitmq_mqtt,rabbitmq_stomp].

