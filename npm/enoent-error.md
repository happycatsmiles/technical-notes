# npm ENOENT error

The following error means you're not in the correct directory. :)

```sh
 npm i
npm ERR! code ENOENT
npm ERR! syscall open
npm ERR! path C:\Users\tvenkel\Desktop/package.json
npm ERR! errno -4058
npm ERR! enoent ENOENT: no such file or directory, open 'C:\Users\tvenkel\Desktop\package.json'
npm ERR! enoent This is related to npm not being able to find a file.
npm ERR! enoent
```
