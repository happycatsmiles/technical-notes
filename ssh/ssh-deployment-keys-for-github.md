# SSH Deployment Keys for GitHub 

## Background

GitHub requires the SSH deployment keys be unique. 
This creates a problem if you try to use an account's SSH key,
because you can only use it for *one* deployment key. 

Beyond that, *an account's SSH key should not be used as a deployment key*. 
They are similar things (SSH keys) but they have two distinct purposes and distinct lifecycles. 
Deployment needs should not affect an account's SSH key.
The account's needs should not affect the deployment key. 

Thus deployment keys should be unique per project and per server. 

## Using SSH Host Aliases

SSH allows us to create *aliases* to tell SSH to access a specific host using a certain SSH key. 

Aliases live in the `~/.ssh/config` file. 
If this file doesn't exist, 
you can create it with a text editor our simply `touch` it.
Set its permissions to `600`. 

```bash
$ cd ~/.ssh
$ touch config
$ chmod 600 config
```

### Create a SSH Keys

Say we have two separate projects that we wish to deploy on a server, `fluffy-kittens` and `puppy-eyes`. 
We can generate new keys for these. 

```bash
$ ssh-keygen -t rsa -f ~/.ssh/id_rsa-fluffy-kittens -C "Fluffy Kittens on example server."
$ ssh-keygen -t rsa -f ~/.ssh/id_rsa-puppy-eyes -C "Puppy Eyes on example server."
```

If the comments are clear enough, 
it's easy to identify the keys as being a deployment key for a certain project on a certain server. 

### Create the Aliases

The `~/.ssh/config` file has the following format. 
What you use for the `Host` alias isn't terribly important. 
In this example we'll prefix `github.com` with the project name.

```
Host fluffy-kittens.github.com 
    Hostname github.com 
    IdentityFile ~/.ssh/id_rsa-fluffy-kittens
    
Host puppy-eyes.github.com 
    Hostname github.com 
    IdentityFile ~/.ssh/id_rsa-puppy-eyes
```

* The `Host` keyword is the alias. 
* `Hostname` is the actual host to connect to. 
* `IdentityFile` is the SSH key to use when connecting to `Hostname`.

### Copy the Deployment Keys to GitHub 

We'll copy/paste the public portion of the keys into the corresponding GitHub repo.

```sh
$ cat ~/.ssh/id_rsa-fluffy-kittens.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDODDzXIt++N1xniywoZK5JUHdEOvo03vNJLIMRqCcg
e9gRFjwXaeVR2qXQIXRfsN40+ZFc2C/l4yoiouscwNjHAIN09z/MeUqQqPh6nbvfDMd/XK5sa7z0QiY7
9ZdrU5wvgJGhsTOUAYHJCwMVhqeZ5tKYOkNoZpUfG793oJjlewj4ZW30XD3grEVkyC7PTINCuDb93gIm
W5XYDw1t2aE6pULgAgG3PbDPjwkWHQ9aIWnGTbM4NZJ3mY8B39+b6B9KgMn2faHNJfXxEPRBhcc0rEMD
vB5eiuk0JqGVJTQDg1BHDImgi7lBvdTiaHvQz3bF5UTckdvEfFjrD4mgnGj7 Fluffy Kittens on 
example server.
$ cat ~/.ssh/id_rsa-puppy-eyes.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC4IbjhDXk8Uh8VgbLZDzjhjuxaR5Ipyaf6H/6Ml+Lb
/F5sB9clFwFdYx7VekpIHQr1CjCgwOtA4AxHt6d2i/r6i98F5gmH9SZYneLw7c6BtSDi7oBenEGNwOsb
EK5pVc/rCJNNrPmuvVAiLGf6/BqZi454WvYaG+8wHy+XqUGcfiGvrRpWeKPLv5Ls1MO7hXj2DzOiGw1g
E9I3NWNIdaX+0Tl4wdXSJJp2XkrJtFrxCEf+RBKNmPqznCIGUrTrWr+WUzOEatwj3UxG40mWynZ2u0/6
uouXq5kuRThmsP3VFvexJh3eJtC4dbr6yjpvJM3eDfxbYm7EiRdDlZb+LNsh Puppy Eyes on 
example server.
```

Deployment keys are set in the projects `Settings` tab.


[Picture goes here.]

[Picture goes here.]

### Using the Keys

Cloning should now work. 
The trick here is to replace `github.com` with the alias. 

* In the first line of the following examples we're pulling from 
the alias `fluffy-kittens.github.com` instead of the usual `github.com`.
* In the second line we're pulling from 
the alias `puppy-eyes.github.com` instead of the usual `github.com`.


```bash
$ git clone git@fluffy-kittens.github.com:happycatsmiles/fluffy-kittens.git
Cloning into 'fluffy-kittens'...
remote: Counting objects: 7, done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 7 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (7/7), done.
$ git clone git@puppy-eyes.github.com:happycatsmiles/puppy-eyes.git
Cloning into 'puppy-eyes'...
remote: Counting objects: 7, done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 7 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (7/7), done.
```
