# sshd_config

This is a little tighter than the defaults.
This restricts logins to public key access.

It's best to not allow root login at all.
Do this by setting:

```conf
PermitRootLogin no
```

---

```conf
UsePAM yes
PubkeyAuthentication yes
PasswordAuthentication no
ChallengeResponseAuthentication no

PermitRootLogin without-password
PermitEmptyPasswords no

IgnoreRhosts yes
SyslogFacility AUTHPRIV

HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

AuthorizedKeysFile .ssh/authorized_keys
GSSAPIAuthentication no
GSSAPICleanupCredentials no

X11Forwarding yes

# Allow client to pass locale environment variables
AcceptEnv LANG LC_*
AcceptEnv XMODIFIERS

# override default of no subsystems
Subsystem sftp  /usr/libexec/openssh/sftp-server
```

## SFTP

If trying to connect via SFTP produces messages like:

```
SFTP channel opened.
SFTP channel closed by server.
```

The SSH server can't start the SFTP subsystem. 

On Ubuntu 20,
this can be resolved with:

```
$ cd /usr/lib/tmpfiles.d/
$ echo > sshd.conf
d /run/sshd 0755 root root
^D
$ systemd-tmpfiles --create
```

and adding the line

```
Subsystem sftp internal-sftp
```

to the
`/etc/ssh/sshd_config`
file.

Check the configuration file with

```
sshd -T
```
