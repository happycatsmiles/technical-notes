# Keyless ssh Config

This has not be vetted by any expert. 
In theory, this prevents any login attempts without keys. 
Note that this *does* allow for root login with keys.
Logging in directly as root is not the best of practices. 

```
UsePAM yes
PubkeyAuthentication yes
PasswordAuthentication no
ChallengeResponseAuthentication no

PermitRootLogin without-password
PermitEmptyPasswords no

IgnoreRhosts yes
SyslogFacility AUTHPRIV

HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

AuthorizedKeysFile .ssh/authorized_keys
GSSAPIAuthentication no
GSSAPICleanupCredentials no
UsePrivilegeSeparation sandbox          # Default for new installations.


X11Forwarding yes

# Accept locale-related environment variables
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS

# override default of no subsystems
Subsystem sftp  /usr/libexec/openssh/sftp-server

# Added by DigitalOcean build process
ClientAliveInterval 120
ClientAliveCountMax 2
```
