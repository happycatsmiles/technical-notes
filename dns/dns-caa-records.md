# DNS CAA Records

Jan 2, 2019

These improve the security of web sites by providing authorized certificate authorities via DNS. 
Details can be found
[here](https://blog.qualys.com/ssllabs/2017/03/13/caa-mandated-by-cabrowser-forum). 

An example CAA record for letsencrypt.org is: 

```
example.org. CAA 128 issue "letsencrypt.org"
```

The value 128 sets the high bit which is the "considered critical" flag.

## Testing

This can be tested with a service like 
[Qualys' SSL Server Test](https://www.ssllabs.com/ssltest/index.html). 


## GoDaddy DNS

In GoDaddy's DNS manager, the values are:
*   name: <name matching the A record>
*   flags: 128
*   tag: issue
*   value: letsencrypt.org 
