# `React.Component` Typing

The class definition for `React.Component` uses generics to define the type of the component `props` and `state` fields.

```tsx
export class AgentHistory extends React.Component<AgentHistoryParams, AgentViewState> {

  constructor(props: AgentHistoryParams) {
    super(props)
    // This field's type is `AgentViewState`.
    this.state = {
      values: [],
    }
  }
...
}
```
