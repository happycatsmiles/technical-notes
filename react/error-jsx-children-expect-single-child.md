# React Error

The following was an unhelpful error:

> This JSX tag's 'children' prop expects a single child of type 'ReactNode', but multiple children were provided.

The problem code in question:

```tsx
export function Foo() {
  return (
    <p>Something {{variable}}</p>
  )
}
```

The problem is the Angular-style double brackets (`{{}}`).
React uses single brackets. (`{}`).
