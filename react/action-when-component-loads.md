# Performing an Action When a Component Loads

## Synchronous Version

```tsx
  componentDidMount() {
    // Perform synchronous task.
  }

```

## Asynchronous Version

```tsx
  async componentDidMount() {
    // Perform asynchronous task, for example:
    await this.loadData()
  }
```
