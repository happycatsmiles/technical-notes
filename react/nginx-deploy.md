# Deploying a React App to nginx

If a React app uses routing, 
the nginx configuration needs to be able to 
redirect internal routes to the `index.hmtl` with the React app.

```
# This is the root location for a React app.

index index.html;

location / {
  # First attempt to serve request as file.
  # Try to deliver /index.html which is the React app.
  # Fall back to displaying a 404.
  try_files $uri /index.html =404;
}
```
