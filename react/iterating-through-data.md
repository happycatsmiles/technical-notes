# Iterating Through Data

If one wishes to display a list of data that's in array,
it can be useful to have two React components:

* a container for the list
* a component that handles displaying the data

## Example

For example, if one calls an API to retrieve a list of people,
one might receive data that looks like:

```json
[
  { "id": 1, "name": "Alice", "description": "Person #1"},
  { "id": 2, "name": "Bob", "description": "Person #2"},
  { "id": 3, "name": "Celeste", "description": "Person #3"}
  { "id": 4, "name": "Dan", "description": "Person #4"}
]
```

Each of the items in the array can be described thus:

```ts
export interface PersonDto {
  id: string
  name: string
  description: string
}
```

### The Component To Display The Element

This is an example of a component that displays the data from the API:

```ts
interface ShowPersonProps {
  data: PersonDto
}

export function ShowPerson(props: ShowPersonProps) {
  return (
    <li>
      Name: {props.data.name}
      Description: {props.data.description}
    </li>
  )
}
```

### The Component To Display The List

This is an example of a parent component that displays a list of the data.

A typical way of producing a list of repeated components
is to use the array's `map()` method to convert each element to a component.

Note that React needs to have a unique identifier so it can handle creating updating the screen efficiently.
It provides the `key` attribute to receive the unique identifier.
If the data elements have a unique identifier, then it's convenient to use that.

```ts
interface ShowPeopleProps {
  list: PersonDto[]
}

export function ShowPeople(props: ShowPeopleProps) {
  return (
    <h1>People</h1>
    <ul>
      {props.list.map(x => <ShowPerson key={x.id} data={x}/>)}
    </ul>
  )
}
```
