# Create New React App

## Basic App Creation

```sh
npx create-react-app <app-name> --template typescript
```

### TypeScript Changes

To be able to use things like `for(const i of ...)`,
the `.tsconfig` needs to be tweaked.

Add: 

```json
    "downlevelIteration": true,
```

It may be useful to update the target, etc:

```json
{
    "lib": [
      "es2020",
      "dom",
      "dom.iterable"
    ],
    "module": "es2020",
    "target": "es2020"
}
```

## Add SASS

```sh
npm i -D sass
```

## Homepage

There's a setting for the relative homepage location.
To be able to mount the app in a location other than root,
add the following to the `package.json`:

```json
  "homepage": ".",
```

## Common Styling

If there's global common styling,
include these in `index.tsx`:

```tsx
import React from 'react'
import ReactDOM from 'react-dom/client'

import './index.scss'
import './styling/base-theme.scss'
import { App } from './App'

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement,
)
root.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
)
```

## Errors Including Components

I've encountered problems with:

```ts
include App from './App'
```

This may need to be changed to:

```ts
include { App } from './App'
```
