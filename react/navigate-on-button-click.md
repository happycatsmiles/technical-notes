# Navigate On Button Click

The `react-router-dom` has a factory for the function to use to navigate.
`userNavigate()` produces a function to call.

```tsx
import React from 'react'
import { useNavigate } from 'react-router-dom'

import { AgentRowProps } from '../../types/agent-row-props'

export function AgentRow(props: AgentRowProps) {
  const record = props.record
  const navigate = useNavigate()
  return (
    <tr>
      <td>
        {record.uuid}
      </td>
      <td>
        {record.username}
      </td>
      <td>
        {record.displayName}
      </td>
      <td>
        <button onClick={() =>
          navigate(`/agents/${encodeURIComponent(record.uuid)}`)
        }>
          🔍
        </button>
      </td>
    </tr>
  )
}
```
