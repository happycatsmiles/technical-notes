# Basic Elements

| Name          | Web                     | Description                                                                                           | Docs                                     |
| :------------ | :---------------------- | :---------------------------------------------------------------------------------------------------- | :--------------------------------------- |
| <View>        | A non-scrolling <div>   | A container that supports layout with flexbox, style, some touch handling, and accessibility controls | https://reactnative.dev/docs/view        |
| <Text>        | <p>                     | Displays, styles, and nests strings of text and even handles touch events                             | https://reactnative.dev/docs/text        |
| <Image>       | <img>                   | Displays different types of images                                                                    | https://reactnative.dev/docs/image       |
| <TextInput>   | <input type="text">     | Allows the user to enter text                                                                         | https://reactnative.dev/docs/textinput   |
| <ScrollView>  | <div>                   | A generic scrolling container that can contain multiple components and views                          | https://reactnative.dev/docs/scrollview  |
| StyleSheet    | CSS                     | CSS-like abstraction layer                                                                            | https://reactnative.dev/docs/stylesheet  |
| <Button>      | <button>                | A basic button component that should render nicely on any platform.                                   | https://reactnative.dev/docs/button      |
| <Switch>      | <input type="checkbox"> | requires an onValueChange callback that updates the value prop                                        | https://reactnative.dev/docs/switch      |
| <FlatList>    | <ul>                    | Scrollable lists                                                                                      | https://reactnative.dev/docs/flatlist    |
| <SectionList> | <h1>+<ul>               | Scrollable list with section headings                                                                 | https://reactnative.dev/docs/sectionlist |

## Other Elements

https://reactnative.dev/docs/components-and-apis



