# Create React App With Typescript

```sh
npx create-react-app my-app --template typescript
```

## Existing Projects

To add this to an existing project directory:

1. `cd` to the parent directory.
2. Run the `npx` command with the project directory name instead of `my-app` above.

## Example

If one creates a new repo `much-good` and clones it
into `/home/foo/projects`,
one can do the following:

```sh
$ pwd
/home/foo/projects/much-good
$ cd ..
$ npx create-react-app much-good --template typescript
$ cd much-good
```
