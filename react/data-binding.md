# Data Binding

React doesn't support the Angular concept of data binding,
but a similar effect can be had using the `onChange` and `value` attributes.

```tsx
...

render(): ReactNode {
    return (
        <input onChange={x => this.handleSearchInputChange(x)}
                value={this.state.filter}/>
    )
  }

  private handleSearchInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    // Update `this.state.filter` here, which will be reflected in the `value` property.
    ...
  }
```
