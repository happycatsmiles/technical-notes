# Setting the Basename Using `react-router-dom`

## Summary

The react build system appears to use NodeJS as a tool.
This allows one to inject environment variables into the React app during build time.
These environment variable settings get baked into the resulting build.

## Dependencies

This technique uses a developer dependency `env-cmd`.

```sh
$ npm i -D env-cmd
```

## Create Environment Files

1. Create a directory to hold the environment files in.
For example `environments` under the project root.

2. Create files for each platform, e.g. dev, production, etc.

```sh
$ ls environments
dev.env prod.env
```

3. Create an environment variable for the basename.

**NOTE:** All environment files must start with `REACT_APP_`,
otherwise they will be ignored.

```sh
$ cat environments/dev.env
REACT_APP_BASENAME="/"
$ cat environments/prod.env
REACT_APP_BASENAME="/online"
$
```

## Add Environment-Specific Commands To `package.json`.

```json
  "scripts": {
    "start": "react-scripts start",
    "start:dev": "env-cmd -f ./environments/dev.env react-scripts start",
    "start:prod": "env-cmd -f ./environments/prod.env react-scripts start",
    "build": "react-scripts build",
```

## Access Environment Variables Inside React App

`createBrowserRouter` takes optional options:

```tsx
export const router = createBrowserRouter([
    {
      path: '/',
      element: <TopLevelPage/>,
      errorElement: <ErrorPage/>,
    },
  ],
  {
    basename: process.env.REACT_APP_BASENAME,
  },
)
```

```tsx
  console.group('ErrorPage')
  console.debug('props', props)
  console.error('error', error)
  console.error('BASENAME', {basename: process.env.REACT_APP_BASENAME})
  console.groupEnd()
```

## API Root

```ts
class UrlSegments {
  public static readonly AgentsView = '/agents-view'
  public static readonly ArrivalDepartureView = '/arrival-departure-view'
}

export class Urls {
  public static readonly ApiBase = process.env.REACT_APP_API_BASE

  public static readonly AgentsView = `${Urls.ApiBase}${UrlSegments.AgentsView}`
  public static readonly AgentsViewByUuid = (uuid: string) =>
    `${Urls.ApiBase}${UrlSegments.AgentsView}/${encodeURIComponent(uuid)}`
}
```

## Sources

* https://medium.com/geekculture/multiple-environments-in-create-react-app-619bd3dc1c61
