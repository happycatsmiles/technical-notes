# Don't Render Anything

To render nothing, use `null`.
This has the same effect as `*ngIf` in Angular.

```tsx
import React from 'react'

import { ArrivalDepartureValues } from '../../types/arrival-departure-values'

export function OnlineDisplay(props: ArrivalDepartureValues) {
  return props.values.length > 0
    ? (
      <div>
      ... stuff goes here ...
      </div>
    )
    // Don't render anything.
    : null
}
```
