# Fragments

## Summary

A React fragment is somewhat analogous to `<ng-container>`,
in that it doesn't generate a DOM node.

```tsx
render() {
  return (
    <React.Fragment>
      <ChildA />
      <ChildB />
      <ChildC />
    </React.Fragment>
  );
}
```

Or, if there are no keys or attributes, the following shorthand may be used:

```tsx
render() {
  return (
    <>
      <ChildA />
      <ChildB />
      <ChildC />
    </>
  );
}
```

### Keyed Fragments

Where keys are needed, such as with multiple sibling components,
one may pass `key={...}` to `<React.Fragment>`.
This is not valid with the shorthand notation `<>`.

```tsx
function Glossary(props) {
  return (
    <dl>
      {props.items.map(item => (
        // Without the `key`, React will fire a key warning
        <React.Fragment key={item.id}>
          <dt>{item.term}</dt>
          <dd>{item.description}</dd>
        </React.Fragment>
      ))}
    </dl>
  );
}
```

## Source

* [Official React Documentation](https://reactjs.org/docs/fragments.html)
