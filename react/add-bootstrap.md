# Add Bootstrap to React

There are a couple of NPM packages:

```sh
npm i -S react-bootstrap bootstrap
```

The styling needs to be included,
likely in the top-level component (e.g. `App.scss`):

```ts
// https://react-bootstrap.github.io/getting-started/introduction/

/* The following line can be included in a src/App.scss */

@import "~bootstrap/scss/bootstrap";
```

## Source

https://react-bootstrap.github.io/getting-started/introduction/
