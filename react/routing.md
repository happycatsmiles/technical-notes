# React Routing

Notes for adding routing to a React app.

## Dependencies

```sh
npm i react-router-dom
```

## Router Root

The router is initialized by calling `createBrowserRouter`.
This creates the routes,
so it may be useful to put this in a file,
for example,
`./routes/routes.tsx`:

```tsx
export const router = createBrowserRouter([
    {
      path: '/',
      element: <Root/>,
      errorElement: <ErrorPage/>,
    },
  ],
  {
    basename: process.env.REACT_APP_BASENAME,
  },
)
```

This needs to be included into the `index.tsx`.

```tsx
import React from 'react'
import ReactDOM from 'react-dom/client'
import { RouterProvider } from 'react-router-dom'

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement,
)
root.render(
  <React.StrictMode>
    <ErrorBoundary>
      <RouterProvider router={router}/>
    </ErrorBoundary>
  </React.StrictMode>,
)
```

## Error Handling

One may catch errors in "boundaries":

```tsx
import React, { ErrorInfo } from 'react'

export class ErrorBoundary extends React.Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state = {hasError: false}
  }

  static getDerivedStateFromError(error: any) {
    // Update state so the next render will show the fallback UI.
    return {hasError: true}
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    // You can also log the error to an error reporting service
    console.error(error, errorInfo)
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>
    }
    return this.props.children
  }
}
```

## Router Outlet

The `<Outlet/>` appears to be in concept to the Angular outlet.

```tsx
import { Outlet } from 'react-router-dom'

export function Root() {
  return (<Outlet/>)
}
```

## Children

```tsx

export const router = createBrowserRouter([
    {
      path: '/',
      element: <Root/>,
      errorElement: <ErrorPage/>,
      children: [
        {
          path: '',
          element: <TopLevelPage/>,
        },
        {
          path: 'search',
          element: <SearchPage/>,
        },
        {
          path: 'agents/:uuid',
          element: <AgentViewPage/>,
          loader: agentViewLoader,
        },
      ],
    },
  ],
  {
    basename: process.env.REACT_APP_BASENAME,
  },
)
```

## Loader

A loader fetches data:

```tsx
import { AgentsViewDto } from '../../lib/dtos/agents-view.dto'
import { AgentViewLoaderParams } from '../types/agent-view-loader-params'
import { Urls } from '../../lib/urls'

export async function agentViewLoader(values: AgentViewLoaderParams): Promise<AgentsViewDto | null> {
  const uuid = values.params['uuid']
  if (!uuid) return null

  const response = await fetch(Urls.AgentsViewByUuid(uuid))
  if (!response.ok) return null

  return await response.json()
}
```
