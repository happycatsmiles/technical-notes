# Nested Routes

Nested routes can be gathered into its own `<Routes>` component and 
used as the element of the parent route.

For example the following nested routes are bundled together:

```tsx
import React from 'react'
import { Route, Routes } from 'react-router-dom'

import { CategoriesItemCreate } from '../categories/categories-item-create'
import { CategoriesItemEdit } from '../categories/categories-item-edit'
import { CategoriesList } from '../categories/categories-list'
import { CategoriesItemView } from '../categories/categories-item-view'
import { CategoriesItemDelete } from '../categories/categories-item-delete'

export const RoutingCategories = () => {
  return (
    <Routes>
      <Route path="create" element={<CategoriesItemCreate/>}/>
      <Route path=":cid/view" element={<CategoriesItemView/>}/>
      <Route path=":cid/update" element={<CategoriesItemEdit/>}/>
      <Route path=":cid/delete" element={<CategoriesItemDelete/>}/>
      <Route path="*" element={<CategoriesList/>}/>
    </Routes>
  )
}
```

And fit into the overall tree thus:


```tsx
export function RoutingRoot() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/categories/*" element={<RoutingCategories/>}/>
        <Route path="/agents/*" element={<RoutingAgents/>}/>
        <Route path="*" element={<HomePage/>}/>
      </Routes>
    </BrowserRouter>
  )
}
```