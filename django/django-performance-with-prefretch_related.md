# Django performance with `prefetch_related`

For better Django performance, `.prefetch_related('field1', 'field2', ...)` will include these related tables in the query, eliminating the need for further queries when accessing data. 
