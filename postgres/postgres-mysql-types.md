# Postgres vs MySQL Types

| MySQL     | Postgres      |
| :-------- | :------------ |
| CHAR      | TEXT          | Note: Use TEXT!
| VARCHAR   | TEXT          |
| TEXT      | TEXT          |
| numeric   | NUMERIC(a, b) | Note: Always define sizes |
| TIMESTAMP | TIMESTAMPZ    |

