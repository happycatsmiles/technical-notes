# PostgreSQL Autoincrement Field

## Overview

PostgreSQL three `serial` convenience types:

| Type        | Description                 |
| :---------- | :-------------------------- |
| smallserial | signed 16-bit autoincrement |
| serial      | signed 32-bit autoincrement |
| bigserial   | signed 64-bit autoincrement |

```sql
CREATE TABLE examples (
   id SERIAL PRIMARY KEY
);
```

## Not True Types

The
[documentation](https://www.postgresql.org/docs/current/datatype-numeric.html#DATATYPE-SERIAL)
states these create sequences behind the scenes:

> The data types smallserial, serial and bigserial are not true types, but merely a notational convenience for creating unique identifier columns (similar to the AUTO_INCREMENT property supported by some other databases). In the current implementation, specifying:

```sql
CREATE TABLE tablename (
    id SERIAL
);
```

is equivalent to specifying:

```sql
CREATE SEQUENCE tablenam_id_seq;
CREATE TABLE tablename (
    id integer NOT NULL DEFAULT nextval('tablename_id_seq')
);
ALTER SEQUENCE tablename_id_seq OWNED BY tablename.id;
```

## Manual Insertions / Sequence Collisions

If one manually inserts values into the `serial` field,
the sequence backing the primary key is not updated.

Given the table

```sql
CREATE TABLE examples (
   id SERIAL PRIMARY KEY,
   name character varying(255)
);
```

the following SQL will fail because the next sequence value is `1`:

```sql
insert into example.tablename(id, name) values (1, 'foo');
insert into example.tablename(id, name) values (2, 'bar');
insert into example.tablename(name) values ('baz');


ERROR:  duplicate key value violates unique constraint "tablename_pkey"
DETAIL:  Key (id)=(1) already exists.
SQL state: 23505
```

## Fixing Primary Key Sequences

### Manually

To correct sequences that are behind table values,
the
[documentation](https://www.postgresql.org/docs/current/sql-createsequence.html)
states:

```sql
SELECT SETVAL('example.tablename_id_seq', MAX(id)) FROM example.tablename;
```

### Bulk Fixing

The generate the SQL to fix all sequences in a database,
use the following script.
The generated SQL must then be run against the database.

```sql
SELECT 'SELECT SETVAL(' 
    || quote_literal(quote_ident(PGT.schemaname) || '.' || quote_ident(S.relname)) 
    || ', COALESCE(MAX(' 
    || quote_ident(C.attname) || '), 1) ) FROM ' || quote_ident(PGT.schemaname)
    || '.'
    || quote_ident(T.relname)
    || ';'
FROM pg_class AS S,
    pg_depend AS D,
    pg_class AS T,
    pg_attribute AS C,
    pg_tables AS PGT
WHERE S.relkind = 'S'
    AND S.oid = D.objid
    AND D.refobjid = T.oid
    AND D.refobjid = C.attrelid
    AND D.refobjsubid = C.attnum
    AND T.relname = PGT.tablename
ORDER BY S.relname;
```

## References

[Using Autoincrement](https://www.tutorialspoint.com/postgresql/postgresql_using_autoincrement.htm)

[Serial Data Type](https://www.postgresql.org/docs/current/datatype-numeric.html#DATATYPE-SERIAL)

[Fixing Sequences](https://wiki.postgresql.org/wiki/Fixing_Sequences)

[CREATE SEQUENCE](https://www.postgresql.org/docs/current/sql-createsequence.html)
