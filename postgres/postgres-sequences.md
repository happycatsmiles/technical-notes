# PostgreSQL Sequences

## Quick Takes

* Default to 64 bits.
* The `OWNED BY` option tells PostgreSQL to drop the sequence if the owning table is dropped.
* Can wrap around with the `CYCLE` option.
* Values are never rolled back in transactions. (See the big **Caution** in the documentation.)

## Functions

* `nextval(<sequence>)` returns the next value.
* `currval(<sequence>)` returns the last value provided.
* `setval(<sequence>)` sets the current value. Setting this to 42 will produce 43 on `nextval`.
  * There's a way to override this in the documentation.

## Refrences

[Sequence Manipulation Functions](https://www.postgresql.org/docs/current/functions-sequence.html)
