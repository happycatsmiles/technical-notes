# PostgreSQL Timestamp

## Don't Use `timestamp`

**Caution:** 
[timestamp doesn't do what you think it does.](https://wiki.postgresql.org/wiki/Don%27t_Do_This#Don.27t_use_timestamp_.28without_time_zone.29)

Use `timestamptz` instead.

