# PostgreSQL Strings

According to the PostgreSQL wiki,
[don't use varchar(n) by default](https://wiki.postgresql.org/wiki/Don%27t_Do_This#Don.27t_use_varchar.28n.29_by_default)

Use the `text` field for text columns instead of `varchar(n)`.

* There is no performance or storage penalty in PostgreSQL.
* Use `text` with check constraints if an upper limit is truly desired.
* `text` *is* the internal default type.
* Other string types have restrictions and performance penalties.

> Generally, there is **no** downside to using `text` in terms of performance/memory. On the contrary: `text` is the optimum. ... text is literally the "preferred" type among string types in the Postgres type system,
> 
> [Any downsides of using data type “text” for storing strings?](https://stackoverflow.com/questions/20326892/any-downsides-of-using-data-type-text-for-storing-strings/20334221#20334221)
