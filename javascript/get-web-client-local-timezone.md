# Get the Current Time Zone for the Browser

```js
/**
 * This gets the browser client's current timezone.
 *
 * Reference:
 *
 * https://stackoverflow.com/questions/1091372/getting-the-clients-timezone-offset-in-javascript#1091399
 */
export function getClientTimeZone(): string {
  return Intl.DateTimeFormat().resolvedOptions().timeZone
}
```
