# Zero-Padding in JavaScript

Zero padding is a specialized use case with certain assumptions. 

* Zero padding is fixed with (usually for display).
* Numbers are non-negative integers.

Two common methods of zero padding I see are
1)  iteratively adding zeroes to the beginning of the string, or 
2)  using `new Array(n).join('0')` to construct the correct number of zeroes before pepending. 

A benchmark for these methods 
[here](https://gist.github.com/andrewrk/4382935) 
shows that the `new Array()` method is generally quite slow
compared to prepending zeroes.

I would propose one more method that takes advantage of the first assumption, 
where numbers are assumed to be non-negative integers. 

If we can safely assume integers in the range
specified by the ES6 
[`Number.isSafeInteger()` ](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isSafeInteger)
function, 
which as of this writing (2018) 
are up to 16 decimal digits in length,
then we can simplify the two methods to:

```js
function zfill3(number, size) {
  number = '0000000000000000' + number.toString() 
  return number.slice(-size)
}
```

which to my benchmarking is fast enough to be worth considering.

Running 10,000,000 operations:
1)  5.6S for prepending.
2)  15.2S for `new Array()`.
3)  1.6S for my suggestion. 

It can be argued that my suggestion won't work 
for integers out of the range specified by 
[`Number.isSafeInteger()` ](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isSafeInteger).
This is true,
but that violates the assumptions made above,
and you're likely using a numerical package that
supports large precision numbers such as 
[`mathjs`](http://mathjs.org/).
