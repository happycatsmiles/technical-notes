# JavaScript `for await...of`

April 28, 2021

This is a `for...of` loop that
does an `await` for each element in the array.
This allows for array elements to be processed in the order they occur in the array.

This does not wait for all promises to complete first.
It simply awaits each one in turn.
Because the elements are asynchronous,
elements may complete before the `for await...of` loop gets to them.

```js
async function af(delay) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('af finished', delay)
            resolve(delay)
        }, delay)
    })
}

async function test() {
    let aa = [af(300), af(3000), af(2000), af(1000), af(500)]

    for await (i of aa) {
        console.log('for await', i)
    }
}

test()
    .then(() => console.log('Test done.'))
    .catch(err => console.error(err))
```

Console output:

```text
af finished 300
for await 300
af finished 500
af finished 1000
af finished 2000
af finished 3000
for await 3000
for await 2000
for await 1000
for await 500
Test done.
```

## References

* [MDN Documentation](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for-await...of)

