# Cloning Objects in JavaScript

The function [`structuredClone()`](https://developer.mozilla.org/en-US/docs/Web/API/structuredClone) exists as part of the standard JavaScript toolkit now.

This performs a deep copy intelligently.

This will throw an exception if its parameter "is not serializable".
This appears to be:

* Functions
* Symbols

