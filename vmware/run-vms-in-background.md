# Run Virtual Machines in the Background Using VMware Workstation 14

When running VMware Workstation GUI remotely, 
whether via VNC or X11, 
and either the GUI or the login session terminates, 
*the running VMs will terminate*. 

This is true even with the preference option `Keep VMs running after Workstation closes` is selected. 

For example,
when accessing the Workstation GUI on a Linux host using X11 over ssh, 
if you click on the corner close 🗙 the server VMs die. 
If The menu item `File | Quit` is selected, 
the VMs appear to continue running until the SSH session that was tunnelling X11 terminates. 
At this point the servers die again. 

## Shared VMs

The documentation states for VMs to continue running in the background, *they must be shared VMs*. 
The preference option `Keep VMs running after Workstation closes` is for *shared VMs only*.

Ordinary VMs can be turned into shared VMs by 
selecting the VM, 
then selecting the menu item `VM | Manage | Share…`. 

If you choose to move the VM, rather than copy, the virtual NIC's settings will be preserved. 

Once the VM is shared, and is running, 
it will continue to run when the GUI session terminates. 

## Remote Management

Additionally, shared VMs can be managed remotely by:

* Tunnel to the remote host’s port 443.
* Select file menu `File | Connect to Server…`. 
* Use `localhost:`<port> in the `Server name:` input box. 

For example, 
if local port 4443 is forwarded to the remote server’s port 443 over SSH, 
you would enter `localhost:4443` in the `Server name:` input box. 
