# Completing git Rebase Operations

When rebasing in git, one encounters situations where there are conflicts.
The rebase is halted in an incomplete state.
After resolving conflicts, one must instruct git to continue.

## Continue Rebase

In most circumstances, this is what one wants after resolving conflicts.

The docs say:

> Restart the rebasing process after having resolved a merge conflict.

```sh
git rebase --continue
```

## Abort Rebase

The docs say:

> Abort the rebase operation and reset HEAD to the original branch.

```sh
git rebase --abort
```

## Skip Rebase

Some have suggested using `--skip` but it seems to be a tool used with some wisdom.

The docs say:

> Restart the rebasing process by skipping the current patch.

```sh
git rebase --skip
```
