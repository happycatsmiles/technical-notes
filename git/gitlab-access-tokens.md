# Access gitlab using personal authentication tokens.

`https://oauth2:<token>@gitlab.com/path-to-repo.git` 

HTTP uses the form   `<protocol>://<username>:<password>@<hostname>`

The parts are:

* `oauth2` - username
* `<token>` - password
* `gitlab.com/path-to-repo.git` 
  * Be sure to append `.git` to the repo URL.

## Update Git Remote With New Access Token

You can see the current remote with:

```sh
git remote -v
```

One will see a list something like:

```txt
origin  https://oath2:XXXXXXXXXXXXXXXXXXXXXXXXXX@gitlab.com/my-group/my-project.git (fetch)
origin  https://oath2:XXXXXXXXXXXXXXXXXXXXXXXXXX@gitlab.com/my-group/my-project.git (push)
```

Next, remove the existing remote:

```sh
git remote rm origin
```

Next, add the remote back, but with the new access token:

```sh
git remote add origin https://oath2:glpat-YYYYYYYYYYYYYYYYYYYY@gitlab.com/my-group/my-project.git
```

Next, hook up the tracking for the branch again:
**NOTE:** This is for the develop branch.

```sh
 git branch --set-upstream-to=origin/develop develop
```
