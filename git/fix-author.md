# Fix Author of Last Commit

```sh
git commit --amend --reset-author --no-edit
```

## References

* https://stackoverflow.com/questions/3042437/how-can-i-change-the-commit-author-for-a-single-commit

