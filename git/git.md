# git Notes

## Show The Current Remote

```sh
git remote show origin
```

## Set User and E-Mail

```sh
git config user.name "Foo"
git config user.email "foo@bar.com"
```

## View User and E-Mail

```sh
git config user.name
git config user.email
```
