# git-remote

To show just the remote origin URL:

```sh
git config --get remote.origin.url
```

