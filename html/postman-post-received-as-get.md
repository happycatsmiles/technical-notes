# Postman POST received as GET

This is a bit of an obscure "gotcha." 

**Situation:**

1) nginx redirects HTTP to HTTPS with a 301 status code.
2) Postman is doing a POST to `http://...`. 

According to [Postman Issue 450](https://github.com/postmanlabs/postman-app-support/issues/450), 
Postman emulates a web browser's behavior in this case, 
and the POST is stripped of its contents and converted to a GET. 
I have not verified that this is actual web browser behavior or that this is the mechanism, 
but the following solution worked.

**Solution:**

POST to `https://...` instead of `http://...`.
