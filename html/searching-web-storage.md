# Searching With Web Store

<https://protonmail.com/support/knowledge-base/search-message-content/>

The pertinent parts:

## Search message content

ProtonMail uses
[zero-access encryption](https://protonmail.com/blog/zero-access-encryption/)
to store all emails at rest, and emails sent between ProtonMail users are fully end-to-end encrypted. This is good for privacy because it means we cannot read the content of your emails.

You can search the content of your encrypted emails without giving ProtonMail access to those messages. We achieve this by creating a local index of your emails and storing it using your browser’s web storage.

When you perform a search, the app goes through the index of your downloaded emails and highlights all matches.

Everyone that subscribes to a Visionary, Lifetime, or Professional Proton plan will be able to use content search.

### Security

The index is created in your browser and never leaves it, so no one else, including ProtonMail, can access it without physical access to the device your browser is installed on. Each message inside the index is encrypted and can be decrypted only when you are logged in on the device hosting the index.

This means that when you have search message content enabled, but your web browser is closed, an attacker with physical access to your device cannot read the contents of the index file. If you are logged in with your ProtonMail tab open, a decrypted version is stored in memory, but at this point, they would have direct access to your emails, anyway.

For more information on protecting your local data, see our
[threat model](https://protonmail.com/blog/protonmail-threat-model/)
or our article explaining
[how to secure your data](https://protonvpn.com/blog/how-to-secure-online-data/).  

### The index of your emails

To search your messages’ contents while keeping them private, ProtonMail creates an index of your emails in your browser’s web storage. However, this means that:

You must create an index of your messages for each browser on each device that you access ProtonMail with.
You may not be able to search your messages’ contents if you use your browser in Private (Incognito) mode because browsers limit access to their web storage in Private mode. We therefore recommend against using this search if you are using Private mode.
If you sign out of your ProtonMail account, your index will still be available the next time you sign in.
If you clear your browser data, your index will be erased.
