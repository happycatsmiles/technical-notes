# Common Mouse Cursor Styles

Mouse cursor styles can be found at the [MDN pages](https://developer.mozilla.org/en-US/docs/Web/CSS/cursor).

Ones listed on an old sticky note are

1. default (normal pointer)
1. pointer  (hand with finger, something to select)
1. progress  (busy but can still select)
1. wait (busy)
1. text
1. no-drop
1. not-allowed
