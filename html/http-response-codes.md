# HTTP Response Codes

## DELETE

* 200 (OK) - The action succeeded. The body includes a representation describing the status.
* 202 (Accepted) - The action is pending. Success is expected.
* 204 (No Content) - The action succeeded. No data is being returned.

## POST

* 200 (OK) - The response body has the entity.
* 201 (Created) - The headers have the response (e.g. `Location`).
* 204 (No Content) - The action succeeded. No data is being returned.

## PATCH

* 200 (OK) - The response body has the entity.
* 204 (No Content) - The action succeeded. No data is being returned.

## 200

* The action succeeded. Data is being returned.
* Headers such as ETag may be expected.
* Expected to be cacheable.

## 204

* The action succeeded. No data is being returned.
* Headers such as ETag may be expected.
* Expected to be cacheable.

## References

> ...a 200 response always has a payload,
> though an origin server MAY generate a payload body of zero length.
> If no payload is desired, an origin server ought to send 204 (No
> Content) instead.
>
> [Hypertext Transfer Protocol (HTTP/1.1): Semantics and Content](https://datatracker.ietf.org/doc/html/rfc7231#section-6.3.1)

> The 204 response code is used because the response does not carry a
> message body (which a response with the 200 code would have).  Note
> that other success codes could be used as well.
>
> [PATCH Method for HTTP, RFC-5789](https://datatracker.ietf.org/doc/html/rfc5789#section-2.1)

* [DELETE on MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/DELETE)

* 204 response code in [Hypertext Transfer Protocol (HTTP/1.1): Semantics and Content](https://datatracker.ietf.org/doc/html/rfc7231#section-6.3.5)
