# HTTP Problem Details

[RFC7807](https://datatracker.ietf.org/doc/html/rfc7807)
proposes a standard way of returning more helpful error information.

## Required Parts

### Content-Type

The new `application/problem+json` type indicates a problem detail result.

### Json Fields

* `type` (string)
  * Effectively required
  * This is the primary identifier for the problem type.
* `title` (string)
  * Effectively required
  * This is a human-readable version for `type`.
  * It shouldn't change from occurrence to occurrence of the problem.
* `status` (number)
  * Optional
  * Should match the returned status code.
  * This is for convenience of the consuming software.
* `detail` (string)
  * Optional
  * Human-readable explanation specific to this occurrence of the problem.
* `instance` (string)
  * Optional
  * A URI that identifies the specific occurrence of the problem.

## Example Returns

### Example 1

Here, the out-of-credit problem (identified by its type URI)
indicates the reason for the 403 in "title", gives a reference for
the specific problem occurrence with "instance", gives occurrence-
specific details in "detail", and adds two extensions; "balance"
conveys the account's balance, and "accounts" gives links where the
account can be topped up.

The ability to convey problem-specific extensions allows more than
one problem to be conveyed.

```http
HTTP/1.1 403 Forbidden
Content-Type: application/problem+json
Content-Language: en

{
    "type": "https://example.com/probs/out-of-credit",
    "title": "You do not have enough credit.",
    "detail": "Your current balance is 30, but that costs 50.",
    "instance": "/account/12345/msgs/abc",
    "balance": 30,
    "accounts": ["/account/12345",
                    "/account/67890"]
}
```

### Example 2

The ability to convey problem-specific extensions allows more than
one problem to be conveyed.  For example:

```http
HTTP/1.1 400 Bad Request
Content-Type: application/problem+json
Content-Language: en

{
"type": "https://example.net/validation-error",
"title": "Your request parameters didn't validate.",
"invalid-params": [ {
                        "name": "age",
                        "reason": "must be a positive integer"
                    },
                    {
                        "name": "color",
                        "reason": "must be 'green', 'red' or 'blue'"}
                    ]
}
```
