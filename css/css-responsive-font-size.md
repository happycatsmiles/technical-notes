# Responsive CSS Font Size

[This article](https://bits.theorem.co/css-pro-tips-responsive-font-sizes-and-when-to-use-which-units/)
suggests

```css
html {
  font-size: calc(1em + 1vw)
}
```

Where the scaling factor for `vw` controls the overall size.
