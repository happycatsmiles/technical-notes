# Vertically Centring Text

> The correct way to do this in modern browsers is to use Flexbox.

For example,
if text is in a `<div>`:

```css
div {
    height: 100%;
    display: flex;
    align-items: center;
}
```

## References

[StackOverflow](https://stackoverflow.com/questions/2939914/how-do-i-vertically-align-text-in-a-div#2939979)
