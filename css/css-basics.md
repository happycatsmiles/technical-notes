# CSS Basics

| CSS              | Note                                     |
| :--------------- | :--------------------------------------- |
| `div + p`        | Adjacent siblings                        |
| `div ~ p`        | All `p` with `div` sibling.              |
| `p:first-child`  |
| `p:last-child`   |
| `p:nth-child(2)` | Even odd                                 |
| `p[attr]`        | `p` with attribute `attr`                |
| `p[attr="x]`     | `p` with attribute `attr` with value `x` |
