# CSS Grid Elements Appearing in the Wrong Place

2021-04-25

I ran into a situation where all of the grid elements were piled into the last grid element.

```css
.container {
display: grid;
grid-template-columns: 40px 1fr 120px;
grid-template-areas:
    "a b c"
    "d d";
}
```

Note that in `grid-template-areas`
the first line has all three columns defined.
The second line has only two.

The grid template areas were not processed correctly and
all of the elements piled into the first available element,
i.e. in the last grid element.

## Lesson

Ensure that everything in  `grid-template-areas`
has every column defined,
even if it's empty (`" . "`).
