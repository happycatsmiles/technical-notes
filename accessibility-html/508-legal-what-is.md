# Section 508

## What is Section 508

* The Rehabilitation Act of 1973 requires access to programs and activities that are funded by Federal agencies and to Federal employment.
  * Section 502 establishes the Access Board.
  * Section 508 sets requirements to access electronic information and technology in the Federal sector.
* The [U.S. Access Board](https://www.access-board.gov/)
  * "The Access Board is an independent federal agency that promotes equality for people with disabilities through leadership in accessible design and the development of accessibility guidelines and standards." [cite](https://www.access-board.gov/law/ra.html)
* The law
  * "The law applies to all Federal agencies when they develop, procure, maintain, or use electronic and information technology. Federal agencies must ensure that this technology is accessible to employees and members of the public with disabilities to the extent it does not pose an “undue burden.” Section 508 speaks to various means for disseminating information, including computers, software, and electronic office equipment. It applies to, but is not solely focused on, Federal pages on the Internet or the World Wide Web. It does not apply to web pages of private industry."
  * "The Board is responsible for developing accessibility standards for such technology for incorporation into regulations that govern Federal procurement practices. The net result will be that Federal agencies will have to purchase electronic and information technology that is accessible except where it would cause an “undue burden.”"
  * "The law also provides a complaint process under which complaints concerning access to technology will be investigated by the responsible Federal agency."
* The standards are constantly evolving.
* The main standards and guidelines may be found [here](https://www.access-board.gov/ict/).
  * This provides links to several categories:
    * [Section508.gov](https://www.section508.gov/) — GSA’s Government-wide IT Accessibility Program
    * [Comparison Table of WCAG 2.0 to Original 508 Standards](https://www.access-board.gov/ict/wcag2ict.html)
    * [Mapping of WCAG 2.0 to Functional Performance Criteria](https://www.access-board.gov/ict/wcagtofpc.html)
    * [ICT Testing Baseline for Web Accessibility](https://ictbaseline.access-board.gov/)

## WGAC 2.0

* Th Access Board states, "The Revised Section 508 Standards reference the World Wide Web Consortium’s (W3C) Web Content Accessibility Guidelines (WCAG) 2.0, Level (A) and (AA) Success Criteria and conformance requirements as the “new” Section 508 standards for Web content." [cite](https://ictbaseline.access-board.gov/introduction/#background)
* The W3C (World Wide Web Consortium) has an evolving set of guidelines.
* [Web Content Accessibility Guidelines (WCAG) 2.0](https://www.w3.org/TR/WCAG20/)
* Guidelines on meeting the criteria https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0

## Questions

* Section 508 is a part of Federal law.
  * Do we need input from the legal side?
  * When the State is subjected to a lawsuit over 508 compliance,
    what is needed by the State to walk into a courtroom and lay out their case confidently?
  * Is this a concern for us?
  * To what degree is this a concern?
* Who determines what "compliance" consists of?

# Text of Section 508 of the Rehabilitation Act of 1973, as amended (29 U.S.C. §794d)

* §794d. Electronic and information technology
  * (a) Requirements for Federal departments and agencies
    * (1) Accessibility
      * (A) Development, procurement, maintenance, or use of electronic and information technology
        * When developing, procuring, maintaining, or using electronic and information technology, each Federal department or agency, including the United States Postal Service, shall ensure, unless an undue burden would be imposed on the department or agency, that the electronic and information technology allows, regardless of the type of medium of the technology –
          * (i) individuals with disabilities who are Federal employees to have access to and use of information and data that is comparable to the access to and use of the information and data by Federal employees who are not individuals with disabilities; and
          * (ii) individuals with disabilities who are members of the public seeking information or services from a Federal department or agency to have access to and use of information and data that is comparable to the access to and use of the information and data by such members of the public who are not individuals with disabilities.
      * (B) Alternative means efforts
        * When development, procurement, maintenance, or use of electronic and information technology that meets the standards published by the Access Board under paragraph (2) would impose an undue burden, the Federal department or agency shall provide individuals with disabilities covered by paragraph (1) with the information and data involved by an alternative means of access that allows the individual to use the information and data.
    * (2) Electronic and information technology standards
      * (A) In general
        * Not later than 18 months after August 7, 1998, the Architectural and Transportation Barriers Compliance Board (referred to in this section as the ‘Access Board’), after consultation with the Secretary of Education, the Administrator of General Services, the Secretary of Commerce, the Chairman of the Federal Communications Commission, the Secretary of Defense, and the head of any other Federal department or agency that the Access Board determines to be appropriate, including consultation on relevant research findings, and after consultation with the electronic and information technology industry and appropriate public or nonprofit agencies or organizations, including organizations representing individuals with disabilities, shall issue and publish standards setting forth–
          * (i) for purposes of this section, a definition of electronic and information technology that is consistent with the definition of information technology specified in section 5002(3) of the Clinger-Cohen Act of 1996 (40 U.S.C. 1401(3)); and
          * (ii) the technical and functional performance criteria necessary to implement the requirements set forth in paragraph (1).
      * (B) Review and amendment
        * The Access Board shall periodically review and, as appropriate, amend the standards required under subparagraph (A) to reflect technological advances or changes in electronic and information technology.
    * (3) Incorporation of standards
      * Not later than 6 months after the Access Board publishes the standards required under paragraph (2), the Federal Acquisition Regulatory Council shall revise the Federal Acquisition Regulation and each Federal department or agency shall revise the Federal procurement policies and directives under the control of the department or agency to incorporate those standards. Not later than 6 months after the Access Board revises any standards required under paragraph (2), the Council shall revise the Federal Acquisition Regulation and each appropriate Federal department or agency shall revise the procurement policies and directives, as necessary, to incorporate the revisions.
    * (4) Acquisition planning
      * In the event that a Federal department or agency determines that compliance with the standards issued by the Access Board under paragraph (2) relating to procurement imposes an undue burden, the documentation by the department or agency supporting the procurement shall explain why compliance creates an undue burden.
    * (5) Exemption for national security systems
      * This section shall not apply to national security systems, as that term is defined in section 5142 of the Clinger-Cohen Act of 1996 (40 U.S.C. 1452).
    * (6) Construction
      * (A) Equipment
        * In a case in which the Federal Government provides access to the public to information or data through electronic and information technology, nothing in this section shall be construed to require a Federal department or agency –
          * (i) to make equipment owned by the Federal Government available for access and use by individuals with disabilities covered by paragraph (1) at a location other than that where the electronic and information technology is provided to the public; or
          * (ii) to purchase equipment for access and use by individuals with disabilities covered by paragraph (1) at a location other than that where the electronic and information technology is provided to the public.
      * (B) Software and peripheral devices
        * Except as required to comply with standards issued by the Access Board under paragraph (2), nothing in paragraph (1) requires the installation of specific accessibility_related software or the attachment of a specific accessibility_related peripheral device at a workstation of a Federal employee who is not an individual with a disability.
  * (b) Technical assistance
    * The Administrator of General Services and the Access Board shall provide technical assistance to individuals and Federal departments and agencies concerning the requirements of this section.
  * (c) Agency evaluations
    * Not later than 6 months after August 7, 1998, the head of each Federal department or agency shall evaluate the extent to which the electronic and information technology of the department or agency is accessible to and usable by individuals with disabilities described in subsection (a)(1), compared to the access to and use of the technology by individuals described in such subsection who are not individuals with disabilities, and submit a report containing the evaluation to the Attorney General.
  * (d) Reports
    * (1) Interim report
      * Not later than 18 months after August 7, 1998, the Attorney General shall prepare and submit to the President a report containing information on and recommendations regarding the extent to which the electronic and information technology of the Federal Government is accessible to and usable by individuals with disabilities described in subsection (a)(1).
    * (2) Biennial reports
      * Not later than 3 years after August 7, 1998, and every 2 years thereafter, the Attorney General shall prepare and submit to the President and Congress a report containing information on and recommendations regarding the state of Federal department and agency compliance with the requirements of this section, including actions regarding individual complaints under subsection (f).
  * (e) Cooperation
    * Each head of a Federal department or agency (including the Access Board, the Equal Employment Opportunity Commission, and the General Services Administration) shall provide to the Attorney General such information as the Attorney General determines is necessary to conduct the evaluations under subsection (c) and prepare the reports under subsection (d).
  * (f) Enforcement
    * (1) General
      * (A) Complaints
        * Effective 6 months after the date of publication by the Access Board of final standards described in subsection (a) (2), any individual with a disability may file a complaint alleging that a Federal department or agency fails to comply with subsection (a)(1) of this section in providing electronic and information technology.
      * (B) Application
        * This subsection shall apply only to electronic and information technology that is procured by a Federal department or agency not less than 6 months after the date of publication by the Access Board of final standards described in subsection (a) (2).
    * (2) Administrative complaints
      * Complaints filed under paragraph (1) shall be filed with the Federal department or agency alleged to be in noncompliance. The Federal department or agency receiving the complaint shall apply the complaint procedures established to implement section 794 of this title for resolving allegations of discrimination in a federally conducted program or activity.
    * (3) Civil actions
      * The remedies, procedures, and rights set forth in sections 794a(a)(2) and 794a(b) of this title shall be the remedies, procedures, and rights available to any individual with a disability filing a complaint under paragraph (1).
  * (g) Application to other Federal laws
    * This section shall not be construed to limit any right, remedy, or procedure otherwise available under any provision of Federal law (including sections 791 through 794(a) of this title) that provides greater or equal protection for the rights of individuals with disabilities than this section.
