# Accessibility: ARIA

## Overview

> Accessible Rich Internet Applications (ARIA) is a set of attributes that define ways to make web content and web applications (especially those developed with JavaScript) more accessible to people with disabilities.
>
> [MDN entry for ARIA](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA)

HTML 5 provides much functionality in elements.
These should be used rather than AIRA when possible.

> Addition of ARIA semantics only exposes extra information to a browser's accessibility API, and does not affect a page's DOM.
> 
> [MDN entry for Using ARIA: Roles, states, and properties](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques)

## Roles

* Many HTML elements may use the `role="..."` attribute to make the intent of the element more explicit.
* Some elements have implicit roles in certain contexts.
  * For example, `<header>` implies `role="banner"` *when it's a descendant of `<body>`.
  * [Reference](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/header#accessibility)

Role categories:

* Widget Roles
  * Buttons, menus, switches, tabs, etc.
* Composite Roles
  * Combobox, listbox, tablist, treegrid, etc.
* Document Structure Roles
* Landmark Roles
  * main, form, navigation, search, etc.
* Live Region Roles
  * alert, marquee, status, etc.
* Window Roles
  * dialogs, etc.

Details may be found on at the [MDN entry for ARIA Roles](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles).

## States and Properties

ARIA states and properties give additional information about state and relationships between elements.

* Widget Attributes
  * checked, readonly, required, label, etc.
* Live Region Attributes
* Relationship Attributes
  * labelledby, errormessage, etc.

## Keyboard Navigation

* Web apps must be navigable via keyboard only.
* See also the [MDN page "An overview of accessible web applications and widgets"](https://developer.mozilla.org/en-US/docs/Web/Accessibility/An_overview_of_accessible_web_applications_and_widgets#keyboard_navigation).
