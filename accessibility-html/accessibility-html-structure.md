# Accessibility: HTML Structure

## Semantic Markup

* One can communicate the intent of regions sections of a page visually without semantic markup.
  However, non-visual technology such as screen readers cannot determine this intent.
* One may communicate the intended structure of a page with *semantic markup*.
* The [MDN page for Content Sectioning](https://developer.mozilla.org/en-US/docs/Web/HTML/Element#content_sectioning) has a list of elements and their use.

### Headings

Content should use headings to logically organize content.
There should be *one* `<h1>` header to announce the intent of the entire page.

* Headings should be nested in numeric order,
  i.e. `<h1>`, `<h2>`, `<h3>`, `<h4>`, etc.
* Remember these describe page structure,
  *not* layout.
* Do *not* use headings for visual formatting;
  override styling it with CSS.
* If a header should not appear visually,
  hide it with CSS.
  (See [Markup Exclusively For Assistive Technology](#markup-exclusively-for-assistive-technology).)

### Navigation

* Navigation sections allow screen readers to present regions of the screen as navigable links,
  allowing the user to determine whether they need to wade through the details of that section.
* Be mindful that the `<nav>` element should encompass
  the whole section of navigation information.
  This may be a list of links, or encompass other components.
* Sections like `<footer>` imply navigation links may exist,
  so `<nav>` within a `<footer>` provides no additional semantic information.
* [MDN Reference for `<nav>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/nav)
* [What WG Reference for `<nav>`](https://html.spec.whatwg.org/multipage/sections.html#the-nav-element)

#### Multiple Navigation Sections

If a page contains multiple navigation sections,
these can be usefully labelled using headings and `aria-labelledby`.
One may, of course, hide the headings with CSS.

```html
<header>
  <nav aria-labelledby="primary-navigation">
    <h2 id="primary-navigation">Primary navigation</h2>
    <!-- navigation items -->
  </nav>
</header>

<!-- page content -->

<footer>
  <nav aria-labelledby="footer-navigation">
    <h2 id="footer-navigation">Footer navigation</h2>
    <!-- navigation items -->
  </nav>
</footer>
```

[MDN Reference](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements#labeling_section_content)

### The `<section>` Element

* `<section>` provides a generic section for content.
* While technically the `<section>` element doesn't *require* a heading,
  assistive technology makes a heading necessary.
* See [Markup Exclusively For Assistive Technology](#markup-exclusively-for-assistive-technology)
  for section headers that shouldn't be visible.

For example:

```html
<section>
    <h2 class="sr-only">Submitted Requests</h2>
    ...
</section>
```

### The `<heading>` Element

* `<heading>` describes a block of HTML that is a heading.
* It still needs a header (e.g. `<h2>`) to provide context.
* See [Markup Exclusively For Assistive Technology](#markup-exclusively-for-assistive-technology)
  for headers that shouldn't be visible.

Example:

```html
<header>
  <h1>Main Page Title</h1>
  <img src="mdn-logo-sm.png" alt="MDN logo">
</header>
```

## Markup Exclusively For Assistive Technology

Some elements are needed for assistive technology,
but should not exist visually.

One can move them off-screen with CSS.

For example:

```css
.sr-only {
  position: absolute;
  top: -9999px;
  left: -9999px;    
}
```

```html
<section>
  <h2 class="sr-only">Descriptive Text For What Is Obvious Visually</h2>
</section>
```

[MDN Reference](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/section#using_a_section_without_a_heading)
