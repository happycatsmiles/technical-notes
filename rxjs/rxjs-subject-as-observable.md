# RxJS: Turn a Subject into an Observable

April 28, 2021

It's not uncommon to use a type of subject internally
but only expose an `Observable`.

This conversion can be done with the `asObservable` method.

For example:

```ts
private readonly _importantThing$ = new BehaviorSubject<Important | null>(null)
public readonly importantThing$ = this._importantThing$.asObservable()
```
