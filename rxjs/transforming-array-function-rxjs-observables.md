# Transforming an Array with a Function that Returns RxJS Observables

This is for handling a situation where one needs to transform an array
where the transformation function returns an observable for 
each element in the array. 

The idea is to 

1.  turn the array into an observable,
2.  map each event into an inner observable with the transformation function, 
3.  concatenate all of the results, then 
4.  convert the results back into an array.

The result is an observable, of course, which then can be subscribed to.

```ts
import { from } from 'rxjs'
import { concatAll, map, toArray } from 'rxjs/operators'

...

const arr = [1, 2, 3, 4]

const transformationObservable = from(arr).pipe(
    map(x => transformFunction$(x)),
    concatAll(),
    toArray(),
)
```
