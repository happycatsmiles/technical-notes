# Waiting for a Ready Signal (RxJS)

For those situations where operations need to wait for a subsystem to initialize, 
and that subsystem emits an event when it's ready, the 
[skipUntil()](http://reactivex.io/documentation/operators/skipuntil.html)
operator may be the right tool for the job. 
