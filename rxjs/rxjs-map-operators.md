# RxJS Map Operators

## `concatMap`

* Strings together a set of completing observables **in order**.
* Waits for each input observable to complete.
* Do not use on observables that never complete.

## `switchMap`

* Switches from one observable to another.
  * One-shot.
  * Once `switchMap` triggers, the connection to the source observable is lost.
* Uses
  * Appropriate for transforming HTTP requests as they are one-shot.
  * Do not use to transform ongoing observable events.
    `mergeMap` is probably the proper tool for the job.

## `mergeMap`

* Combines observable events from multiple sources.
* Can use on observables whether they complete or not.
* Whether `mergeMap` completes depends on whether all source observables complete.
* Use when transforming events into new observables,
  such as performing a lookup each time an event arrives.
  `switchMap` is not the right tool for this.
