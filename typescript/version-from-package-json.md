# Extract Version From `package.json`

## Preparatory Steps

The TypeScript options need to support reading from the `package.json` file.

Add the following to the `tsconfig.ts`:

```json
{
  "compilerOptions": {
    "allowSyntheticDefaultImports": true,
    "resolveJsonModule": true
  }
}
```

## Import

```ts
import { default as versionInformation } from '../../../package.json'
```
