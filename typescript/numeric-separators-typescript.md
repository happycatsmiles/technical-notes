# Numeric Separators in TypeScript

You can use the underscore `_` to break up long numbers for clarity.

Source: [TypeScript 2.7: Numeric Separators](https://blog.mariusschulz.com/2018/02/16/typescript-2-7-numeric-separators)
