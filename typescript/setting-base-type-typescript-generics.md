# Setting Base Type in TypeScript Generics

Sometimes I have a generic that is intended for things derived from a type. 
TypeScript supports this with the `extends` keyword: 

```
class AClass<T extends U> {

  ...
  const a: T = ... 
  if(a.uProperty1) ...
  ...

}
```

This allows us to access properties of U without TypeScript complaining "Property X doesn't exist on Y." 
