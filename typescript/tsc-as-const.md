# TypeScript `as const`

April 28, 2021

Reading through 4.2 release notes I saw `as const`.
It says, "This can even be used to enable `enum`-like patterns in plain JavaScript code if you choose not to use TypeScript’s `enum` construct."

https://www.typescriptlang.org/docs/handbook/release-notes/typescript-4-2.html#reverting-template-literal-inference

I read more about `as const`.

https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-4.html#const-assertions

It appears that `as const` can make a `const` object rather immutable.
Otherwise it doesn't appear that `as const` works like an enum at all.

```ts
enum ColorEnum {
  red = 'RED',
  blue = 'BLUE',
  green = 'GREEN'
}

const ColorConst = {
  red: 'RED',
  blue: 'BLUE',
  green: 'GREEN'
} as const

const ColorObject = {
  red: 'RED',
  blue: 'BLUE',
  green: 'GREEN'
}

type ColorString = 'RED' | 'BLUE' | 'GREEN'


// It appears Enum is pretty strict about what it accepts

let a1 :ColorEnum = ColorEnum.blue

// Cannot assign literal to enum
let a2 :ColorEnum = 'RED'

// Cannot assign 'as const' object to enum
let a3 :ColorEnum = ColorConst.red

// Cannot assign const object to enum 
let a4 :ColorEnum = ColorObject.red

// ColorString is pretty lax as long ast it's one of the specific values

let b1 :ColorString = ColorEnum.red
let b2 :ColorString = 'RED'
let b3 :ColorString = ColorConst.red 

// Type 'string' is not assignable to type 'ColorString'
let b4 :ColorString = ColorObject.red // NO

// ColorConst seems to be a lump of data

// Cannot use ColorConst as a type; it's an object
let c1 :ColorConst 

// It forces c2 to be the whole object.
let c2: typeof ColorConst = 'RED' // NO
let c3: typeof ColorConst = {
  red: 'RED',
  blue: 'BLUE',
  green: 'GREEN'
}

// const object properties are mutable
ColorObject.red = 'ORANGE' // OK!

// Property doesn't exist on type.
ColorObject.orange = 'ORANGE' // NO
ColorObject['orange'] = 'ORANGE' // Sorry no tricks

// Error says this is a read-only property
ColorConst.red = 'ORANGE'

// Orange does not exist on type
ColorConst.orange = 'ORANGE'

// Plain JavaScript
let a = ColorConst.red 
```
