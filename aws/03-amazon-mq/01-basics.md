# Messaging Basics

* Messaging provides
  * Loosely coupled
  * Large scale
  * Fault tolerance
* Old technology
  * Many industries
    * Retail
    * Media, entertainment, gaming
    * Financial and banquing
    * Healthcare
    * Manufacturing
  * MOM
    * Message-oriented middleware
    * Message broker
  * Business critical
  * High risk to modify
  * Heavy cost
    * People
    * Licensing
    * Support
  * Difficult to manage

## Amazon MQ

* Based on Apache ActiveMQ
* Amazon SQS
  * Message queue
* Amazon SNS
  * Pub/sub

![](images/capture-046.png)
