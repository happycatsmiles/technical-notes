# AWS Services for .NET Applications

* AWS Compute
  * Instances
    * EC2
      * Families of instances
        * compute optimized
          * CPU-intensive
        * memory optimized
        * storage optimized
    * Elastic Beanstalk
      * provides infrastructure
      * capacity provisioning
      * load balancing
      * automatic scaling
      * health monitoring
  * Containers
    * ECS (Elastic Container Service)
    * EKS (Elastic Kubernetes Service)
    * microservices
    * batch jobs
    * migrating existing applications
  * Serverless
    * Lambda
    * Fargate
      * serverless container service
* AWS Database Service
  * S3 (simple storage service)
  * RDS (Relational database service)
  * DynamoDB
    * key/value storage service
    * high-traffic web apps
    * e-commerce
    * game apps
* AWS Identity Services
  * IAM (Identity and Access Management)
    * Fine-grained control for accessing resources
    * Users
    * Groups
    * Roles
      * application
  * Directory Service
    * Microsoft Active Directory
  * Cognity
    * Social media login
    * Google login
    * Enterprise SSO
* Monitoring and auditing services
  * CloudWatch
  * CloudTrail
    * User activity tracking
    * Dumps logs to S3
    * Compliance