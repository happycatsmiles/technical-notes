# .NET Developer Tools

* AWS SDK for .NET
  * available on nuget
* IDEs
  * AWS Toolkit for Visual Studio
    * views into services
  * AWS Toolkit for VS Code
  * AWS Toolkit for JetBrains Rider
* Shell
  * AWS Tools for PowerShell
  * AWS CLI
  * .NET Core CLI extensions
* DevOps
  * AWS TOolkit for Azure DevOps
  * AWS CDK for .NET (cloud developer kit)
