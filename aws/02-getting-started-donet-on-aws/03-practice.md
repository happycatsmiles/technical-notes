# Developer Access Practice

* On IAM
  * Create new user
  * "Attach existing policies directly" > AdministratorAccess
    * This gives full access to the user.
    * OK for development.
    * Not OK for deployment; want tighter controls.
  * Download CSV.
* Install VS Studio Tools
  * https://marketplace.visualstudio.com/items?itemName=AmazonWebServices.AWSToolkitforVisualStudio2022
  * Run the executable.
* 