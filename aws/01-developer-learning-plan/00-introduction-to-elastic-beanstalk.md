# Introduction to AWS Elastic Beanstalk

* How can I quickly get my application into the cloud?
* Platform as a Service
  * Infrastructure.
* Quick deployment of apps
* Reduces management complexity

![](images/capture-041.png)

* Keep control in your hands
  * Choose instance type
  * Choose database
  * Set and adjust autoscaling
  * Update application
  * Access server log files
  * Enable HTTPS on load balancer
* Large range of platforms
  * Package Builder
  * Single, multiple or preconfigured Docker
  * Go
  * Java SE
  * Java with Tomcat
  * .NET on Windows Server with IIS
  * Node.js
  * PHP
  * Python
  * Ruby

![](images/capture-042.png)

* Update app as easily as you deployed it.

![](images/capture-043.png)

