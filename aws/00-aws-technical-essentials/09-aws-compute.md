# AWS Compute

* "Compute" is the term Amazon uses for anything that provides computational functionality.
  * Servers, basically.
  * The following names are available:
    * EC2 (Elastic Compute Cloud)
      * Virtual machines
    * Lightsail
    * Lambda
    * Batch
    * Elastic Beanstalk
    * Serverless Application Respository
    * AWS Outposts
    * EC2 Image Builder

## EC2

* Virtual servers
* Called "EC2 Instances"
* AMI - Amazon Machine Image
  * Installing OS is not your responsibility.
  * Defines
    * Storage mappings
    * Architecture type
      * 32-bit
      * 64-bit
      * 64-bit ARM
* When an instance launches:
  * AWS allocates a virtual machine that runs on a hypervisor.
  * The AMI you selected is copies to the root device volume.
  * You can connect to and install packages and additional software.
* Can create AMI from a running instance.
* Available AMIs
  * "Quick Start" AMIs, created by AWS
  * AWS Marketplace AMIs
    * Popular OS and commercial software
  * Your own AMIs
  * Build your own custom image with EC2 Image Builder
* Identifying
  * Each AMI has an ID
  * Prefixed by `ami-` followed by a hash.
  * Unique per region.

### EC2 Lifecycle

![](capture-011.png)

* States
  * Pending = VM booting
  * Stop
    * Shutdown
  * Stop-Hibernate
    * Hibernate
    * Memory preserved
  * Terminate
    * Deletes VM instance entirely.
    * Terminated instances may appear in the AWS Management Console for a while due to lag.
* Billing
  * Running - Billing starts
  * Stopping - Billing still going

#### State Notes

| State          | Host | Public IP | Private IP | RAM  | Billing                                    |
| :------------- | :--- | :-------- | :--------- | :--- | :----------------------------------------- |
| Reboot         | Same | Same      | Same       | Lost | Continues                                  |
| Stop           | X    | New       | Same       | Lost | Usage/networking stops. Storage continues. |
| Stop-Hibernate | X    | New       | Same       | Kept | Usage/networking stops. Storage continues. |

### Pricing

* On-Demand
  * Most expensive
  * No contracts
  * Pay as you go
* Reserved Instances (RIs)
  * 1- or 3-year contract
  * Three types of billing
    * No up-front
    * Partial up-front
    * All up-front
  * Greater discounts as you pay more up front.
  * All are discounted over on-demand.
* Spot Instances
  * Bid on excess capacity.
  * Pricing fluctuates
  * Set a limit how much to pay per hour.
  * Run if your offer is above the "spot" price and capacity exists.
  * Your instance may be terminated at any time.
    * Lack of capacity
    * Spot price change.

### Launch App in EC2 Instance

* Launch an EC2 instance
  * Log in as IAM user.
  * Default screen has lots of options
  * Menu > EC2
  * "Welcome to the EC2 console!"
    * Launch instance big orange button.
* Launch configuration
  * AMI (Amazon Machine Image)
    * Base OS images
    * Many to choose from.
  * Choose instance type
    * CPU/RAM/etc.
    * Button: Next: Configure Instance Details
  * "Adding instructions to user data"
  * User Data is a script to run at instance launch.
  * Demo:
    * `yum -y update`
    * `# Add node's source repo`
    * `curl -sL https://npm.nodesource.com/setup_15.x | bash -`
    * `yum -y install nodejs`
    * `# Make directory for app`
    * `mkdir -p /var/app`
    * `# Get the app from S3`
    * `wget https://aws-tc-largeobjects.s3-us-west-2.amazonaws.com/ILT-TF-100-TECESS-5/app/app.zip`
    * `unzip app.zip -d /var/app`
    * `cd /var/app`
    * `npm install`
    * `npm start`
  * Button: Add Storage
    * 8GB
  * Button: Add Tags
    * Important to track instances.
    * Key:Value pairs
    * e.g. "Name" and "Web Application"
  * Button: Security Group
    * Security group is firewall
    * Key Pairs
      * If we want to connect to console outside of the AWS console, key pairs required.
      * If only using AWS console, can choose "Proceed without key pair".
  * Button: View Instances
    * Use check box to select instance and see details.
    * There is a refresh button on top of the page to update if the instance isn't running yet.
    * Status is on line in list.
    * Can copy "Public IPv4 DNS" to connect to the instance via HTTP (as it's a web app demo).
    * Connect to Instance
      * With instance selected, use button at top: "Connect"
      * Default is unprivileged user.
      * Can use `sudo` without password.
    * Control Instance state
      * With instance selected, use button at top: "Instance State"
        * Can stop, start, reboot, hibernate or terminate.
* Test web application.
* Connect to the EC2 instance console.
  * There is a web-based console.

### Container Services

* AWS provides two types of orchestration
  * ECS: Elastic Container Service
  * EKS: Elastic Kubernetes Service

![](capture-012.png)

### Serverless

* You can't access the underlying services and instances that are hosting your "solution".
* You focus on your app.
  * The rest is abstracted away from you.
* Serverless removes:
  * The need to patch and update your server instances.

#### Fargate

* Serverless platform that you can run ECS or EKS on top of.
* Scaling, patching the OS, etc. etc. are managed automatically.

![](capture-013.png)

![](capture-014.png)

## AWS Lambda

* No server.
* Upload code.
* Code does not run until a trigger event.
  * Many types of triggers.
  * HTTP requests.
  * AWS service events.
    * e.g. File upload to S3.
* Trigger causes code to run and execute.
* Billing
  * Execution rounded up to the next millisecond.
* Many languages supported.

### Demo

![](capture-016.png)

* Source code available from [course page](https://explore.skillbuilder.aws/learn/course/1851/play/45289/aws-technical-essentials-104).
* Resize photo uploaded to S3, and store it in a different S3 location. ("bucket")
* Management console > Lambda > Functions
* Button: "Create Function"
* Button: "Author from scratch"
* Select runtime.
  * Determines language.
* Permissions
  * Lambda functions run under an AIM role.
  * The demo code uses the AWS SDK to make API calls to S3.
  * Therefore they must be signed and authenticated.
* Designer View
  * Button: Add Trigger
  * Runtime Settings
    * Can specify the handler.
    * e.g. `resize.lambda_handler`
* Run test
* Lambda > Functions > *function name*
  * Monitoring tab
  * "View logs in CloudWatch"

### Lambda Gotchas

* If triggering from an S3 upload to a directory:
  * Do **not** save output to the same directory, else it will endlessly trigger.

## Which Compute Service to Use

* Occassional Runs
  * Lambda
* Minimal refactoring from Linux
  * EC2
  * These can be Linux boxes and everything can be
* New app, microservices or service-oriented design, minimize risk of deploying new changes to production
  * ECS or EKS
  * No access to underlying OS needed
  * Just run app containers.

