# Availability

## Availability Types

* Active-Passive
  * One of two systems is available at one time.
  * State is stored on the server.
* Active-Active
  * Best for stateless systems.

### Terminology

| Availability (%)                | Downtime (per year) |
| :------------------------------ | :------------------ |
| 90% (one nine)                  | 36.53 days          |
| 99% (two nines)                 | 3.65 days           |
| 99.9% (three nines)             | 8.77 hours          |
| 99.95% (three and a half nines) | 4.38 hours          |
| 99.99% (four nines)             | 52.60 minutes       |
| 99.995% (four and a half nines) | 26.30 minutes       |
| 99.999% (five nines)            | 5.26 minutes        |

## Scaling

* Vertical Scaling
  * Creating a bigger instance
* Horizontal scaling
  * Many smaller instances
  * Default account is limited to 20 instances per region
  * Can request larger quota
* Use a load balancer to provide single point of contact.

![](capture-035.png)

