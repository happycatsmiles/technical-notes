# Module 6: Monitoring, Optimization, Serverless

## Monitoring

* Monitoring tools collect data
  * Metrics
  * Logs
  * Traffic
* Data points
  * e.g. CPU utilization
* Metrics
  *  Group of data points
  *  EC2 e.g.
     *  CPU Utilization
     *  Network In
     *  Network Out
  *  RDS e.g.
     *  Database connections
*  Statistics
   *  Metrics over time

### CloudWatch

* Seems similar to Prometheus.
* Single point of collection
* Many resources automatically start providing statistics.
* Can provide custom (programmatic) metrics.
* Logs
  * Log event
    * A log event is a record of activity recorded by the application or resource.
  * Log stream
    * Log events are grouped into log streams
    * All belong to the same resource being monitored
  * Log groups
    * Log streams are organized into log groups.
    * all share the same retention and permissions settings.
* Dashboard
  * Container for graphs to display statistics.
  * Can display data across regions
* Alarms have three states
  * OK
  * Alarm
  * Insufficient Data
* Simple Notification Service (SNS)
  * "Topic" is the type of notification.
* Demo
  * AWS Management Console > CloudWatch
    * Link: Dashboards
    * Button: Create dashboard

![](capture-034.png)

* Demo
  * AWS Management Console > CloudWatch
  * Link: Alarms
  * Button: Create alarm
  * Select criteria for the alarm
  * Button: Next
  * Configure actions page
  * "In alarm" means when the alarm transitions to this state.
