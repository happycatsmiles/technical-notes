# Identity and Access Management (IAM)

* IAM "Identity"
  * This is not specified
  * Groups
  * User
  * Roles
* All API calls to AWS services by code must be
  * signed
  * authenticated
* Programs running in containers use roles.
  * Roles are managed by IAM
* IAM can dole out permissions to access different parts of AWS
* IAM does not provide application-level access controls.
* AWS Account Access
  * Each user has their own login.
  * Logins take care of authentication.
  * Authorization is controlled by "IAM Policies".
* IAM User
  * Can be:
    * A person that interacts with AWS.
    * A service that interacts with AWS.
  * Each action is billed to your account.
  * Consists of:
    * A name
    * set of credentials
  * Can be given:
    * Access to the AWS Management Console
    * Programmatic access to the AWS CLI
    * Programmatic access to the AWS API
  * No access by default
* IAM Groups
  * "Best Practice"
  * Groups of users
  * Many:Many relationship between users and groups
  * Groups cannot contain groups
  * Root is God.
* IAM Policies
  * Grant or deny permission to take actions.
    * Actions = API call
    * Everything is an API call.
  * Attach policies to AWS "identities"
    * User
    * Groups
  * Types of Policies
    * Managed
      * Pre-packaged policies for common tasks provided by AWS
    * Inline
      * custom policy attached to a group.
  * Compares resource policies to user/groups to determine allow/deny.
  * JSON
    * `Version`
      * Must come first.
    * `Statement`
      * Array of objects
      * `Effect`: `Allow` or `Deny`
      * `Action`:
        * `*` wildcard
        * Array of strings
        * e.g.
          * `ec2:*`: Any EC2 action
          * `ec2:RunInstances`
      * `Resource`:
        * `*` wildcard
        * Amazon Resource Name (string)
      * `Condition`: optional

## Example: Administrator Policy

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
}
```

## Example: User Can Change Password Policy

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "iam: ChangePassword",
                "iam: GetUser"
            ],
            "Resource": "arn:aws:iam::123456789012:user/${aws:username}"
        }
    ]
}
```

![](Capture-003.PNG)
