# Networking

## IP Addresses

* Choose network size via CIDR
* Smallest is /28 (16 addresses)
* Largest is /16 (64k addresses)

### Reserved IPs

THere are five reserved addresses.
Two are reserved due to IP.
Three are arbitrary reservations by Amazon.

#### Example

* VPC with IP range 10.0.0.0/22
* Divided into four /24 subnets
* Each /24 subnet has 251 IP addresses

| IP Address | Reserved For              |
| :--------- | :------------------------ |
| 10.0.0.0   | Network Address           |
| 10.0.0.1   | VPC local router          |
| 10.0.0.2   | DNS server                |
| 10.0.0.3   | Future user               |
| 10.0.0.255 | Network broadcast address |

## VPC (Virtual Private Cloud)

* Virtual network with the following properties:
  * VPC name
  * Region-specific
  * Can (should) cross availability zones.
  * IP range

![](capture-017.png)

## Creating a VPC

* From management console, ensure one is in the correct region.
  * Dropdown in the upper right menu of the management console.
* Type VPC in the service search bar at the top of the management console.
* Title "Resources by Region"
* VPC link
* "Your VPCs"
* Button: Create VPC

## Subnets

* Divide VPC into subnets
  * Isolate resources
* Per availability zone (?)
* Must be a subset of the VPC

![](capture-018.png)

## Creating a Subnet

* From management console, ensure one is in the correct region.
  * Dropdown in the upper right menu of the management console.
* Type VPC in the service search bar at the top of the management console.
* Title "Resources by Region"
* Subnets link
* Button: "Create Subnet"

## Internet Gateway (IGW)

* Gateway between the VPC and the internet at large.
* Must be attached to a VPC; no defaults.


* From management console, ensure one is in the correct region.
  * Dropdown in the upper right menu of the management console.
* Type VPC in the service search bar at the top of the management console.
* Title "Resources by Region"
* Internet Gateways link
* Button: Create Internet Gateway
* VPC > Internet gateways > igw-<key>
* Actions dropdown: Attach to VPC

## Virtual Private Gateway (VGW)

* Gateway between external resources and a VPC

![](capture-019.png)

## Resilience

* Need to duplicate subnets to other AZ.

![](capture-020.png)

![](wPxuV_0JWkhj9dQV_InIjhnQ0x-d4xLbK.png)

## VPC Routing

### Main Route Table

* Creating a VPC creates a "main route table".
* Creating subnets by default allows traffic to flow between all subnets.

### Custom Route Tables

* One creates a custom route table, the associate it with one or more subnets.
* Creating this association overrides the main route table.
* Public availability
  * If there is a route between the Internet Gateway (IGW) and a subnet,
    then the subnet is publicly available.
  * If there is no route between the IGW and a subnet,
    the subnet is private.

![](capture-021.png)

### Example: Make Subnet Publicly Accessible

* From the VPC dashboard
  * Link: Route Tables
  * Displays VPCs for the region.
  * Click check box for the desired VPC.
  * Information shows below.
  * Select "Routes" in the lower options.
  * Button: Edit Routes
  * Button: Add Route
    * 0.0.0.0/0
    * Target: Internet Gateways
      * Then select the desired IGW
    * Button: Save
  * This creates the route table but doesn't associate it with any subnets.
  * Tab: Subnet Associations
    * Button: Edit subnet associations
    * Select subnets intended to be public.
    * Button: Save

## VPC Security Groups (Network ACLs)

* Firewall at the subnet level.

### Default Network ACL

* Default blocks all inbound traffic but allows outbound traffic.

### Example: HTTPS and RDP

* Example for allowing HTTPS and home network RDP.
* Note the **outbound rule needs to explicitly allow outbound traffic** on ports 1025-65535.

**INBOUND**

| Rule # | Source IP        | Protocol | Port | Allow/Deny | Comments                                    |
| -----: | :--------------- | :------- | :--- | :--------- | :------------------------------------------ |
|    100 | All IPv4 traffic | TCP      | 443  | Allow      | Allow all HTTPS from everywhere.            |
|    130 | 192.0.2.0/24     | TCP      | 3389 | Allow      | Allow inbound RDP traffic from home network |
|      * | All IPv4 traffic | All      | All  | Deny       |

**OUTBOUND**

| Rule # | Destination IP   | Protocol | Port       | Allow/Deny | Comments                  |
| -----: | :--------------- | :------- | :--------- | :--------- | :------------------------ |
|    120 | 0.0.0.0/0        | TCP      | 1025-65535 | Allow      | Allow outbound responses. |
|      * | All IPv4 traffic | All      | All        | Deny       |

### Security Groups

* Can create "security groups" which are these network ACL rules.

#### Example: Accept Internet Traffic

**INBOUND**

| Type  | Protocol | Port Range | Source    |
| :---- | :------- | :--------- | :-------- |
| HTTP  | TCP (4)  | 80         | 0.0.0.0/0 |
| HTTP  | TCP (6)  | 80         | ::/0      |
| HTTPS | TCP (4)  | 443        | 0.0.0.0/0 |
| HTTPS | TCP (6)  | 443        | ::/0      |

![](FgEhatXf6qnJQ1X1_scxJIDNUzFAqjZbL.jpg)
