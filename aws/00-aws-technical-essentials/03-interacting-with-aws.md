# Interacting with AWS

* AWS Management Console
  * Web UI
* AWS CLI
  * [Home Page](https://docs.aws.amazon.com/cli/index.html)
  * [Install Page](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
* AWS SDKs
  * [Home Page](https://docs.aws.amazon.com/sdkref/latest/guide/overview.html)
  * These exists for various languages.
  * Programs can interact with AWS
    * e.g. store files to S3
