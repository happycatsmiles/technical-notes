# EC2 Auto Scaling

* Active-Passive
  * Vertical scaling
  * One active instance, one passive instance
    * Active takes traffic
    * Passive standing by
  * Make the instance larger or smaller
  * Lots of manual work
* Active-Active
  * Horizontal scaling
  * Scales based on CloudWatch metrics
  * Integrates with ELB
    * Requires health check.
    * Either:
      * Successful TCP connection
      * HTTP/HTTPS request
* Traditional Scaling
  * Provision enough servers to handle peak load.

## Configuration

* Launch template / configuration
  * What resource should be auto-scaled?
* EC2 Auto Scaling Group
  * Where should resource be deployed?
* Scaling policies
  * When should resource be added or removed?

### Launch Templates

* EC2 instances need multiple parameters before they can launch
  * Amazon Machine Image (AMI)
  * Instance type
  * Security group
  * EBS volumes
  * etc
* Stored in a "launch template"
* Can use to manually launch an EC2 instance.
* Can use with EC2 Auto Scaling.
* Supports versioning
  * Can iterate with new version
  * Users can use default version

![](Y9U8MWYOjosNS6Ta_0tIEHgm6nFcjy6rI.jpg)

* Can create
  * Using an existing EC2 instance
  * Using an existing template version
  * From scratch

### EC2 Auto Scaling Groups

* Defines where to deploy resources.
  * VPC
  * Subnets.
* Select at least two subnets across Availability Zones.
* Define the "type of purchase" for EC2 instances.
  * On-demand only
  * Spot only
  * Combination
* Capacity
  * Minimum
  * Maximum
  * Desired capacity
    * Starting number of instances
    * May go up or down from there
  * All three may be the same for a constant number of instances.

![](0kZUDudHV3iaR_bM_9nwuC9QQxKigHqNb.jpg)

### Scaling Policies

* CloudWatch alarms
  * Add instances
  * Remove instances
  * Set to a number
  * Use numbers or percentages.
* Cooldown period after triggering
* Types
  * Simple
  * Step
  * Target tracking

#### Simple Scaling Policy

* Simple alarm/action policies
  * e.g. CPU utilization > 60%
* "Until the instance's specified warm-up time has expired, the instance is not counted toward the aggregated metrics of the Auto Scaling group."
* Optional notifications on trigger.

#### Step Scaling Policy

* Respond to additional alarms.
  * e.g. CPU utilization 85% = add two instances; 95% = add four instances
* Can be difficult to judge thresholds and actions.

#### Target Tracking Policy

* Based on metric averages.
  * CPU
  * Network traffic
  * etc.
* Scales up or down based on the desired metrics.

