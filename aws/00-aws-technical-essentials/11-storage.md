# Storage

## Storage Types

* File store
  * NAS/NFS-like
  * Typical Uses:
    * Content storage
    * Development environments
    * User home directories
* Block store
  * Low latency
  * Typical Uses:
    * Databases
* Object store
  * Flat storage; no directory hierarchy
  * WORM (Write Once, Read Many)
  * Updates affect the entire object.
    * One-character update requires the entire object to be re-written.
  * Typical Uses:
    * Static assets
    * Video
    * Audio
    * Images

## EC2 Storage

### EC2 Instance Store

* Internal local drive for an instance
* Ephemeral
* Disappears when instance is stopped(!) or destroyed.

### Elastic Block Storage (EBS)

* External storage for an instance.
* Similar to an external drive.
* Attached to one instance.
  * There are announced plans for this to change.
  * "Multi-Attach"
* Fixed maximum size.
* Can expand.
* 16TB maximum.
* Typical Uses:
  * OS boot/root volumes.
    * Root device for an instance launched from Amazon Machine Image (AMI) typically is an EBS volume.
    * Called "EBS-backed AMI"
  * Databases
  * Enterprise block storage
  * Throughput-intensive applications
    * Long continuous reads or writes
* Features Offered
  * High availability (in one Availability Zone)
  * Lifespan independent of the instance using it.
  * Supports encryption
  * Modify volume type, size, IOPS without stopping the instance.
* Backups
  * Done as snapshots.
* Snapshots
  * Stored across Availability Zones via S3.
  * Handled automatically through the console
    * No need to mess with S3 directly.
  * Can create a new volume from a snapshot.

#### EBS Volume Types

Type | Description |Uses |Size | Max IOPS | Max Throughput
:-- | :-- | :-- | :-- | :-- | :-- | 
EBS Provisioned IOPS SSD | Highest performance | Databases | 4GB -- 16TB | 64k | 1 GB/s
EBS General Purpose SSD | General purpose | General filesystem | 1GB -- 16TB | 250 MB/s
Throughput Optimized HDD | Low cost HDD for throughput | Big data | 500GB -- 16TB | 500 MB/s
Cold HDD | Lowest cost HDD | Cold data | 500GB -- 16TB | 250 MB/s

## Simple Storage Service (S3)

* Object store
* "Storage For The Internet"
  * Not mounted as volumes.
  * Not tied to Compute
  * Accessible via HTTP URI.
* Unlimited quantity.
* Individual object size of 5TB.
* Distributed across a region.
* Intended to be durable.

### Bucket

* Bucket is a S3 storage unit.
* Folders
  * Buckets can have folders inside.
  * There is no actual directory structure.
  * Purely for organizing
* Bucket names must be globally unique and DNS compliant
  * Become part of the URI.
* Uploading a file give the object the name of the file.
* Everything private by default.
* Making object public is multiple steps.
  * Attempt to make object public
    * Amazon S3 > <bucket-name> > <object-name>
    * Dropdown: Object actions
    * Make Public
    * Ah! But bucket settings by default "private".
    * Must change the bucket settings.
  * Change bucket settings
    * Link: bucket settings for Block Public Access
    * Button: Edit
    * Check box (off): Block *all* public access.
    * Button: Save Changes
    * Dialog: Type confirm.
  * Attempt (again) to make object public
    * Amazon S3 > <bucket-name> > <object-name>
    * Dropdown: Object actions
    * Make Public
    * Success.
* Security
  * Can use IAM policies.
  * Can use "S3 bucket policies"
  * IAM policies attached to
    * users
    * groups
    * roles
  * S3 bucket policies
    * attached to buckets
    * NOT folders
    * NOT objects
* Versioning
  * Buckets support versioning.
  * No actual deletion of objects.
  * Objects have version numbers.
  * States
    * Unversioned (default)
    * Versioning enabled
    * Versioning suspended
      * New adds and edits do not have versioning.
      * Retains existing versioning.

### S3 Storage Classes (Tiers)

Class | Description
:-- | :--
S3 Standard | General-purpose storage
S3 Intelligent Tiering | Automatic object movement between frequent and infrequent tiers.
S3 Standard-Infrequent Access (IA) | Prices per GB and per retrieval; long-term cold storage.
One Zone IA | Stores in one availability zone instead of 3+ AZs. Lower cost cold storage.
S3 Glacier | Data archival.
S3 Glacier Deep Archive | Long-term archive, access perhaps twice a year. Regulatory compliance.

#### Lifecycle Policies

* Where data is stored can be changed.
* Automated policies.

![](qCUAPTIXWCtvNwWm_e9I6oE5HEKhzWY2m.jpg)

## Demo: Create an S3 Bucket

* Names have to be globally unique
* Default security to block everything
  * Bucket policy will be created to give public access.
* Amazon S3 > <bucket-name> 
  * Permissions tab
  * Bucket Policy card
  * Button: Edit
  * The policy is JSON
    * This bucket policy maps to an IAM role.
    * It gives full access to the application.

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowS3ReadAccess",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam:<number>:role/EmployeeDirectoryAppRole"
      },
      "Action": "s3:*",
      "Resource": [
        "arn:aws:s3:::<bucket-name>",
        "arn:aws:s3:::<bucket-name>/*"
      ]
    }
  ]
}
```
