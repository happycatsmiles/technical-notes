# Role-Based Access in AWS

* An "identity" that can be assumed by a user or program.
* Managed by IAM.
* Programs
  * Do not create an IAM user for the program.
  * Do not hard-code credentials.
  * Use IAM Roles
  * ![](Capture-004.PNG)
* Roles
  * No static logic credentials
  * Assumed programmatically
  * credentials are temporary for a configurable time span
  * credentials expire and are rotated.
* Federated Users
  * One may use a third-party identity provide to manage users.
  * One may associate federated users with roles.
  * There are services such as AWS SSO (Single Sign-On) to help with this.
  * ![](capture-010.png)

## Demo To Create A Role

From the AWS Management Console:

![](Capture-005.PNG)

One may use the top bar to search for things in the Management Console, such as IAM:

![](Capture-006.PNG)

One may select `Roles` from the left-hand menu:

![](capture-008.png)

One may create a new role with `Create Role`:

![](capture-009.png)

Further details may be found in the [video](https://explore.skillbuilder.aws/learn/course/1851/play/45289/aws-technical-essentials-104).

