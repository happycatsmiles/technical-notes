# Databases on AWS

* Unmanaged database
  * Run in an EC2 container.
* Managed database
  * Run by Amazon

![](oiTBtQ6jq407jipV_rJUeOc4ihDuZwgAm.png)

![](wFi8QJoR3Ymp4q5P_au-KJ3clD2M8sRQV.jpg)

## Amazon RDS (Relational Database Service)

* Uses Block Storage (EBS)
  * General Purpose SSD
  * Provisioned IOPS (SSD)
  * Magnetic storage (not recommended)
* Easy create vs standard create
  * Pre-set backup, availability, etc.
  * Standard lets you tweak these values.
* Aurora
  * Tailored to Amazon's cloud.
  * Large data handling ability (up to 128TB)
  * 5x faster than MySQL
  * 3x faster than PostgreSQL
* DB Instance
  * Similar to an EC2 instance
  * Lives in one subnet inside of one AZ.
  * "RDS multi-AZ deployment"
  * Automatic fail-over from primary to secondary DB.
  * App needs to handle re-connecting.
  * Types
    * Standard
    * Memory Optimized
    * Burstable Performance
* Security
  * Can be placed in a Virtual Private Cloud (VPC)
  * Use Identity Access Management (IAM)
* Backups
  * Automatic
  * Manual

![](capture-023.png)

## Purpose-Built Databases

* Use database that fits use case.
  * Simple lookups don't need full relational database.
  * RDS charges per hour.

### Amazon DynamoDB

* Non-relational
* Key-value pairs
* Charges by
  * Use
  * Amount of data

### Amazon DocumentDB

* Document-oriented CMS
* MongoDB compatibility

### Amazon Neptune Graph Database

* social networks
* recommendation engines
* fraud detection

### Amazon QLDB Ledger Database

* Immutable ledger
* Good for regulatory and auditing needs

## DynamoDB

![](capture-024.png)

* Purpose-built
  * No good for all use cases
* Fully managed NoSQL database.
* Serverless
* Lives across AZs
* Scalable
* Fast
  * millisecond response time
* simple queries
* Flexible attributes

![](capture-025.png)

### Example App With DynamoDB

* AWS management console
  * Search for "DynamoDB"
  * Button: Create Table
  * Must choose attribute to be primary key

## Choosing the Right Database Service

* AWS supports apps using multiple database services for different data.
* No one-size fits all.

| Database Type | Use Cases        | AWS Service           |
| :------------ | :--------------- | :-------------------- |
| Relational    | Traditional apps | RDS, Aurora, Redshift |
Key Value | High-traffic apps, gaming | DynamoDB
In-Memory | Caching, Sessions, Leaderboards | ElastiCache for Memcached & Redis
Document | CMS |  DocumentDB (with MongoDB compatibility)
Wide Column | Industrial apps fleet management, route optimization|Keyspaces (for Apache Cassandra)
Graph | Fraud detection, social networking, recommendation engines | Neptune
Time Series | IoT, DevOps, industrial telemetry | Timestream
Ledger | Systems of record, supply chain, registrations, banquing | QLDB

## Demonstration: DynamoDB

* AWS Management Console > DynamoDB
* Button: Create Table
* Add info via app.
* DynamoDB Console > Table > Employee
* Click on item to edit item.
* DynamoDB Console > Table > Employee
* Button: Create Item
  * Have to create each field.
* 