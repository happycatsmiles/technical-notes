# Search Engines

Since DuckDuckGo declared they would start to engage in censorship,
there was a search for alternatives.

* https://swisscows.com/
* https://yandex.ru/
  * Russian
* https://www.mojeek.com/
* https://www.wolframalpha.com/
  * Science-oriented
* https://metager.org/
* https://www.qwant.com/
  * Like DuckDuckGo, but uses Bing as the back end.
* https://searx.space/
  * DIY search engine
  * Open Source hackable metasearch engine
* Yaacy
  * P2P DIY search engine
  * Not on clear net
* https://gibiru.com/
  * Like pre-corrupt DuckDuckGo
