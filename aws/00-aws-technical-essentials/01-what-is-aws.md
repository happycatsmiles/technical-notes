# What is AWS

* Discussion of private data centre vs. cloud-provided resources
* "the compute the code runs on"
  * This is unusual language.
* Six advantages of cloud computing
  * These read like general marketing items
    * Pay as you go
    * Benefit from massive economies of scale
    * Stop guessing capacity
    * Increase speed and agility
      * devops
    * Realize cost savings
    * Go global in minutes
* Extra sites
  * Types of cloud computing
    * Infrastructure as a Service (IaaS)
    * Platform as a Service (PaaS)
    * Software as a Service (SaaS)
  * Deployment models
    * Cloud
      * Fully in the cloud
    * Hybrid
      * Partially in the cloud
      * Communication between cloud and non-cloud services
    * On-Premises
      * Private cloud
  * Cloud Computing
    * Machine learning
    * AI
    * "Data Lakes"
    * Analytics
    * IoT
      * Curious they add this here.
  