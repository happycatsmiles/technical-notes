# Employee Directory Application Hosting

* VPC (virtual private cloud) provides a network
* There is a default VPC associated with every account.
* Every EC2 instance must live inside of a network.
* EC2
  * "Compute" service
    * This is just the name they use
  * Hosts VMs
* Starting an Instance
  * "User Data"
    * A script to run when the VM starts.
* Security Group
  * Firewall for the instance.