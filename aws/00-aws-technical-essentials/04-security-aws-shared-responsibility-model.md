# Security and the AWS Shared Responsibility Model

* AWS responsible for some aspects of security.
  * Physical security
  * Private fibre
  * Virtualization below the VM
  * Security "Of" the cloud
* User is responsible for other aspects of security.
  * Security "In" the cloud
  * VM OS updates
  * Data handling & encryption.
* For example user is responsible for:
  * Choosing a Region for AWS resources in accordance with data sovereignty regulations.
  * Implementing data-protection mechanisms, such as
    * encryption and
    * scheduled backups
  * Using access control to limit who can access to your data and AWS resources
* There is some nuance in edge cases.

![](k9I_CPpgT9ldzDnD_ORXP9nIg811Z19fP.png)
