# AWS Global Infrastructure

* Data Centres
  * Amazon has physical data centres in various global locations.
  * Locations kept secret
* Availability Zone
  * One or more data centres with high-speed connections for redundancy.
* Region
  * One or more Availability Zones for redundancy.
  * Named by physical location
  * Independent of each other
    * No data replication between regions
* Selecting a region
  * Compliance
    * Legal restrictions
  * Latency
    * How close to user base
  * Pricing
    * Varies from region to region
    * Taxes and regulations can affect this.
  * Service availability
    * Not all features are available in all regions.
* Naming
  * Regions
    * us-east-1
  * Availability Zones
    * Append a letter to region names
    * us-east-1a
* Services
  * Some services are region scoped
  * Some services are AZ scoped
* Best Practices
  * 2+ AZ
  * Use "region-scoped managed services"
  * When not possible, ensure replication across AZs.

![](oginJXNL_Le4B0v__GApZkCJ7lxzYwaX-.jpg)

