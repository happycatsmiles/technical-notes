# Elastic Load Balancing (ELB)

* Highly available
* Auto scaling
* Regional service
  * Ensure it's deployed across multiple Availability Zones.
* Hybrid Mode
  * Can load balances to on-premises services

## Types

   Application load balancer (ALB)
  * HTTP/HTTPS (layer 7)
* Network load balancer (NLB)
  * TCP/UDP/TLS (layer 4)
* Gateway load balancer (GLB)
  * IP (layer 3+4)
  * Can load balances to on-premises applications.

## Health Checks

* Dedicated page to ensure required services are available.
* Performs health check before sending traffic to EC2 instance.
* EC2 auto scaling informs ELB when an instance will be removed.
  * Can prevent EC2 instance from being removed until all connections have completed.
  * "Connection Draining"

![](pNvTcJrjmRabrPTE_dJANJE3449uCjTyK.jpg)

## Application Load Balancer (ALB)

* HTTP/HTTPS
  * Use network load balancer for other protocols
* Listener
  * Port + Protocol
    * 80 / HTTP
    * 443 / HTTPS
* Target group
  * e.g.
    * EC2 Instances
    * Lambda instances
    * TCP/IP ports
  * Each group must have a health check.
* Rules
  * Each listener has default rule
  * Can define additional rules

Example additional rule: Redirect `/info` to a certain group.

![](capture-038.png)

### Some Features

* Routes traffic based on request data
  * Protocol
  * URI
  * HTTP headers
  * HTTP method
  * Source IP
  * Query string
* Can send responses directly
  * Can reply to client with fixed response
    * e.g. fixed HTML page
  * Can redirect
  * Removes work from servers
* Uses TLS Offloading
  * Import SSL certificate via IAM
  * Import SSL certificate via AWS Certificate Manager (ACM)
  * Create free certificate using ACM
  * Ensures traffic between ALB and client is encrypted.
* Can authenticate users
  * OpenID
  * Integrates with other AWS services to provide
    * SAML
    * LDAP
    * Microsoft Active Directory
    * etc.
* Can configure client IP ranges
* Algorithms
  * Round robin (default)
    * Good general default
  * Least Outstanding Request
    * Useful when response times can vary wildly.
* Can use sticky sessions
  * If requests must be sent to the same backend server because the application is stateful, use the sticky session feature. 
  * Uses an HTTP cookie to remember across connections
* Masks source IP
  * Clients see the IP address of the ALB.

## Network Load Balancer (NLB)

* Protocols
  * TCP
  * UDP
  * TLS
* Operates at the connection level
  * Doesn't understand HTTP
* Flow hashing algorithm
  * Matches
    * Protocol
    * Source IP
    * Source Port
    * Destination IP
    * Destination Port
    * TCP sequence number
  * If all the same, packets sent to the exact same target.
* Sticky sessions
  * Based on source IP
* Supports TLS offloading
  * Similar to Application Load Balancing
* Handles millions of requests per second without scaling
* Supports static and elastic IP addresses
* Preserves the source IP address.
  * Targets see the client IP address instead of the load balancer.

## Choosing ELB Type

| Feature                                |     ALB     |      NLB      |
| :------------------------------------- | :---------: | :-----------: |
| Protocols                              | HTTP, HTTPS | TCP, UDP, TLS |
| Connection draining                    |      X      |       X       |
| IP addresses as targets                |      X      |       X       |
| Static and elastic IP addresses        |             |       X       |
| Preserve source IP address             |             |       X       |
| Routing based on source IP, path, etc. |      X      |               |
| Redirects                              |      X      |               |
| Fixed response                         |      X      |               |
| User authentication                    |      X      |               |
