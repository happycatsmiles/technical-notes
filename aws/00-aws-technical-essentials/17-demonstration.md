# Demo

* Look at load balancing target groups
* Three instances instead of two
  * The extra is the original EC2 before the auto-scaling was put into place.
  * Need to manually remove this.
