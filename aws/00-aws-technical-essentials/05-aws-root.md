# AWS Root User

* The username/password you use to sign up with AWS becomes a "root user".
* Enable MFA
* There are only a few actions that are required by root.
* Otherwise do not use, even for administrative actions.
* Access Key
  * Access key ID, for example, A2lAl5EXAMPLE
  * Secret access key, for example, wJalrFE/KbEKxE

## Delete your keys to stay safe

If you don't have an access key for your AWS account root user, don't create one unless you absolutely need to. If you have an access key for your AWS account root user and want to delete the keys, follow these steps:

1. In the AWS Management Console, go to the My Security Credentials page, and sign in with the root user’s email address and password.
2. Open the Access keys section.
3. Under Actions, choose Delete.
4. Choose Yes.
