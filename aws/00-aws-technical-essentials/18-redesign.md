# Redesign Making Full Use of Amazon Services

* The initial design is good.
* Traditional architecture.

![](capture-039.png)

* Route 53 for DNS.
* S3 + CloudFront for hosting / caching front end.
* API Gateway + Lambda functions to host back end API.
* Data layer is the same.

![](capture-040.png)
