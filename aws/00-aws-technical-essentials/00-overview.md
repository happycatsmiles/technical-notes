# AWS Technical Essentials

## Overview

### Example App

Amazon has a very fine-grained set of tools.

* Amazon VPC
  * Virtual Private Cloud
* Availability Zones
  * The VPC spans these
* App container runs in an EC2 instance.
  * Example has one per availability zone
* Amazon EC2 Auto Scaling
* ELB
  * Elastic Load Balancing
  * Balances across EC2 instances
* Amazon RDS DB Instance
  * Two instances
    * one marked P
    * one marked S
  * Primary, secondary (?)
* Amazon DynamoDB
  * Different from RDS instances
* Amazon CloudWatch
  * monitoring
* Amazon S3
  * "Super Simple Storage"
* Amazon IAM
  * Identity and Access Management
  * For security and identity

![](Capture-002.PNG)

## References

* [What is AWS](https://explore.skillbuilder.aws/learn/course/1851/play/45289/aws-technical-essentials-104)
* 
