# Reset Blank Screen (Windows 10)

Sometimes,
when waking a computer with multiple monitors,
one of the screens does not wake up.
One may recycle the power of the screen,
or one may use the following key combination:

```
CTRL + SHIFT + WIN + B 
```
