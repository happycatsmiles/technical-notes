# Docker Complains "Ports Are Not Available"

## Symptom

Having worked for a very long time,
today Docker Desktop gives the following cryptic error when attempting to start a container:

![](images/docker-ports-are-no-available-01.png)

> Error invoking remote method 'docker-start-container': Error (HTTP code 500) server error - Ports are not available: exposing port TCP 0.0.0.0:<port> -> 0.0.0.0.: listen tcp 0.0.0.0:<port> bind: An attempt was made to access a socket in a way forbidden by its access permissions.

## Underlying Problem

Apparently Windows reserves a large block of ports dynamically.
It appears that by chance, Windows' `winnat` has reserved the desired port.

## Solution

To determine Window's dynamic port range:

```cmd
netsh int ipv4 show dynamicport tcp
```

To fix the problem:

1. Stop `winnat`.
2. Exclude the desired port range.
3. Restart `winnat`.

```cmd
net stop winnat
netsh int ipv4 add excludedportrange protocol=tcp startport=61613 numberofports=2
net start winnat
```

## References

* StackOverflow [Many excludedportranges how to delete](https://superuser.com/questions/1579346/many-excludedportranges-how-to-delete-hyper-v-is-disabled/1671710#1671710)
* A long list of people dealing with the problem [on the Docker GitHub](https://github.com/docker/for-win/issues/3171).
* StackOverflow [Ports are not available](https://stackoverflow.com/questions/65272764/ports-are-not-available-listen-tcp-0-0-0-0-50070-bind-an-attempt-was-made-to)
