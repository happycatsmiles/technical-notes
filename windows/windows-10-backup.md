# Backup Machine on Windows 10

`robocopy` is short for "robust copy".
It seems to have capabilities similar to `rsync`.

## Backup C:\ Drive

* Run PowerShell or command prompt as administrator.
* Use backup media formatted as NTFS.
* Assuming backup media is mounted on the D: drive:
* Note that `/mt:32` is 32 threads, which is targeted to a 16-core machine.

```sh
robocopy c:\ d:\ /mir /fft /r:3 /w:10 /z /np /ndl /xjd /mt:32
```

## References

* [StackOverflow Question](https://superuser.com/questions/814102/robocopy-command-to-do-an-incremental-backup)
* [Microsoft Documentation](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/robocopy)
