# Kill Windows Processes by Name

```sh
taskkill /f /im <executable name.exe> /t
```

*  /f force kill
*  /t kill child processes also
*  /im executable name
