# LSL `llHTTPRequest` Sending Blank Body

When sending JSON data, (for example with a POST), it's mandatory to set the MIME type:

```lsl
    list parameters = [ 
        HTTP_METHOD, method, 
        HTTP_MIMETYPE, "application/json",
    ];

    llHTTPRequest(url, parameters, body);
```

If the MIME type is not set to `application/json`, the body will be empty.
