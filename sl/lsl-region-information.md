# Region Information

LSL provides the following information:

| What                | LSL                               | Notes                                                                     |
| ------------------- | --------------------------------- | ------------------------------------------------------------------------- |
| Region Name         | `llGetRegionName()`               |
| Simulator Host Name | `llGetSimulatorHostname()`        | 10 second forced delay. Not useful information.                           |
| Simulator Host Name | `llGetEnv("simulator_hostname")`  | No delay. Still not useful information.                                   |
| Time Dilation       | `llGetRegionTimeDilation()`       | Float [0..1]                                                              |
| Region FPS          | `llGetRegionFPS()`                | Float [0..45]                                                             |
| Channel             | `llGetEnv("sim_channel")`         | Appears to be platform information.                                       |
| Version             | `llGetEnv("sim_version")`         | Simulator version number.                                                 |
| Corner              | `llGetRegionCorner()`             | Vector with (x, y) lower left corner in metres.                           |
| Grid Offset         | corner / 256                      | Vector with (x, y) lower left corner in region units, i.e. corner / 256m. |
| Agent Limit         | `llGetEnv("agent_limit")`         | Number of avatars allowed in the region.                                  |
| Per CPU             | `llGetEnv("region_cpu_ratio")`    | Number of regions per CPU.                                                |
| Estate ID           | `llGetEnv("estate_id")`           | Internal ID for region type.                                              |
| Estate Type         | `llGetEnv("estate_name")`         | Name of region type.                                                      |
| Region Idle         | `llGetEnv("region_idle")`         | Unknown.                                                                  |
| Region Product Name | `llGetEnv("region_product_name")` |
| Region Product SKU  | `llGetEnv("region_product_sku")`  |
