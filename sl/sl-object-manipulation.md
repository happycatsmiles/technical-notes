# Manipulating Objects in SecondLife

## Scaling / Stretching

* The option "stretch both sides" keeps the same origin.

## Selecting En Mass

* One can select other objects my mistake.
* Build > Options > Select Only My Objects
  * This may be helpful.
* Manipulate the camera, e.g. overhead view, to avoid selecting undesired objects.

## Linking

* The last selected prim is the root.
* Link number 0: Unlinked object
* Link number 1: Root prim of linked objects.
* The root object is highlighted in yellow.
  * Child objects are in blue.
* Edit child objects by checking "Edit Linked".
