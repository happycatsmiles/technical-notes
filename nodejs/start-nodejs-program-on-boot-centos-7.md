# Start NodeJS Programs At Startup On CentOS 7

PM2 can install itself as a service to be started on system boot. 

```bash
$ sudo npm i -g pm2
$ sudo pm2 startup systemd
```

**Note:** I had to run the second command as root. I got errors when I ran it with sudo. 
