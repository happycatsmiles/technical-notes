# Allow NodeJS to open port 80

NodeJS, like other programs, can't open ports below 1024 without running as root. 
This command will give NodeJS the ability to bind to those ports without root privileges. 
This isn't without risk as it give persmission to *any* node program. 

```bash
setcap cap_net_bind_service=+ep /usr/bin/node
```
