# Auto-Start NodeJS Programs On Boot (CentOS)

pm2 can start NodeJS programs set up under *one* user. 

```bash
pm2 unstartup
pm2 startup -u username --hp=/home/username
```

Note that `pm2 unstartup` is needed with any changes, 
including updating pm2. 

To tell pm2 which programs to run, 
get all of the programs running the way you want them,
then use `pm2 save`, e.g.

```bash
pm2 start my-node-app/dist/index.js --name "pnr"
pm2 save
```

to save off the configuration file that pm2 will use to start the process(es) upon startup. 

You can then reboot the machine to test that the node programs were restarted correctly. 
