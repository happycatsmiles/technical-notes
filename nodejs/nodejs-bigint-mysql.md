# NodeJS + MySQL `BIGINT` Fields

Because JavaScript does not support 64-bit integers, 
reading from a 64-bit database field (i.e. `BIGINT`)
can cause wrong data to be passed back and forth. 
This can also happen with the `DECIMAL` field.

The NPM packages 
[mysql2](https://www.npmjs.com/package/mysql2) 
and
[mysql](https://www.npmjs.com/package/mysql)
both have the configuration options

```javascript
{
    supportBigNumbers: true,
    bigNumberStrings: true
}
```

which will treat `DECIMAL` and `BIGINT` fields as strings. 

More information in the 
[mysql package documentation](https://www.npmjs.com/package/mysql#connection-options).
