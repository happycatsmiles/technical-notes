# Using `npm` in CI/CD

Using

```
npm i
```

is not guaranteed to install the same packages represented in the `package-lock.json`.
Use

```
npm ci
```
