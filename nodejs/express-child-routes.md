# Child Routes in Express

In the `index.ts` file,
the API routes can be caught with:

```ts
app.use('/app/v1/', webAppRoutes())
```

which can delegates to its children:

```ts
...
import { unknownRoute } from '../../common/express/middleware/unknown-route.middleware'

const Router = require('express').Router

export function webAppRoutes(): typeof Router {

  const router = new Router({mergeParams: true})

  // For /app/v1/factions...
  router.use('/factions', fmFactionsRoutes())
  // For /app/v1/players
  router.use('/players', fmPlayersRoutes())

  // All other web API endpoints get caught here in 404 land.
  // This is only needed at the top-most child,
  // unless doing something fancy.
  router.use(unknownRoute)

  return router
}
```

## Merging Parameters

The child routes need to include `mergeParams`:

```ts
  const router = new Router({mergeParams: true})
```

so all of the nested parameters end up in `req.params` as one would expect.

