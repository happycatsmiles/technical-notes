# Extracting Command Line Arguments With `nconf`

One may extract an array of command-line arguments using `nconf` thus:

```ts
const args :string[] = nconf.get('_') ?? []
```
