# Install NodeJS on CentOS Via Package Manager

The NodeJS.org site has notes for installing from a package manager 
at https://nodejs.org/en/download/package-manager/

Be sure to use the right setup version for the NodeJS version you want. 
For example, the instructions only have version 8 shown but the current version is 9. 
You can tweak the setup version like this:

```
curl --silent --location https://rpm.nodesource.com/setup_9.x | sudo bash -
```

The downside to this, is installing or updating global packages requires `sudo` access. 

## Untested Alternative Method

I haven't tried this yet, but the method on DigitalOcean at 
https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-centos-7
