# NodeJS `debug` Using `import`

```ts
import debugFn from 'debug'
const debug = debugFn('foo')
```
