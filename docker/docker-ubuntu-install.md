# Setting Up Ubuntu As Docker Node

## DigitalOcean

Be sure to put the new droplet in the right virtual network.
That can't be changed later.

Ensure the right tag is used so the firewall allows traffic between nodes.

## Install Docker

DigitalOcean's documentation usually has a good article for the particular version of Linux.
For example:
[How To Install and Use Docker on Ubuntu 22.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04)

## Firewalls

If needed, one can open the Docker-specific ports.
`ufw` has a quirk where these need to be manually done one after another.

**NOTE:** 

```bash
sudo ufw allow 2377/tcp
sudo ufw allow 7946/tcp
sudo ufw allow 7946/udp
sudo ufw allow 4789/udp
sudo ufw reload
sudo ufw enable
sudo ufw status
```

To get more detailed information, one can use:

```bash
sudo ufw status verbose
```
