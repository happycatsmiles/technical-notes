# Deploying a Static Angular App on Docker Swarm using nginx and Gitlab

These are accumulated notes
about the process of:

* Use Gitlab CI to automatically:
  * Compile an Angular web app to static files.
  * Package the static files in a Docker nginx container.
* Using a Docker swarm configs to configure nginx.
* Deploy the Docker stack.

## Building the App

### First Step

Ensure the app can be built by hand from a clean checkout.
Angular sets the NPM `package.json`'s `scripts` section to make this straightforward.
For example:

```sh
git clone ...  my-awesome-app
cd my-awesome-app
npm install
npm run build
```

### GitLab CI

The `.gilab-ci.yml` file needs a compile stage to produce the static files.
In part,

```yml
compile_dev:
  stage: compile
  image: node:12
  script:
    - npm install
    - npm run build
  artifacts:
    paths:
      - dist/
```

## Building the Container

A `Dockerfile` in the Angular project's root directory
can now be used to create the container.
This example copies the static files to `/opt`.
The `nginx` config will need to serve from `/opt`.

```yml
FROM nginx:stable-alpine

RUN mkdir -p /opt

# The new program
COPY dist/ /opt

EXPOSE 80
```

The `.gitlab-ci.yml` (in part):

```yml
containerize_dev:
  stage: containerize
  image: docker:latest
  services:
    - docker:dind
  dependencies:
    - compile_dev
  environment:
    name: dev
  script:
    - echo "Environment $CI_ENVIRONMENT_NAME $CI_ENVIRONMENT_SLUG"
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    - docker build -t registry.gitlab.com/my/awesome-app/dev .
    - docker push     registry.gitlab.com/my/awesome-app/dev
  only:
    - develop
```

## Configuration Files

Supplying the container's nginx with a configuration file via Docker configs
allows the configuration to be manipulated without having to rebuild the container.

For testing,
a simple config file along the lines of the following should work:

```conf
# One worker process per container.
worker_processes 1;

events { worker_connections 1024; }

http {
  include /etc/nginx/mime.types;
  sendfile on;

  server {
    listen 80;

    location / {
      root /opt;
      index index.html;
    }
  }
}
```

## Deploying the Container in a Stack

Deploying the container is straightforward.

* Map the port appropriately.
* Map the nginx configuration file.
* Set replication as needed.

The container's stack portion may look like:

```yml
 container_label:
    image: registry.gitlab.com/my/awesome-app/dev:latest
    deploy:
      replicas: 1
      restart_policy:
        condition: any
    configs:
      - source: container_config
        target: /etc/nginx/nginx.conf
        mode: 0444
    ports:
      - "5000:80"
    networks:
      - net
```

If using straightforward Docker configs,
the declaration may look like:

```yml
configs:
  container_config:
    external: true
    name: config.my.awesome-app.v4.beta.json
```

## Deploying Behind a Reverse Proxy

There should be no problems
with the container running behind an nginx reverse proxy
if the proxy rewrites the URL appropriately.

If the reverse proxy places the web app under a base directory,
e.g. `https://example.com/my-awesomeapp`,
the NPM `package.json` scripts may need to be tweaked.

Similarly, for different builds,
such as a beta vs. production build,
the NPM `package.json` scripts may need to be tweaked.

```json
{
  ...
  "scripts": {
    "build": "ng build --prod --base-href=/my-awesome-app/",
    "build-beta": "ng build --configuration=beta --base-href=/my-awesome-app/",
    ...
}
```
