# Why `bcrypt`? 

Jan 3, 2019

In the Docker
[registry setup documentation](https://docs.docker.com/registry/recipes/nginx/#setting-things-up)
It uses `nginx:alpine` with the note 

> Only nginx:alpine supports bcrypt.

The significance of `bcrypt` is explained
[here](https://security.stackexchange.com/questions/4781/do-any-security-experts-recommend-bcrypt-for-password-storage).
