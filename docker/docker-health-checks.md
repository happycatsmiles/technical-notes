# Docker Health Checks

Docker stacks have a `healthcheck` option for containers.
The healthcheck is a command that's run *within the container*.
Its exit status indicates success or failure.

## HTTP

HTTP-enabled resources can use `curl` for health checks.

```yml
    healthcheck:
      test: curl -f http://localhost:3000/health || exit 1
```

### Alpine Containers

Containers based on Alpine Linux can install `curl` via the `Dockerfile`:

```sh

```

## MariaDB

There is a [healthcheck.sh](https://mariadb.com/kb/en/using-healthcheck-sh-script/) script.

## Redis

```yml
    healthcheck:
      test: redis-cli ping || exit 1
```

## RabbitMQ

RabbitMQ has its own health check.

```yml
    healthcheck:
      test: rabbitmq-diagnostics -q check_running
```

## Resources

* [Docker HEALTHCHECK documentation](https://docs.docker.com/engine/reference/run/#healthcheck)
  * This demonstrates how to examine the status manually.
