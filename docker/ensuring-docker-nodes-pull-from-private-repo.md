# Ensuring Docker Nodes Pull From Private Repository

Jan 10, 2019

A person was having problems with 
`docker stack deploy` 
not replicating containers 
in a private registry
across a cluster.

The answer that solved the problem was:

  Do you have authentication on the registry? 
  If so you may need to do a `docker login` on the box 
  where you're deploying the stack and then 
  run the deploy with `--with-registry-auth`.
   
  For example: `docker stack deploy --with-registry-auth -c stack.yml test`
