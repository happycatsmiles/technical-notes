# `docker stop` vs `docker kill`

Jan 3, 2019

`docker stop` will request a graceful process shutdown with `SIGTERM`.

`docker kill` will force process termination with `SIGKILL`.

**Note:** Filesystem changes are kept. 
The container is not destroyed. 
The processes inside the container are affected. 

If you wish to start the container fresh, 
destroy it and create a new container. 
