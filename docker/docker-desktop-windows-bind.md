# Creating a Bind Mount for a Container in Docker Desktop for Windows

Access to the local drive is disabled by default.
To enable access to the local drive: 

*   Right click on the Docker whale icon `> Settings > Resources > File Sharing`.
*   Check the correct drive, e.g. drive `C`.
*   Use Unix-style paths starting with the drive letter.
    For example, `"/c/Users/example/my-project/config.json"`. 

The container should be able to start now.
