# Manage Docker Node Labels

## List Labels

It's handy to create the following alias for bash:

```sh
alias labels=' docker node ls -q | xargs docker node inspect   -f '\''{{ .ID }} [{{ .Description.Hostname }}]: {{ range $k, $v := .Spec.Labels }}{{ $k }}={{ $v }} {{end}}'\'''
```

## Add Label

```sh
docker node update --label-add key=value node-01
```
