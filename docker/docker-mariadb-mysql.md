# Running MariaDB in Docker

Secrets and configs are effective tools
form minimizing the amount of information in the deployment scripts.

## Durable Storage

* The database files are stored on the regular file system.
* Volumes have been found to be too difficult to deal with, e.g. relocating the database or backups.
* The directory owner/group should be `999.999`.

```sh
cd /home
sudo mkdir -p mariadb.core.prod/mysql
sudo chown -R 999.999 mariadb.core.prod/
```

## Docker Node Label

* The docker node that hosts the durable storage needs a label so Docker can ensure
  the container runs on the correct node.

```sh
docker node update --label-add mariadb.core.prod=true db-01
```

```yml
services:
  database:
    image: mariadb:10.8
    deploy:
      replicas: 1
      restart_policy:
        condition: any
      placement:
        constraints:
          - node.labels.${DB}.${STAGE}==true
  ...
```


## Docker Network

* An automatically-created docker network exists for each deploy.
* Database clients attach to the network to communicate with the database.
* Details:
  * Driver: Overlay
  * Enable manual container attachment: Yes
* Example:
  * `mariadb_core_prod_net`

```yml
services:
  database:
    ...
    networks:
      - net
...

networks:
  net:
    driver: overlay
    attachable: true
```

## Root Password

* The root password can be stored as a secret.
* One presents the password as a file.
* The configuration uses this file as a source for obtaining the password.

```yml
services:
  database:
   ...
    environment:
      - MYSQL_ROOT_PASSWORD_FILE=/tmp/root_pwd
    ...
    secrets:
      - source: db_root_pwd
        target: /tmp/root_pwd
        mode: 0444

secrets:
  db_root_pwd:
    external: true
    name: secret.${DB}.mysql-root-password.v1.${STAGE}.txt
```

## Configuration

* This supports multiple databases and replica instances.
* One can divide the configuration into two broad divisions:
  * The non-replicate part common to all the databases and replicas.
  * The part unique to specific replicas or masters.
* Each unique part exists in its own Docker config.
* The configuration glues everything together.

```yml
version: "3.7"
services:
  database:
  ...
    configs:
      - source: db_config
        target: /etc/mysql/conf.d/config.cnf
        mode: 0444
      - source: repl_config
        target: /etc/mysql/conf.d/repl.cnf
        mode: 0444
    ...

configs:
  db_config:
    external: true
    name:  config.${DB}.common-conf.v1.${STAGE}.ini
  repl_config:
    external: true
    name:  config.${DB}.repl-conf.v1.${STAGE}.ini
```

## Example Files

### Stack Example

```yml
version: "3.7"
services:
  database:
    image: mariadb:10.8
    deploy:
      replicas: 1
      restart_policy:
        condition: any
      placement:
        constraints:
          - node.labels.${DB}.${STAGE}==true
    environment:
      - MYSQL_ROOT_PASSWORD_FILE=/tmp/root_pwd
    configs:
      - source: db_config
        target: /etc/mysql/conf.d/config.cnf
        mode: 0444
      - source: repl_config
        target: /etc/mysql/conf.d/repl.cnf
        mode: 0444
    secrets:
      - source: db_root_pwd
        target: /tmp/root_pwd
        mode: 0444
    volumes:
      - "/home/${DB}.${STAGE}/mysql:/var/lib/mysql"
    ports:
      - "${PORT}:3306"
    networks:
      - net

configs:
  db_config:
    external: true
    name:  config.${DB}.common-conf.v1.${STAGE}.ini
  repl_config:
    external: true
    name:  config.${DB}.repl-conf.v1.${STAGE}.ini

secrets:
  db_root_pwd:
    external: true
    name: secret.${DB}.mysql-root-password.v1.${STAGE}.txt

networks:
  net:
    driver: overlay
    attachable: true
```

### Common Configuration

* Example `config.blackpaw.mariadb.main.common-conf.v1.beta.ini`

```conf
[mysqld]
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock

max_allowed_packet=500M

# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0

#
# Set character sets to handle full UTF-8.
#
[client]
default-character-set=utf8mb4

[mysql]
default-character-set=utf8mb4

[server]
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_general_ci
init-connect='SET NAMES utf8mb4'

#
# Settings common to both master and slave databases.
#
[mysqld]
innodb_file_per_table=ON
innodb_log_file_size=256M
bind-address=0.0.0.0
expire-logs-days=94

# Set this so changing hosts doesn't blow up.
# From the docs:
# We strongly recommend you use either --log-basename or specify a filename to ensure that replication doesn't stop if the hostname of the computer changes.
log-basename=db1
log-bin

binlog-format=MIXED

# Performance Schema
performance_schema=ON
performance-schema-instrument='stage/%=ON'
performance-schema-consumer-events-stages-current=ON
performance-schema-consumer-events-stages-history=ON
performance-schema-consumer-events-stages-history-long=ON
```

### Master/Replication Configuration

* Example `config.blackpaw.mariadb.main.repl-conf.v1.beta.ini`:

```conf
# MASTER/SLAVE REPLICATION
#
# Master 1

[mysqld]
server-id=1
```
