# Logging in Docker Containers

* There are extensive options for logging in Docker containers.
* There are many drivers available.
* The default is a JSON driver.
* Maximum size and auto-log rotation can be specified.

## Example

The following example turns on debugging information for application-specific events,
sets the maximum log file to 10MB, and
rotates logs automatically,
keeping a total of 10 log files:

```yml
services:
  server:
    image: example/dev:latest
    deploy:
      replicas: 1
      restart_policy:
        condition: any
      placement:
        constraints:
          - node.labels.core == true
    environment:
      DEBUG: 'app:*'
    secrets:
      - source: service_config
        target: /app/config.json
        mode: 0444
    ports:
      - "3003:3000"
    networks:
      - db
    logging:
      driver: "json-file"
      options:
        max-size: "10m"
        max-file: "10"
```
