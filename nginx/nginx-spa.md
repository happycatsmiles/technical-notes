# nginx Serving Single-Page Application

Single-page apps handle their own internal routing once the main page is loaded.

Returning to one of these internal routes directly can cause nginx to return a 404 error.

The nginx configuration option `try_files` tell nginx how to fall back when it can't find a file.

For example,
a simple nginx config for a container that serves a SPA might look like the following
where `try_files` tells nginx to fall back to `/index.html`,
which will load the SPA, which will then handle the internal route.

```nginx
#
# This is the configuration for nginx inside the SPA web app containers.
#
worker_processes auto;
worker_cpu_affinity auto;

events { worker_connections 1024; }

http {
  include /etc/nginx/mime.types;
  sendfile on;

  server {
    listen 80;

    location / {
      root /opt;
      index index.html;
      try_files $uri $uri/ /index.html;      
    }
  }
}
```
