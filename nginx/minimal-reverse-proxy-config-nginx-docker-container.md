# Minimal nginx Reverse Proxy Configuration for Docker Containers

Jan 11, 2019

Note: This is suitable for demonstration purposes.
It's not intended to be a proper production configuration. 

Given an upstream server named `foo` on port 3000,

```
# One worker process per container.
worker_processes  1;

events { worker_connections  1024; }

http {
    sendfile        on;

    upstream foo-svc {
        server foo:3000;
    }

    server {
         listen 80;

         location / {
            proxy_pass         http://foo-svc;
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
        }
    }
}
```
