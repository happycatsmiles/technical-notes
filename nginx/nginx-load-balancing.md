# Load Balancing With ngingx

The default round-robin method of load balancing is: 

```
http {
    upstream myapp1 {
        server srv1.example.com;
        server srv2.example.com;
        server srv3.example.com;
    }

    server {
        listen 80;

        location / {
            proxy_pass http://myapp1;
        }
    }
}
```

To use the least number of connections method of load balancing, 
add `least_conn` to the `upstream` section of the configuration.

```
http {
    upstream myapp1 {
        least_conn;

        server srv1.example.com;
        server srv2.example.com;
        server srv3.example.com;
    }

    server {
        listen 80;

        location / {
            proxy_pass http://myapp1;
        }
    }
}
```


## Reference

[Using nginx as HTTP load balancer](https://nginx.org/en/docs/http/load_balancing.html)
