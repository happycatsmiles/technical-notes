# HTTP Strict Transport Security On nginx

Jan 2, 2019

Set the `Strict-Transport-Security` header in each server block to enable HTTP Strict Transport Security.

```
    add_header Strict-Transport-Security "max-age=31536000; includeSubdomains" always;
```
