# Install nginx on CentOS 7

* nginx is part of EPEL. 
* A barebones CentOS 7 install may not have `firewalld` installed.

```
yum install -y epel-release
yum install -y nginx
systemctl enable nginx
systemctl start nginx
yum install -y firewalld
systemctl enable firewalld
reboot
firewall-cmd --permanent --zone=public --add-service=http 
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload
```

The configuration information will be in the directory `/etc/nginx`. 
