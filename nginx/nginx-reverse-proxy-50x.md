# 502: Connection refused while connecting to upstream
# 504: Gateway Time-out

This can be tricky to figure out.

1. Check, double-check, triple-check the API URI call is correct.
   1. I've wasted hours when it turns out the API URI was in error.
   2. For example, https://example.com/main-web-api-beta/healthcheck
      https://example.com/main-web-api/healthcheck
      i.e. `-beta` was missing from the URI.
2. Ensure the container is healthy.
3. Ensure the container is available via HTTP from within the cloud.
   1. e.g. curl http://127.0.0.1:3014/version
4. Ensure the container is available via HTTP from the web host.
   1. Reboot web server?
   2. Are other services available from the web host?
5. Everything seemed fine.
   1. Update `Dockerfile`` and `.gilab-ci.yml`.
   2. Beta deployed fine; it works.
   3. Prod deployed fine; it works **now**.



