# Install Redis on CentOS 7

```
yum install epel-release
yum install redis
systemctl start redis
systemctl enable redis
```

See the Redis documentation to determine if boosting the number of open handles per process is a good idea. 
