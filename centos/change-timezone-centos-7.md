# Change timezone on CentOS 7

Timezones are managed with `timedatectl`. 

```bash
timedatectl list-timezones | less
timedatectl set-timezone America/Los_Angeles
```
