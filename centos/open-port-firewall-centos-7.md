# Open a Port on the CentOS 7 Firewall

> **!!! WARNING !!!** 
> If running Docker, this will bung up Docker's firewall settings.
> You must restart the Docker service.

To open (for example), port 5000: 

```bash
firewall-cmd --zone=public --add-port=5000/tcp --permanent
firewall-cmd --reload
```

## Supplemental Information

### Install `firewalld`

```bash
sudo yum install firewalld
sudo systemctl enable firewalld
reboot
```

### Move NIC to a Zone 

```bash
sudo firewall-cmd --zone=home --change-interface=eth0 --permanent
```

### Craete a Specialized Zone

For example, allow access to MySQL from another machine on a private network.

```bash
sudo firewall-cmd --new-zone=example --permanent
sudo firewall-cmd --zone=example --add-source=10.0.17.1/32 --permanent
sudo firewall-cmd --zone=example --add-port=3306/tcp --permanent
sudo firewall-cmd --reload
```

### Reload

Not all changes will be applied immediately.
Remember to reload.

```bash
sudo firewall-cmd --reload
```

### References

https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-using-firewalld-on-centos-7
