# Let's Encrypt Renewal

One can test a potential renewal with:

```sh
sudo certbot renew --dry-run
```

One can force a renewal with:

```sh
sudo certbot renew
```
