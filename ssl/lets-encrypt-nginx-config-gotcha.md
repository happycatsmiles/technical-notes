# Let's Encrypt nginx Config Gotcha

The Certbot script used by [Let's Encrypt](https://letsencrypt.org/) can create some confusion when run against an nginx server. 

Certbot finds the 

```
server { 
...
```

section of the configuration file and *appends* a modified copy of it to the config file. It does not touch the original server section. 

If you need to modify the configuration, 
you must **not** modify the original section since it will be ignored when nginx hits the *next* (modified) server section. 

It may be wise to comment out or simply delete the original section. 
