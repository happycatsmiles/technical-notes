# `my.cnf` Underscores or Hyphens

Jan 17, 2019

Both are valid and can be mixed.

The behavior of both MariaDB and MySQL is as if the regex `/[-_]/` is used when parsing configuration files. 

For consistency, pick one and run with it. 
