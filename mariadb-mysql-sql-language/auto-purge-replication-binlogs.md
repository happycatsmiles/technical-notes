# Auto-Purge MariaDB/MySQL Replication binlogs

Auto-purging replication binlogs are controlled by the 
[expire_logs_days](https://mariadb.com/kb/en/library/replication-and-binary-log-server-system-variables/#expire_logs_days)
server paramter. 
By default, this value is `0`, meaning no auto-purging. 

Once this value has been set to a positive integer,
logs older than that number of days are *candidates* for auto-purging. The binlogs are not actually purged until:

1.  the server restarts,
2.  the current binlog reches its maximum size and a new binlog is created,
3.  or the binlogs are manually purged via SQL.
