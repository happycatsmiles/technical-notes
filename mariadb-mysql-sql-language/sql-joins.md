# SQL JOINs

The `JOIN` syntax in SQL is pretty straightforward once understood as set operations with a set (table) on the left and a set (table) on the right. 

## Summary

Given two tables `ATABLE` with M rows, and `BTABLE` with N rows, we can imagine the following set operations: 

Join Type | Desired Rows | Row Count | Description 
--- | --- | --- | --- 
INNER | A∩B | [0..min(M, N)] | Intersection between `ATABLE` and `BTABLE`. This includes only those records which are common between the two.
LEFT (INNER) | A | M | All records in `ATABLE` with those in `BTABLE` that may match, for a total of M rows. Missing rows in `BTABLE` are simply `NULL` in all columns. Typically `BTABLE` is a child of `ATABLE`. 
RIGHT (INNER) | B | N | All records in `BTABLE` with those in `ATABLE` that may match, for a total of N rows. Missing rows in `ATABLE` are simply `NULL` in all columns. Typically `ATABLE` is a child of `BTABLE`.  
OUTER | C(A,B) | M×N | Cartesian product of all records in `ATABLE` paired with all records in `BTABLE`, for a total of M×N rows. 

### Inner Join (Intersection) 

If `ATABLE` and `BTABLE` have a common field `ATABLE.ID` and `BTABLE.ID`, then these two are equivalent: 

```sql
SELECT
	A.*, B.*
FROM
	`ATABLE` A,
	`BTABLE` B
WHERE
	A.ID = B.ID;
```
and
```sql
SELECT
	A.*, B.*
FROM
	`ATABLE` A 
	JOIN `BTABLE` B ON A.ID = B.ID;
```

I personally find the first (`WHERE A.ID = B.ID`) clearer in most cases. 

### Left Join (`ATABLE` Supplemented By `BTABLE`) 

This is a common case for including supplementary information that may (or may not) exist in another table. However, the non-existence of matching `BTABLE` records won't affect the rows returned from `ATABLE`. 

If `ATABLE` has the field `ATABLE.ID` and `BTABLE` refers to the first via field `BTABLE.A_ID`, then we can have: 

```sql
SELECT
	A.*, B.* 
FROM
	ATABLE A
	LEFT JOIN BTABLE B ON A.ID = B.A_ID;
```

The `LEFT JOIN` says, "we want everything on the left-hand table (`ATABLE`)." 

#### Left Join With Multiple Tables

If `TABLEA` has  two child tables, `BTABLE` and `CTABLE`, then we can include these by: 

```sql
SELECT
	A.*, B.*, C.*
FROM
	ATABLE A
	LEFT JOIN BTABLE B ON A.ID = B.A_ID
	LEFT JOIN CTABLE C ON A.ID = C.A_ID;
```

The `LEFT JOIN` says, "we want everything on the left-hand table (`ATABLE`)."  

### Right Join (`BTABLE` Supplemented by `ATABLE`) 

This is a very rare case. If you need it, look it up to be sure this is what you want. 
If it really is needed, the syntax will be the same as the `LEFT JOIN` examples, except with `RIGHT JOIN`. Be sure this is really what you want. 
The `RIGHT JOIN` says, "we want everything on the right-hand table (`BTABLE`)."  

### Outer Join (Cartesian Product)

This is an unusual case. It returns each row of `ATABLE` repeated with each row of `BTABLE`. 

```sql
SELECT
	A.*, B.* 
FROM
	ATABLE A
	OUTER JOIN BTABLE B ON A.ID = B.ID;
```
