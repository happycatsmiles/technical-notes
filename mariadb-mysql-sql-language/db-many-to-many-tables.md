# Many to Many Tables

After some serious heartburn with many:many tables with two columns
(primary key for tale A and primary key for table B),
it seems prudent to:

1. Let this table have its own primary key.
2. Be cautious about using constraints to enforce business logic.
   This is a database, not a logic engine.
3. Use the right tool for the job.


