# SELECT INTO

In stored procedures, there is a warning:

`<select expression> INTO <destination>;`
is deprecated and will be removed in a future release.
Please use

```sql
SELECT <select list>
INTO <destination>
FROM ...
```

instead.
