# MySQL Dump/Load Database

This is very specific and is probably not suitable for general use. 

```bash
time mysqldump -u <admin> -p <schema> --complete-insert --skip-opt --add-drop-table --add-locks --create-options --lock-tables --default-character-set=utf8 | bzip2 -9v > /tmp/dump.sql.bz2

 time bzcat /tmp/dump.sql.bz2 | mysql -u <dbuser> -p <schema>
 ```
 