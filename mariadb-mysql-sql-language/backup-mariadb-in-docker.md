# Backup MariaDB Running in a Docker Container

There are a few moving parts:

1. A script to perform all the operations.
2. A crontab file to run the script regularly.
3. A directory to hold the backups.
4. Accounts on other machines that can be `rsync`ed with passwordless `ssh`.

## The Script

This script reaches into the container to pull out information such as password.

* The file controlling the backup live in `/home/backups/bin`.
* The container with the MariaDB instance is called `main1_database`.
* The name of the database is `db_name`.
* This accesses Docker a secret with the database root password that is injected into `/tmp/root_pwd`.
* Backups are stored in `home/backups/database-backups`.
* The last 32 backups are kept.
* Copies of the backups are `rsync`ed to three other machines.

```bash
#!/bin/bash
docker exec $(docker ps -q -f name=main1_database) /usr/bin/mysqldump --default-character-set=utf8mb4 --skip-set-charset --events --routines --triggers --hex-blob --master-data=2 --single-transaction --quick --gtid --socket=/var/lib/mysql/mysql.sock --user root --password=$(docker exec $(docker ps -q -f name=main1_database) cat /tmp/root_pwd) db_name | bzip2 -9 > /home/backups/database-backups/database-backup-$(date +"%Y-%m-%d_%H-%M-%S").sql.bz2

# Keep the last 32 backups.
find /home/backups/database-backups -name 'database-backup-*' | sort -r | tail -n +33 | xargs rm -f
rsync -avW --delete-before /home/backups/database-backups/ -e "ssh -l backups" host1:/home/backups/database-backups
rsync -avW --delete-before /home/backups/database-backups/ -e "ssh -l backups" host3:/home/backups/database-backups
rsync -avW --delete-before /home/backups/database-backups/ -e "ssh -l backups" host4:/home/backups/database-backups
```

## The `crontab` File

This is a file called `crontab` which can be installed with:

```bash
crontab crontab
```

The file:

```conf
MAILTO="sysadmin@example.com"
30 4 * * * /home/backups/bin/backup-database.sh
```
