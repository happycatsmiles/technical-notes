# MySQL Equivalent of JavaScript `Date.now()` 

This should produce the equivalent of the JavaScript `Date.now()` function.

```sql
select floor(unix_timestamp(now(3))*1000);
```
