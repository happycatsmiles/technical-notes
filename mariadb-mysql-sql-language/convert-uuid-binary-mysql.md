# Convert UUID to Binary in MySQL/MariaDB 

This is suitable for storing in a BINARY(16) column for indexing. 

```sql
UNHEX(REPLACE(UUID(), '-', ''))
```

There are other caveats. See [here](https://stackoverflow.com/questions/15473654/mariadb-before-insert-trigger-for-uuid) for an example. 
