# Restrict Access To Specific Views in MySQL / MariaDB

If a database user should have no access to a schema 
except to `SELECT` one or more specific views in that restricted schema,
access can be granted with: 

```sql
GRANT SELECT ON `restricted_schema`.`specific_view` TO `user`@`host`;
FLUSH PRIVILEGES;
```

If you use a tool like MySQL Workbench to log in as that user,
the restricted schema will now show up with only the `specific_view` visible. 
