# Python 2.7 Virtual Environments

First ensure that `pip` and virtual environment support is installed. 

```sh
virtualenv --version
```

If you need to install this for Ubuntu,

```sh
sudo apt-get install python-pip
sudo pip install virtualenv virtualenvwrapper
echo "export WORKON_HOME=~/Env" >> ~/.bashrc
echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc
source ~/.bashrc
```

The `echo` commands add support to the command line. 

## Creating

To create a Python virtual environment called `myenv` use `mkvirtualenv`. 

```sh
$ mkvirtualenv myenv
New python executable in /home/hcs/Env/myenv/bin/python
Installing setuptools, pip, wheel...done.
virutalenvwrapper.user_scripts creating /home/hcs/Env/myenv/bin/predeactivate
virutalenvwrapper.user_scripts creating /home/hcs/Env/myenv/bin/postdeactivate
virutalenvwrapper.user_scripts creating /home/hcs/Env/myenv/bin/preactivate
virutalenvwrapper.user_scripts creating /home/hcs/Env/myenv/bin/postactivate
virutalenvwrapper.user_scripts creating /home/hcs/Env/myenv/bin/get_env_details
(myenv) $
```

## Activating 

To activate a virtual environment called `myenv` use

```sh
$ source myenv/bin/activate
(myenv) $
```

Note that this should display the name of the virtual environment before the prompt, e.g. `(myenv)`. 

## Deactivating

To deactivate a Python virtual environment use

```sh
(myenv) $ deactivate
$
```

The prompt should return to the regular prompt without the virtual environment name in parentheses.
