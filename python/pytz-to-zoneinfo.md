# Python pytz to zoneinfo

## Requirements

* Python 3.9 introduces `zoneinfo`.
* Be sure to include `tzdata` as a dependency.
* The `datetime` object understands `ZoneInfo` classes.

## Use

| pytz                                    | zoneinfo                      |
| :-------------------------------------- | :---------------------------- |
| `pytz.timezone(x)`                      | `zoneinfo.ZoneInfo(x)`        |
| `from django.utils.timezone import utc` | `from common.date import utc` |

```py
from zoneinfo import ZoneInfo
from datetime import datetime, timedelta
dt = datetime(2020, 10, 31, 12, tzinfo=ZoneInfo("America/Los_Angeles"))
```

## Django

from django.utils.timezone import utc
