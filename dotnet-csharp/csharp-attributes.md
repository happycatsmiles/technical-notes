# C# Attributes

Attributes are a special syntax for adding class instances
to various bits of code as metadata.
This metadata can be access through reflection.

The following code looks for a class called `ObsoleteAttribute` and instantiates it,
attaching it as metadata to the class `Foo`.

```cs
[Obsolete]
class Foo
```

The following code looks for a class called `ConditionalAttribute`,
creates two separate instances of it,
and attaches them as metadata to `TraceMethod`.

```cs
[Conditional("DEBUG"), Conditional("TEST1")]
void TraceMethod()
{
    // ...
}
```

Attribute class properties may be set with
`Name = Value` parameters.
Thus,

```cs
[Column("url", TypeName = "varchar(200)")]
```

can be thought of as 

```cs
new ColumnAttribute("url") { TypeName = "varchar(200)" }
```

## Custom Attributes

In Django, one may create custom column names by subclassing.
A similar thing can be done with C# attributes.

For example, to encapsulate the attribute:

```csharp
[Column(TypeName = "TIMESTAMP")]
public DateTime WhenCreated;
```

one can:

```csharp
class TimeStampColumAttribute : ColumnAttribute
{
    public TimeStampColumAttribute()
        : base()
    {
        TypeName = "TIMESTAMP";
    }
}

[TimeStampColum]
public DateTime WhenCreated;
```

## References

* [Attributes (C#)](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/attributes/)
* [Use Attributes in C#](https://docs.microsoft.com/en-us/dotnet/csharp/tutorials/attributes)
