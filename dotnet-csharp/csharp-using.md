# C# `using` Declaration

The old `using` statement:

```cs
using (FileStream f = new FileStream(@"C:\file.txt")) 
{
// statements
}
```

This has been replaced by a `using` declaration added to the variable declaration.
It is automatically cleaned up at the end of the scope it's declared in.

```cs
using FileStream f = new FileStream(@"C:\file.txt");
```

* [Documentation](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/proposals/csharp-8.0/using)


