# Entity Framework Core Migrations

## Create a New Migration

```sh
dotnet ef migrations add <name>
```

This creates a new migration with the form:

`yyyymmddhhmmss_<name>`

### Creation Notes

* It may be helpful to create an initial migration that is empty.
* This will give a "clean" starting place during development.

## Apply Migrations

This will apply all outstanding migrations:

```sh
dotnet ef database update
```

## Roll Back Applied Migrations (Or Roll Forward)

To move back and forth among migrations,
give the name of the migration to change to.

```sh
dotnet ef database update <name>
```

For example,
if the migrations `Init` and `Update01` exist,
and were both applied to the database,
the following would roll back from `Update01` to `Init`:

```sh
dotnet ef database update Init
```

## Remove The Last Migration

To remove a migration,
first the migration must not be applied to the database.
Roll back the applied migrations.

Then use the command:

```sh
donet ef migrations remove
```


## References

* [Migrations Overview](https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli)
* [Managing Migrations](https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/managing?tabs=dotnet-core-cli)
