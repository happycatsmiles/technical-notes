# Blazor Singleton Service

## Create the Service Class

Create a `Services` directory and create a class:

```cs
namespace AppDataService.Services
{
    public class AppData
    {
        public int Age { get; set; }
    }
}
```

## Register the Service

In WebAssembly,
open `Program.cs`:

```cs
public static async Task Main(string[] args)
{
    var builder = WebAssemblyHostBuilder.CreateDefault(args);
    builder.RootComponents.Add<App>("app");

    builder.Services.AddScoped(
        sp => new HttpClient {BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)}
    );

    // HERE
    builder.Services.AddSingleton(new Services.AppData());

    await builder.Build().RunAsync();
}
```

## Inject the Service Into the Page

The following will inject
`Services.AppData`
with the name `AppData`:

```cs
@page "/"
@inject Services.AppData AppData
```

## Change Detection

Two components are sharing the same singleton on the same page,
two parts are needed:

1. The components using the service need to listen for the change event.
2. The *service* needs an `OnChange` event along with triggering it.

```cs
@implements IDisposable
...
@code {
    protected override void OnInitialized()
    {
        AppData.OnChange += StateHasChanged;
    }

    void IDisposable.Dispose()
    {
        AppData.OnChange -= StateHasChanged;
    }
}
```

```cs
using System;
namespace AppDataService.Services
{
    public class AppData
    {
        private int _number;
        public int Number
        {
            get
            {
                return _number;
            }
            set
            {
                _number = value;
                NotifyDataChanged();
            }
        }

        private string _color;
        public string Color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
                NotifyDataChanged();
            }
        }

        public event Action OnChange;

        private void NotifyDataChanged() => OnChange?.Invoke();
    }
}```
