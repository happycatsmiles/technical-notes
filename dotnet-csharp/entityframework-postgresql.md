# EntityFramework Core and PostgreSQL

## Preparation

[This install guide](https://docs.microsoft.com/en-us/ef/core/get-started/overview/install)
states to install `dotnet ef`.

```sh
dotnet tool install --global dotnet-ef
```

or update with

```sh
dotnet tool update --global dotnet-ef
```

Packages:

* `Microsoft.EntityFrameworkCore`
* `Microsoft.EntityFrameworkCore.Design`
* `Microsoft.EntityFrameworkCore.Tools`
* `Npgsql`
* `Npgsql.EntityFrameworkCore.PostgreSQL`

Additional packages:

* `EFCore.NamingConventions` to allow for `snake_case`.
* `Npgsql.EntityFrameworkCore.PostgreSQL.NodaTime` to support `NodaTime`.

If starting a new project, enable nullable reference types.

## Create Models

Models, at their simplest, are simple classes.

```cs
namespace Foo.Models 
{
    public class Creature
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
```

## Create Database Context

The bare bones context can be configured thus:

```cs
using Foo.Models;
using Microsoft.EntityFrameworkCore;

namespace Foo.DbContexts
{
    public class CreatureContext : DbContext
    {
        const string DefaultSchema = "animals";

        public DbSet<Creature> Creatures { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options
                .UseNpgsql(
                    // TODO: This should not be hard-coded or checked in, obviously.
                    @"Host=localhost;Username=postgres;Password=asdfasdf;Database=animals",
                    o =>
                    {
                        // Improved time handling in .NET.
                        o.UseNodaTime();
                        // Set the default schema
                        o.MigrationsHistoryTable(
                            HistoryRepository.DefaultTableName,
                            DefaultSchema
                        );
                    })
                // Use `snake_naming` for tables.
                .UseSnakeCaseNamingConvention();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Define the default schema here.
            modelBuilder.HasDefaultSchema(DefaultSchema);

            // Define default column values here.
            modelBuilder.Entity<Creature>()
                .Property(b => b.WhenCreated)
                .HasDefaultValueSql("NOW()");
        }    }
}
```

### Changing Default Naming

Adding the package `EFCore.NamingConventions` will give the ability to default to
`snake_case` for table and column names.

```cs
protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    => optionsBuilder
        .UseNpgsql(...)
        .UseSnakeCaseNamingConvention();
```

https://www.npgsql.org/efcore/modeling/table-column-naming.html

### Better Date/Time Handling

The `Npgsql.EntityFrameworkCore.PostgreSQL.NodaTime` package uses the improved
`NodaTime` package to provide improved date/time handling over the broken
default .NET types.

https://www.npgsql.org/efcore/mapping/nodatime.html

#### PostgreSQL `timezonetz`

PostgreSQL recommends using `timezonetz` to achieve UTC timestamps.

Use the NodaTime `Instant` class.
[Docs](https://www.npgsql.org/doc/types/nodatime.html)

### Default Schema

> Rather than specifying the schema for each table, you can also define the default schema at the model level with the fluent API:

```cs
protected override void OnModelCreating(ModelBuilder modelBuilder)
{
    modelBuilder.HasDefaultSchema("animals");
}
```

https://docs.microsoft.com/en-us/ef/core/modeling/entity-types?tabs=data-annotations

## Strings

The PostgreSQL EF library correctly maps the C# `string` type to
the recommended PostgreSQL `text` column type.

## Decimal

The PostgreSQL EF library maps the C# `decimal` type to
the PostgreSQL `numeric` column type.
This risks bloating columns with thousands of digits
if calculations are performed on the PostgreSQL server.
this should be fine if values are being read and stored,
and calculations are performed in .NET.

The C# `decimal` more closely (but imprecisely) maps to the PostgreSQL `numeric(58, 29)` type.

Using a PostgreSQL domain seems to be reasonable.
However, these are not managed by EF.

## JSON

The [Npgsql JSON mapping docs](https://www.npgsql.org/efcore/mapping/json.html)
give several different ways of handling JSON.

* Raw string
* Strongly typed
* `JsonDocument` DOM mapping

## Default Column Values

Default column values must be specified programmatically in the `OnModelCreating` method.

```cs
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        ...
        // Define default column values here.
        modelBuilder.Entity<Creature>()
            .Property(b => b.WhenCreated)
            .HasDefaultValueSql("NOW()");
    }
```
