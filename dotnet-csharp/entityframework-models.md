# Entity Framework Core Models

## Inclusion

A class is recognized as a database table
by including it as a `DbSet` on the database context object.

## Authority

Regardless of class annotations,
anything in the `DbContext` class's method:

```cs
protected override void OnModelCreating(ModelBuilder modelBuilder)
```

holds the ultimate truth of the model.

* Reference: [Creating and configuring a model](https://docs.microsoft.com/en-us/ef/core/modeling/)

## Default Schema

The default schema can be set in the database context:

```cs
protected override void OnModelCreating(ModelBuilder modelBuilder)
{
    modelBuilder.HasDefaultSchema(DefaultSchema);
}
```

## Naming

By default EF migrations will create table and field names
with the same capitalization as the class and field names.

Adaptors such as the package `EFCore.NamingConventions`
can alter these defaults,
such as using `snake_case` for table and column names.

Table and schema names can be set:

```cs
// Coerce table name
[Table("cat")]
public class Cat
```

```cs
// Coerce table name and schema
[Table("cat", Schema = "animals")]
public class Cat
{
    // Coerce column name
    [Column("id")]
    public long Id { get; set; }

    // Coerce column name and type
    [Column("name", TypeName = "varchar(200)")]
    public string Name { get; set; }

    // Coerce column type
    [Column(TypeName = "decimal(5, 2)")]
    public decimal CuteFactor { get; set; }
}
```

## Indexes

```cs
[Index(nameof(UniqueItem), IsUnique = true)]
[Index(nameof(WhenCreated))]
public class Cat
{
    public long Id { get; set; }
    public string UniqueItem { get; set; }
    public ZonedDateTime WhenCreated { get; set; }
}
```

* [Index Documentation](https://docs.microsoft.com/en-us/ef/core/modeling/indexes?tabs=data-annotations)

## Other Attributes

```cs
[Comment("Feline Overlords")]
public class Cat
...
```

## Many:Many Relationships

Each table class needs a field to refer to the other table class.

```cs
public class Cat {
    ...
    // Many:many foreign key.
    public IList<Human> Humans { get; } = new List<Human>();
}

public class Human {
    ...
    // Many:many foreign key.
    public IList<Cat> Cats { get; } = new List<Cat>();
}
```

## Parent:Child Relationships

### Cascade

The parent table class has a navigation field.
The child table class has a navigation field and a foreign key field.

This creates `CASCADE` relationships.

```cs
public class Parent {
    // Parent-to-child foreign key navigation.
    public IList<Child> Childs { get; } = new List<Child>();
}

public class Child {
    // Child-to-parent foreign key.
    public long ParentId { get; set; }

    // Child-to-parent foreign key navigation.
    public Parent Parent { get; set; }
}
```

Becomes

```sql
CONSTRAINT fk_childs_parents_parents_id  FOREIGN KEY (parents_id) REFERENCES parents (id) ON DELETE CASCADE
```

### Restrict

If the child fields are nullable,
the relationship is `RESTRICT`.

```cs
public class Parent {
    // Parent-to-child foreign key navigation.
    public IList<Child> Childs { get; } = new List<Child>();
}

public class Child {
    // Child-to-parent foreign key.
    public long? ParentId { get; set; }

    // Child-to-parent foreign key navigation.
    public Parent? Parent { get; set; }
}
```

Becomes

```sql
CONSTRAINT fk_childs_parents_parents_id  FOREIGN KEY (parents_id) REFERENCES parents (id) ON DELETE RESTRICT
```

### PostgreSQL Naming

[PostgreSQL recommends overriding the default naming](https://www.npgsql.org/efcore/modeling/table-column-naming.html).

## References

* [Entity Types](https://docs.microsoft.com/en-us/ef/core/modeling/entity-types?tabs=data-annotations)
* [Entity Properties](https://docs.microsoft.com/en-us/ef/core/modeling/entity-properties?tabs=data-annotations%2Cwithout-nrt)
