# EntityFramework Core and PostgreSQL

## Preparation

[This install guide](https://docs.microsoft.com/en-us/ef/core/get-started/overview/install)
states to install `dotnet ef`.

```sh
dotnet tool install --global dotnet-ef
```

or update with

```sh
dotnet tool update --global dotnet-ef
```

Packages:

* `Microsoft.EntityFrameworkCore`
* `Microsoft.EntityFrameworkCore.Design`
* `Microsoft.EntityFrameworkCore.Tools`
* `Pomelo.EntityFrameworkCore.MySql`
* `MySqlConnector` (Implicitly installed.)

Additional packages:

* `Pomelo.EntityFrameworkCore.MySql.Json.Newtonsoft` for JSON handling.
* `EFCore.NamingConventions` to allow for `snake_case`.

If starting a new project, enable nullable reference types.

## Create Models

Models, at their simplest, are simple classes.

```cs
namespace Foo.Models 
{
    public class Creature
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
```

## Create Database Context

The bare bones context can be configured thus:

```cs
using Foo.Models;
using Microsoft.EntityFrameworkCore;

namespace Foo.DbContexts
{
    public class CreatureContext : DbContext
    {
        public DbSet<Creature> Creatures { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            var version = new MariaDbServerVersion(new Version("10.4"));
            options
                .UseMySql(
                    // TODO: This should not be hard-coded or checked in, obviously.
                    @"Host=localhost;Username=postgres;Password=asdfasdf;Database=animals",
                    version
                )
                // Use `snake_naming` for tables.
                .UseSnakeCaseNamingConvention();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Define default column values here.
            modelBuilder.Entity<Creature>()
                .Property(b => b.WhenCreated)
                .HasDefaultValueSql("NOW()");
        }    }
}
```

### Changing Default Naming

Adding the package `EFCore.NamingConventions` will give the ability to default to
`snake_case` for table and column names.

```cs
protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    => optionsBuilder
        .UseNpgsql(...)
        .UseSnakeCaseNamingConvention();
```

https://www.npgsql.org/efcore/modeling/table-column-naming.html

## Data Types

* Mappings are documented [here](https://github.com/PomeloFoundation/Pomelo.EntityFrameworkCore.MySql/blob/master/src/EFCore.MySql/Storage/Internal/MySqlTypeMappingSource.cs).

### Strings

The Pomelo EF library maps the C# `string` type to `varchar(255)`.

### Decimal

The Pomelo EF library maps the C# `decimal` type to `decimal(65, 30)`.

## Default Column Values

Default column values must be specified programmatically in the `OnModelCreating` method.

```cs
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        ...
        // Define default column values here.
        modelBuilder.Entity<Creature>()
            .Property(b => b.WhenCreated)
            .HasDefaultValueSql("NOW()");
    }
```
