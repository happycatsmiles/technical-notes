# Install .NET Runtime on Raspberry Pi

This is the runtime,
not the full SDK for development.

```sh
curl -sSL https://dotnet.microsoft.com/download/dotnet-core/scripts/v1/dotnet-install.sh -o dotnet-install.sh
bash dotnet-install.sh --channel 5.0

echo 'export DOTNET_ROOT=$HOME/.dotnet' >> ~/.bashrc
echo 'export PATH=$PATH:$HOME/.dotnet' >> ~/.bashrc
source ~/.bashrc
```
