# C# Types

## Built-In Types

The following table lists the C# built-in value types:
Built-in types (C# reference)

C# type keyword | .NET type | Notes
:--- | :--- | :---
sbyte | System.SByte
byte | System.Byte
short | System.Int16
ushort | System.UInt16
int | System.Int32
uint | System.UInt32
long | System.Int64
ulong | System.UInt64
float | System.Single
double | System.Double
decimal | System.Decimal | Not like Python `Decimal`.
bool | System.Boolean
char | System.Char
string | System.String
object | System.Object
dynamic | System.Object | Very specialed. See [docs](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/types/using-type-dynamic).

In the preceding tables, each C# type keyword from the left column (except `nint` and `nuint`) is an alias for the corresponding .NET type. They are interchangeable. For example, the following declarations declare variables of the same type:

```cs
int a = 123;
System.Int32 b = 123;
```

## Floating-Point Types

* The `float` type is the expected native base 2 floating-point type.
* The `double` type is the expected native base 2 floating-point types.
* The `decimal` type is a base 10 floating-point type.
  * [Docs here](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/language-specification/types#the-decimal-type)
  * It has greater precision but less range than `float` and `double`.
  * It does not suffer from the base 2 approximation problems.
  * Behaves differently from the Python `decimal.Decimal` type.
    * Does not remember precision.
  * Cannot be mixed with `float` and `double` in calculations.
    * Requires explicit cast, e.g. `(decimal)x`.
    * There is no implicit cast to `decimal` like there is from `float` to `double`.
  * Implemented as a 96-bit integer scaled by a power of ten.

Type | Approximate Maximum | Precision | Literal
:--- |:--- |:--- |:--
`float` | 10^38 | 6-9 digits | 3_000.5f
`double` | 10^308 | 15-17 digits | 3_000.5d
`decimal` | 10^28 | 28-29 digits | 3_000.5m

## Nullable Reference Types

```cs
int? foo;
int bar = foo ?? 0;
```

This is not enabled by default.

In the `.csproj` file add the following XML:

```xml
    <PropertyGroup>
        <Nullable>enable</Nullable>
    </PropertyGroup>
```

Rider's project properties has a convenient UI for this with more options.

## `void`

The void keyword represents the absence of a type.
You use it as the return type of a method that doesn't return a value.

## Native-Sized Integers

C# type keyword | .NET type | Notes
:--- | :--- | :--- 
nint | System.IntPtr
nuint | System.UIntPtr

These are represented internally by the indicated .NET types,
but in each case the keyword and the .NET type are not interchangeable.
The compiler provides operations and conversions for
`nint` and `nuint` as integer types that it
doesn't provide for the pointer types `System.IntPtr` and `System.UIntPtr`.

## Refrences

* [Built-in types](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/built-in-types)
* [Tutorial: Express your design intent more clearly with nullable and non-nullable reference types](https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/tutorials/nullable-reference-types)