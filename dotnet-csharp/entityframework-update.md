# Updating EntityFramework Core

If the following message appears:

> The Entity Framework tools version '6.0.4' is older than that of the runtime ....

update the tools:

```sh
dotnet tool update --global dotnet-ef
```
