# Update Ubuntu

## Summary

## `apt-get` vs `apt`

* `apt-get` is an older package manager.
* `apt` is a new package manager with an unfortunately similar name.

| apt command        | the command it replaces | function of the command                              |
| :----------------- | :---------------------- | :--------------------------------------------------- |
| `apt install`      | `apt-get install`       | Installs a package                                   |
| `apt remove`       | `apt-get remove`        | Removes a package                                    |
| `apt purge`        | `apt-get purge`         | Removes package with configuration                   |
| `apt update`       | `apt-get update`        | Refreshes repository index                           |
| `apt upgrade`      | `apt-get upgrade`       | Upgrades all upgradable packages                     |
| `apt autoremove`   | `apt-get autoremove`    | Removes unwanted packages                            |
| `apt full-upgrade` | `apt-get dist-upgrade`  | Upgrades packages with auto-handling of dependencies |
| `apt search`       | `apt-cache search`      | Searches for the program                             |
| `apt show`         | `apt-cache show`        | Shows package details                                |

## Difference Between Update and Upgrade

* `update` syncs the local database with the latest available packages.
* `upgrade` only gets small changes that don't add/remove new/old dependencies, such as minor updates for Firefox.
* `full-upgrade` gets all updates, crossing major versions.

### Example

```sh
sudo apt update
sudo apt full-upgrade -y
```

## Reference

[Difference Between apt and apt-get Explained](https://itsfoss.com/apt-vs-apt-get-difference/)
