# Open a Port on the Ubuntu 16 Firewall

> **!!! WARNING !!!** 
> If running Docker, this will bung up Docker's firewall settings.
> You must restart the Docker service.

To open (for example), port 5000: 

```bash
sudo ufw allow 5000/tcp
sudo ufw reload
```

## Docker

If running Docker,
restart it with:

```
sudo systemctl restart docker
```

