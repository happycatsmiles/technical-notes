# Ubuntu 16 + ngingx + uWSGI + Django 

DigitalOcean has a 
[nice tutorial](https://www.digitalocean.com/community/tutorials/how-to-serve-django-applications-with-uwsgi-and-nginx-on-ubuntu-14-04) 
on Ubuntu 14 + nginx + uWSGI + Django. 

Ubuntu 16 breaks this because it switched from `upstart` to `systemd`. 

StackExchange has an 
[article with a fix](https://serverfault.com/questions/775965/wiring-uwsgi-to-work-with-django-and-nginx-on-ubuntu-16-04#775966). 

For complaints about `pcre` support, 
see the StackExchange article
[here](https://stackoverflow.com/questions/21669354/rebuild-uwsgi-with-pcre-support#22645915). 
