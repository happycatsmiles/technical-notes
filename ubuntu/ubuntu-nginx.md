# Setting Up nginx on Ubuntu

## Initial Setup

Digital Ocean has a 
[setup guide](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-20-04)
for Ubuntu 20.

## Update Ubuntu

### Update and Upgrade

```sh
sudo apt update && sudo apt upgrade -y && sudo apt-get dist-upgrade -y
```

Note the `dist-update` may present a dialog.

### Clean Up

```sh
sudo apt-get autoremove -y && sudo apt-get autoclean -y
```

Reboot.

## Firewall

Don't forget to open up SSH.

```sh
sudo ufw allow OpenSSH
```

## Install nginx

Digital Ocean has a 
[setup guide](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04)
for nginx.

## Install HTTPS

[Let's Encrypt Site](https://letsencrypt.org/)

