# Add User To A Group

```sh
sudo usermod -a -G groupName userName
```

* The -a (append) switch is essential. Otherwise, the user will be removed from any groups, not in the list.
* The -G switch takes a (comma-separated) list of additional groups to assign the user to.

## References

* [How to add existing user to an existing group?](https://askubuntu.com/questions/79565/how-to-add-existing-user-to-an-existing-group)
