## FET Notes

Personal notes. Corrections welcome.

1.  Bipolar Junction Transistors (BJTs) are controlled by current.
1.  Field-Effect Transistors (FETs) are controlled by voltage.

### On

When FETs are turned on, current can flow **both ways**
between the source and drain.
Thus, they can be used with alternating current (AC).

### Off

When FETs are turned off, FETs act as diodes.
This "body diode" is a natural result of how they are constructed.
Sometimes the body diode is indicated on the FET symbol.

![n-channel fet enhancement body diode](https://user-images.githubusercontent.com/33188353/33003297-9a75b812-cd6e-11e7-8efb-c00be46026f4.png)

To cut off AC voltage completely,
use two FETs with their body diodes pointing in opposite directions.

![opposed body diodes](https://user-images.githubusercontent.com/33188353/33003295-9a552c50-cd6e-11e7-9ee4-ccd896d4a6f6.png)

### Modes

FETs can be made in two modes,
enhancement mode and
depletion mode.
Enhancement mode is the most common.

| Enhancement Mode | Depletion Mode |
| ---------------- | -------------- |
| Normally off          | Normally On |
| Gate turns on         | Gate turns off |
| ![n-channel fet enhancement](https://user-images.githubusercontent.com/33188353/33003299-9a96cfca-cd6e-11e7-9986-404653069117.png)         | ![n-channel fet depletion](https://user-images.githubusercontent.com/33188353/33003296-9a656408-cd6e-11e7-911c-4fa7247c80a0.png) |


### P/N Uses

The terms "sufficiently negative" and "sufficiently positive"
are compared to the drain/source voltage.

| | P-channel | | N-channel |
| --- | --- | --- | --- |
| | ![p-channel fet enhancement body diode](https://user-images.githubusercontent.com/33188353/33003298-9a865c76-cd6e-11e7-978f-ab5c021abfb6.png) | | ![n-channel fet enhancement body diode](https://user-images.githubusercontent.com/33188353/33003297-9a75b812-cd6e-11e7-8efb-c00be46026f4.png) |
| 0 | "On" = the gain is "sufficiently negative" | 1 | "On" = gain is "sufficiently positive" |
| 1 | "Off" = negative voltage removed | 0 | "Off" = positive voltage removed |
| | ![p-channel fet depletion body diode](https://user-images.githubusercontent.com/33188353/33003293-9a2be160-cd6e-11e7-9aa5-d9fd767e086b.png) | | ![n-channel fet depletion body diode](https://user-images.githubusercontent.com/33188353/33003301-9ac5d87e-cd6e-11e7-90cb-70840bde6e39.png)
| 1 | "On" = negative voltage removed | 0 | "On" = positive voltage removed |
| 0 | "Off" = the gain is "sufficiently negative" | 1 | "Off" = gain is "sufficiently positive" |
