# Prettier Options

I can appreciate the goals of prettier,
but I prefer to not use it in my own work.

For shared projects at work, etc.
I can see using the following:


```json
{
  "singleQuote": true,
  "trailingComma": "all",
  "semi": false,
  "bracketSpacing": false,
  "arrowParens": "avoid",
  "bracketSameLine": false
}
```

Some clients want terminating semicolons, so `semi` would be removed.
