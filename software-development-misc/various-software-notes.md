# Various Notes on Software Engineering

This is a collection of ideas to consider for applicability to the situation at hand.

* Make naming more effective.
  * Choose the right name.
  * Get specific.
  * Eliminate words.
* Reduce variable scope.
  * Declare variables at the point of their initialization.
  * Variables used in a single block should be declared in it.
  * Fields used in a single method invocation should be local to that method.
* Replace second assignment to a variable with a new variable.
  * Instead of accumulating changes in a variable, use different variables to
    represent changes.
* Pass globals as parameters.
  * Make dependencies explicit by passing them as arguments rather than
    relying on global access.
* Replace long parameter list with parameter object.
  * Group cohesive parameters together as objects.
  * These are often value types, and may also reveal new domain types.
* Replace resource passing with direct value.
  * Instead of passing suppliers, factories, etc. to methods, pass the result
    of their use directly.
* Remove modifiers.
  * Remove unused modifiers (e.g. setters) and consider replace modifiers with constructors.
  * Extract modifiers to build and make void methods
    [fluent](https://en.wikipedia.org/wiki/Fluent_interface).
* Replace modification with constructors.
  * Many sequences of modification can often be
    replaced with a broader set of meaningful constructors.
* Move modifiers to builders.
  * Define mutable companion class to handle sequences of modification
    (c.f. String and StringBuilders).
* Replace side effects with new objects.
  * Return a new object instead of changing an existing one.
  * Often making void methods fluent is a useful stepping stone.
* Make void methods fluent.
  * Have a method return an existing or new object the caller can chain from.
* Replace mutable collection with
  [persistent](https://en.wikipedia.org/wiki/Persistent_data_structure) collection. [1]
  * Use, create, or repurpose a mutable collection with (or as) a persistent collection,
    i.e. one that is effectively immutable to referential observers,
    allowing sharing without cost of copying.
* Replace mutable collection values with immutable values.
  * A collection that is itself unchanging or persistent cannot
    preserve its immutability guarantee if its collected references are mutable,
    so use immutable values for collection elements.
* Replace state-based object behaviour with polymorphism.
  * Replace flags that are unchanging for a given collection,
    but cause branching to different behaviours with polymorphism,
    i.e. use instances of types to represent the behavioural differences.
* Extract iteration.
  * Repetition is repetitive, so factor out common loops
    in appropriately named and parameterized methods.
* Extract control flow as higher-order functions.
  * Much control flow has stable structure but variable conditions or actions,
    so use pluggable behaviour.
* Replace control flow with data structures.
  * Many data structures embody control intelligence,
    e.g. sorted collections and regular expressions,
    so replace long-hand logic and control with more intelligent data structures.
* Replace control flow with data flow.
  * Use pipelines,
    polymorphism and higher-order methods to replace control with data flow.
* Replace loop with stream filtering.
  * Use collection pipelines to replace
    explicit looping and control flow with data flow.

[1]
> In computing,
> a persistent data structure is [one] that always preserves
> the previous version of itself when it is modified.
> Such data structures are effectively immutable,
> as their operations do not (visibly) update the structure in-place,
> but instead always yield a new updated structure.
>
> (A persistent data structure is not a data structure
> committed to persistent storage, such as a disk;
> this is a different and unrelated sense of the word "persistent.")

