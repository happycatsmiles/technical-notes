# Sequence Diagrams

A handy tool to [create sequence diagrams from text](https://bramp.github.io/js-sequence-diagrams/).
