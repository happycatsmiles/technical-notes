# Don't Refactor Mindlessly

## Human Cost / Human Motivations

> Let clean code guide you. Then let it go.

* There are human psychological and emotional costs to refactoring code.
* Don't refactor just because it bugs you.
  * That's an attachment.
  * It can be an act of ego, assuming you know best and charge right in.

https://overreacted.io/goodbye-clean-code/
