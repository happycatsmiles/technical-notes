# Update Rocky Linux

## Checking for Updates

```sh
sudo dnf check-updates
```

## Updating

```sh
sudo dnf update -y
```

## Installing

```sh
sudo dnf install [-y] <package>
```

## Uninstalling

```sh
sudo dnf erase [-y] <package>
```
