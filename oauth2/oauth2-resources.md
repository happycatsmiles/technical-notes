# OAuth2 Resources

* [An Illustrated Guide to OAuth and OpenID Connect](https://www.youtube.com/watch?v=t18YB3xDfXI) by OktaDev
  * This is is a nice 15-minute breakdown of OAuth2 and OIDC in a comic format.
* [Securing Your APIs with OAuth 2.0 - API Days](https://www.youtube.com/watch?v=PfvSD6MmEmQ)
  * Hour-long in-depth technical description of common flows.
  * Covers PKCE vs. introspection, pros and cons.
* [OAuth: When Things Go Wrong](https://www.youtube.com/watch?v=H6MxsFMAoP8)
  * "Aaron Parecki discusses common security threats when building microservices using OAuth and how to protect yourself. You'll learn about high-profile API security breaches related to OAuth; common implementation patterns for mobile apps, browser-based apps, and web 
server apps; and the latest best practices around OAuth security being developed by the IETF OAuth working group."
  * I haven't looked at this yet.
* [What's going on with the OAuth 2.0 Implicit flow?](https://www.youtube.com/watch?v=CHzERullHe8)
  * Nice explanation of implicit flow and why it's deprecated.
  * Found it at the article [Is the OAuth 2.0 Implicit Flow Dead?](https://developer.okta.com/blog/2019/05/01/is-the-oauth-implicit-flow-dead)
