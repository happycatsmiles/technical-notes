# Premier Pro Titles

For music titles:

1. Lower Left
   1. X: 50 Y: 900 Size: 75%        (Title)
   2. X: 50 Y: 1000 Size: 100%      (Artist)
2. Lower Right
   1. X: 1850 Y: 900 Size: 75% Alignment: Right
   2. X: 1850 Y: 1000 Size: 100% Alignment: Right

Shadow
Colour: Black
Opacity: 100%
