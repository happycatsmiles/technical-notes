# Bootstrap 4 Layout

23 April 2021

## Grid

The Bootstrap "grid" is not related to the CSS grid.
This is their general word for their flex-based grid system.

### Containers

* General containers are `.container`.
* Full-width containers are `.container-fluid`.

### References

* [Layout Overview](https://getbootstrap.com/docs/4.0/layout/overview/)
* [Grid Overview](https://getbootstrap.com/docs/4.0/layout/grid/)

## Horizontal Centring

* Use the class `mx-auto` to horizontally centre elements.
* Use the class `text-center` to horizontally centre text.

### References

* [Sizing](https://getbootstrap.com/docs/4.0/utilities/sizing/)
* [Text](https://getbootstrap.com/docs/4.0/utilities/text/)

## Width and Height

Width and height can be set to
25%, 50%, 75% and 100% of the parent element thus:

| Direction  | 25%    | 50%    | 75%    | 100%    |
| :--------- | :----- | :----- | :----- | :------ |
| Horizontal | `w-25` | `w-50` | `w-75` | `w-100` |
| Vertical   | `h-25` | `h-50` | `h-75` | `h-100` |

Max height or width can be set to 100% with
`mh-100` and `mw-100` respectively.

### References

* [Sizing](https://getbootstrap.com/docs/4.0/utilities/sizing/)

## Margins and Padding

* There are a lot of definitions for margins and padding.
  * `mr-3` is `margin-right: 1rem;`

The numbers are unintuitive.
There is a standard `$spacer` amount of `1rem`.
The numbers after the dash are relative to `$spacer`.

| #    | Value                   |
| :--- | :---------------------- |
| 0    | `0` (no margin/padding) |
| 1    | `$spacer` / 4           |
| 2    | `$spacer` / 2           |
| 3    | `$spacer`               |
| 4    | 1.5 * `$spacer`         |
| 5    | 3 * `$spacer`           |
| auto | set to `auto`           |

### References

* Margins and Spacing
  * [Utilities for Layout](https://getbootstrap.com/docs/4.0/layout/utilities-for-layout/)
  * [Spacing](https://getbootstrap.com/docs/4.0/utilities/spacing/)
